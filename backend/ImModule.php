<?php
namespace emilasp\im\backend;

/**
 * Class ImModule
 * @package emilasp\im\backend
 */
class ImModule extends \emilasp\im\frontend\ImModule
{
    public $tmpPath = '@app/runtime/import/data';
}
