<?php

namespace emilasp\im\backend\controllers;

use kartik\grid\EditableColumnAction;
use Yii;
use yii\helpers\Json;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use emilasp\im\common\models\Product;
use emilasp\im\common\models\search\ProductSearch;
use emilasp\core\components\base\Controller;

/**
 * ProductController implements the CRUD actions for Good model.
 */
class ProductController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index', 'view', 'create', 'update', 'change-status', 'change-index', 'delete'],
                'rules'        => [
                    [
                        'actions' => [
                            'index',
                            'view',
                            'create',
                            'update',
                            'delete',
                            'change-status',
                            'change-index',
                        ],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'delete'        => ['POST'],
                    'change-status' => ['POST'],
                    'change-index'  => ['POST'],
                ],
            ],
        ];
    }


    public function actions()
    {
        return ArrayHelper::merge(parent::actions(), [
            'editcost' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::className(),     // action class name
                'modelClass' => Product::className(),                // the model for the record being edited
                'outputValue' => function ($model, $attribute, $key, $index) {
                    return $model->{$attribute};      // return any custom output value if desired
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';                                  // any custom error to return after model save
                },
                'showModelErrors' => true,                        // show model validation errors after save
                'errorOptions' => ['header' => '']                // error summary HTML options
            ],
            'editstatus' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::className(),     // action class name
                'modelClass' => Product::className(),                // the model for the record being edited
                'outputValue' => function ($model, $attribute, $key, $index) {
                    $statusesView = [
                        0 => '<i class="fa fa-circle-o color-red"></i>',
                        1 => '<i class="fa fa-circle color-green"></i>',
                    ];
                    return $statusesView[$model->{$attribute}];      // return any custom output value if desired
                },
                /*'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';                                  // any custom error to return after model save
                },
                'showModelErrors' => true,                        // show model validation errors after save
                'errorOptions' => ['header' => '']   */             // error summary HTML options
            ],
            'editonindex' => [                                       // identifier for your editable column action
                'class' => EditableColumnAction::className(),     // action class name
                'modelClass' => Product::className(),                // the model for the record being edited
                'outputValue' => function ($model, $attribute, $key, $index) {
                    $onIndexView = [
                        0 => '<i class="fa fa-circle-o color-red"></i>',
                        1 => '<i class="fa fa-circle color-green"></i>',
                    ];
                    return $onIndexView[$model->{$attribute}];      // return any custom output value if desired
                },
                'outputMessage' => function($model, $attribute, $key, $index) {
                    return '';                                  // any custom error to return after model save
                },
                'showModelErrors' => true,                        // show model validation errors after save
                'errorOptions' => ['header' => '']                // error summary HTML options
            ]
        ]);
    }

    public function actionChangeStatus($id)
    {
        $model = $this->findModel($id, Product::className());

        if ($model->status) {
            $model->status = 0;
        } else {
            $model->status = 1;
        }
        $model->save();

        return $this->redirect(['index']);
    }

    public function actionChangeIndex($id)
    {
        $model = $this->findModel($id, Product::className());

        if ($model->on_index) {
            $model->on_index = 0;
        } else {
            $model->on_index = 1;
        }
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Lists all Good models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel  = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->setSort([
            'defaultOrder' => ['id' => SORT_DESC],
        ]);
        
        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Good model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id, Product::className()),
        ]);
    }

    /**
     * Creates a new Good model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Good model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        /** @var Good $model */
        $model = $this->findModel($id, Product::className());

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Good model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id, Product::className())->delete();

        return $this->redirect(['index']);
    }
}
