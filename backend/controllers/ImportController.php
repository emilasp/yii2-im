<?php

namespace emilasp\im\backend\controllers;

use emilasp\im\common\models\search\ProductSearch;
use Goodby\CSV\Import\Standard\Interpreter;
use Goodby\CSV\Import\Standard\Lexer;
use Goodby\CSV\Import\Standard\LexerConfig;
use Yii;
use yii\helpers\Json;
use yii\web\UploadedFile;
use yii\base\DynamicModel;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use emilasp\core\components\base\Controller;

/**
 * ImportController implements the CRUD actions for Good model.
 */
class ImportController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['import-file', 'upload-file', 'row'],
                'rules'        => [
                    [
                        'actions' => [
                            'import-file',
                            'row',
                            'upload-file',
                        ],
                        'allow'   => true,
                        'roles'   => ['admin', 'manager'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'row' => ['POST'],
                    'upload-file' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Good models.
     * @return mixed
     */
    public function actionImportFile()
    {
        $model = $this->getModelFile();

        if ($file = UploadedFile::getInstance($model, 'filename')) {
            $model->import = 1;

            $tmpFileName = Yii::getAlias(Yii::$app->getModule('im')->tmpPath) . DIRECTORY_SEPARATOR . time() . '.csv';

            $file->saveAs($tmpFileName);

            $rows = $this->parseCsv($model->separator, $tmpFileName, $rowsCount = 2);

            $fields = [
                '' => 'Не используется',
            ];

            echo Json::encode([
                /*'csvHead' => $csvHead,
                'csvData' => $csvData,*/
                'tmpFileName' => $tmpFileName,
                'fields' => $fields,
            ]);

            Yii::$app->end();
        } else {
            $model->import = 0;
        }

        return $this->render('form-file', [
            'model' => $model,
        ]);
    }

    /**
     * Lists all Good models.
     * @return mixed
     */
    public function actionUploadFile()
    {
        $searchModel  = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionRow()
    {

    }

    private function parseCsv($delimiter, $file, $rowsCount = 1)
    {
        $data = [];

        $config = new LexerConfig();
        $config
            ->setDelimiter($delimiter) // Customize delimiter. Default value is comma(,)
            ->setEnclosure("'")  // Customize enclosure. Default value is double quotation(")
            ->setEscape("\\")    // Customize escape character. Default value is backslash(\)
            ->setToCharset('UTF-8') // Customize target encoding. Default value is null, no converting.
           // ->setFromCharset('SJIS-win') // Customize CSV file encoding. Default value is null.
        ;


        $lexer = new Lexer($config);

        $interpreter = new Interpreter();
        $interpreter->interpret([1]);

        $interpreter->addObserver(function(array $row) use (&$temperature) {
            $data[] = $row;
        });

        $lexer->parse($file, $interpreter);

        return $data;
    }

    /** Получаем динамическую модель
     * @return static
     */
    private function getModelFile()
    {
        $model =  DynamicModel::validateData([
            'filename',
            'import',
            'tmpfile',
            'separator',
            'typeColumn',
        ], [
            ['filename', 'file', 'extensions' => 'csv, CSV'],
            ['import', 'integer'],
            ['tmpfile', 'string'],
            ['separator', 'string'],
            ['typeColumn', 'string'],
        ]);//, 'mimeTypes' => 'image/jpeg, image/png'

        $model->load(Yii::$app->request->post());

        if ($model->separator == null) {
            $model->separator = "\t";
        }

        return $model;
    }
}
