<?php
use emilasp\files\models\File;
use yii\helpers\Url;

?>

<div class="product-list-order">

    <table cellpadding="0" cellspacing="0" width="100%" class="table table-bordered table-hover table-striped">
        <thead>
        <tr class="head-table-products">
            <th>--</th>
            <th><?= Yii::t('im', 'Article') ?></th>
            <th><?= Yii::t('site', 'Name') ?></th>
            <th><?= Yii::t('taxonomy', 'Properties') ?></th>
            <th><?= Yii::t('im', 'Count') ?></th>
            <th><?= Yii::t('im', 'Cost') ?></th>
            <th><?= Yii::t('im', 'Sum') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($order->productLinks as $productLink) : ?>
            <tr valign="center">
                <?php $product = $productLink->product; ?>
                <td><img src="<?= $product->getImage()->getUrl(File::SIZE_ICO) ?>" width="50px"/></td>
                <td><?= $product->article ?></td>
                <td><a href="<?= $product->getUrl() ?>"><?= $product->name ?></a></td>
                <td>
                    <?php foreach ($productLink->getVariation() as $value) : ?>
                        <?= $value->property->name . ': ' . $value->value ?>,
                    <?php endforeach; ?>
                </td>
                <td><?= $productLink->quantity ?></td>
                <td><?= $product->getPrice() ?></td>
                <td><?= $productLink->cost ?></td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

</div>
