<?php
use emilasp\files\models\File;
use yii\helpers\Url;

?>

<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tr>
        <td align="center">

            <table width="600" cellpadding="2" cellspacing="0" border="1" style="border-collapse: collapse;">
                <thead>
                <tr style="background-color: #95bcdc;">
                    <th style="padding:3px 5px;">--</th>
                    <th style="padding:3px 5px;"><?= Yii::t('im', 'Article') ?></th>
                    <th style="padding:3px 5px;"><?= Yii::t('site', 'Name') ?></th>
                    <th style="padding:3px 5px;"><?= Yii::t('taxonomy', 'Properties') ?></th>
                    <th style="padding:3px 5px;"><?= Yii::t('im', 'Count') ?></th>
                    <th style="padding:3px 5px;"><?= Yii::t('im', 'Cost') ?></th>
                    <th style="padding:3px 5px;"><?= Yii::t('im', 'Sum') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($order->productLinks as $productLink) : ?>
                    <tr valign="center">
                        <?php $product = $productLink->product; ?>
                        <td style="padding:2px 3px;"><img src="<?= $product->getImage()->getUrl(File::SIZE_ICO, false, true) ?>" width="50px"/>
                        <td style="padding:2px 3px;"><?= $product->article ?></td>
                        </td>
                        <td style="padding:2px 3px;"><a href="<?= $product->getUrl() ?>"><?= $product->name ?></a></td>
                        <td style="padding:2px 3px;">
                            <?php foreach ($productLink->getVariation() as $value) : ?>
                                <?= $value->property->name . ': ' . $value->value ?>,
                            <?php endforeach; ?>
                        </td>
                        <td style="padding:2px 3px;"><?= $productLink->quantity ?> <?= Yii::t('im', 'Un') ?></td>
                        <td style="padding:2px 3px;"><?= $product->getPrice() ?> <?= Yii::t('im', 'Rub') ?></td>
                        <td style="padding:2px 3px;"><?= $productLink->cost ?> <?= Yii::t('im', 'Rub') ?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        </td>
    </tr>
    <tr>
        <td style="background-color: #dbe5ff">
            <h3><?= Yii::t('im', 'Sum') ?>: <?= $order->sum ?> <?= Yii::t('im', 'Rub') ?></h3>
            <h3><?= Yii::t('im', 'Count') ?>: <?= count($order->productLinks) ?> <?= Yii::t('im', 'Un') ?></h3>
        </td>
    </tr>

</table>