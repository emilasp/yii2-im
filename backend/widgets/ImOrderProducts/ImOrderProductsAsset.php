<?php

namespace emilasp\im\backend\widgets\ImOrderProducts;

use emilasp\core\components\base\AssetBundle;

/**
 * Class ImProducts
 * @package emilasp\im\backend\widgets\ImOrderProducts
 */
class ImOrderProductsAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'products'
    ];
    public $js = [
        //'sidebar'
    ];
}
