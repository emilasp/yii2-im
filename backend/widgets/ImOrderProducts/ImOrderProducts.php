<?php
namespace emilasp\im\backend\widgets\ImOrderProducts;

use emilasp\core\components\base\Widget;
use emilasp\goal\common\models\Goal;
use yii;

/**
 * Class ImOrderProducts
 * @package emilasp\im\backend\widgets\ImOrderProducts
 */
class ImOrderProducts extends Widget
{
    const TYPE_VIEW_DEFAULT = 'default';
    const TYPE_VIEW_MAIL = 'mail';
    
    public $order;
    
    public $typeView = self::TYPE_VIEW_DEFAULT;

    public function init()
    {
        $this->registerAssets();
    }

    public function run()
    {
        echo $this->render('products-' . $this->typeView, [
            'order' => $this->order,
        ]);
    }


    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        ImOrderProductsAsset::register($view);
    }
}
