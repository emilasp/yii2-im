<?php

use dosamigos\fileupload\FileUpload;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use emilasp\user\core\models\User;
use emilasp\im\common\models\Product;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */

$this->title = Yii::t('im', 'Products Import');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="form-fileload">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->errorSummary($model, ['header' => '']); ?>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Файл</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-md-4">
                    <?= FileUpload::widget([
                        'model' => $model,
                        'attribute' => 'filename',
                        'url' => ['/im/import/import-file'],
                        //'options' => ['accept' => 'text/*'],
                        'clientOptions' => [
                            'maxFileSize' => 2000000
                        ],
                        'clientEvents' => [
                            'fileuploaddone' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
                            'fileuploadfail' => 'function(e, data) {
                                console.log(e);
                                console.log(data);
                            }',
                        ],
                    ]);?>
                </div>
                <div class="col-md-2">
                    <?= $form->field($model, 'separator')
                        ->textInput(['class' => 'form-control'])->label('Разелитель CSV') ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    1
                </div>
                <div class="col-md-4">
                    2
                </div>
                <div class="col-md-4">
                    3
                </div>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('site', 'Download'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

