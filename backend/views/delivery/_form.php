<?php

use dosamigos\ckeditor\CKEditor;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Delivery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="delivery-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('im', 'Tab base') ?></a></li>
        <li><a data-toggle="tab" href="#fields"><?= Yii::t('im', 'Tab fields') ?></a></li>
        <li><a data-toggle="tab" href="#data"><?= Yii::t('im', 'Tab data') ?></a></li>
    </ul>

    <div class="tab-content">
        <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_fields', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_data', ['form' => $form, 'model' => $model]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
