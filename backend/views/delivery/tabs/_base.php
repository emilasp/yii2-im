<?php
use dosamigos\ckeditor\CKEditor;
use emilasp\geoapp\models\GeoAddrobj;
use kartik\widgets\Select2;
use emilasp\user\backend\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="base" class="tab-pane fade in active clearfix">

    <div class="row">
        <div class="col-md-3"><?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?></div>
        <div class="col-md-9"><?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?></div>
    </div>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'options' => ['rows' => 1],
        'preset'  => 'standart',
    ]) ?>

    <?= $form->field($model, 'text_free')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'icon')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'show_on_product_page')->checkbox() ?>

    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'status')->dropDownList($model->statuses) ?></div>
        <div class="col-md-8"><?= $form->field($model, 'order')->textInput() ?></div>
    </div>

</div>