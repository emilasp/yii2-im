<?php
/* @var $this yii\web\View */
use emilasp\geoapp\widgets\AddressWidget\AddressWidget;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;
?>
<div id="fields" class="tab-pane fade clearfix">

    <h2><?= Yii::t('site', 'Fields') ?></h2>

    <?= DynamicFields::widget([
        'form'      => $form,
        'model'     => $model,
        'attribute' => 'field',
        'scheme'    => DynamicModel::SCHEME_FIELDS,
        'sort'      => true,
    ]); ?>

</div>
