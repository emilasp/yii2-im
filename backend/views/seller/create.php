<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Seller */

$this->title = Yii::t('im', 'Create Seller');
$this->params['breadcrumbs'][] = ['label' => Yii::t('im', 'Sellers'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seller-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
