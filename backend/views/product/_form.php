<?php

use emilasp\files\extensions\FileManager\FileManagerForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('im', 'Tab base') ?></a></li>
        <li><a data-toggle="tab" href="#category"><?= Yii::t('im', 'Tab category') ?></a></li>
        <li><a data-toggle="tab" href="#property"><?= Yii::t('im', 'Tab property') ?></a></li>
        <li><a data-toggle="tab" href="#variation"><?= Yii::t('im', 'Tab variation') ?></a></li>
        <li><a data-toggle="tab" href="#price"><?= Yii::t('im', 'Tab price') ?></a></li>
        <li><a data-toggle="tab" href="#images"><?= Yii::t('im', 'Tab images') ?></a></li>
        <li><a data-toggle="tab" href="#seo"><?= Yii::t('im', 'Tab seo') ?></a></li>
    </ul>

    <div class="tab-content">
        <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_category', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_property', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_variation', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_price', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_images', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_seo', ['form' => $form, 'model' => $model]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?= FileManagerForm::widget(['model' => $model]) ?>