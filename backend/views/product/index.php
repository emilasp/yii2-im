<?php
use emilasp\files\models\File;
use emilasp\im\common\models\Brand;
use kartik\editable\Editable;
use yii\helpers\Html;
use kartik\grid\GridView;


/* @var $this yii\web\View */
/* @var $searchModel emilasp\im\common\models\search\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('im', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
    <div class="product-index">

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                ['class' => '\kartik\grid\SerialColumn'],
                [
                    'attribute' => 'image',
                    'value'     => function ($model) {
                        $image = $model->getImage();
                        if ($image) {
                            $html = Html::a(
                                Html::img($image->getUrl(File::SIZE_ICO), [
                                    'alt'   => $image->title,
                                    'width' => '25px',
                                ]),
                                $image->getUrl(File::SIZE_MAX),
                                [
                                    'data-jbox-image' => 'gl',
                                ]
                            );
                            return $html;
                        }
                        return '-';
                    },
                    'class'     => '\kartik\grid\DataColumn',
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '30px',
                    'format'    => 'raw',
                ],
                [
                    'attribute' => 'id',
                    'class'     => '\kartik\grid\DataColumn',
                    'width'     => '100px',
                    'hAlign'    => GridView::ALIGN_CENTER,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                ],
                'article',
                //'seller_id',
                'name',
                [
                    'class'           => 'kartik\grid\EditableColumn',
                    'attribute'       => 'cost',
                    'pageSummary'     => true,
                    'editableOptions' => [
                        'header'      => Yii::t('im', 'Cost'),
                        'format'      => Editable::FORMAT_LINK,
                        'inputType'   => Editable::INPUT_TEXT,
                        //'data'=> $StatusList,
                        'formOptions' => ['action' => ['/im/product/editcost']],
                    ],
                    'hAlign'          => GridView::ALIGN_CENTER,
                ],
                'cost_base',
                //'quantity',
                /*[
                    'attribute'           => 'created_by',
                    'value'               => function ($model) {
                        return $model->createdBy->username;
                    },
                    'class'               => '\kartik\grid\DataColumn',
                    'hAlign'              => GridView::ALIGN_LEFT,
                    'vAlign'              => GridView::ALIGN_MIDDLE,
                    'width'               => '150px',
                    'filterType'          => GridView::FILTER_SELECT2,
                    'filterWidgetOptions' => [
                        'language'      => \Yii::$app->language,
                        'data'          => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                        'options'       => ['placeholder' => '-выбрать-'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                ],*/
                [
                    'attribute' => 'brand_id',
                    'value'     => function ($model, $key, $index, $column) {
                        return $model->brand->name;
                    },
                    'class'     => '\kartik\grid\DataColumn',
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '150px',
                    'filter'    => Brand::find()->map()->all(),
                ],

                [
                    'attribute' => 'created_at',
                    'class'     => '\kartik\grid\DataColumn',
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '250px',
                ],
                /*[
                    'attribute' => 'type',
                    'value'     => function ($model, $key, $index, $column) {
                        return $model->types[$model->type];
                    },
                    'class'     => '\kartik\grid\DataColumn',
                    'hAlign'    => GridView::ALIGN_LEFT,
                    'vAlign'    => GridView::ALIGN_MIDDLE,
                    'width'     => '150px',
                    'filter'    => $searchModel->types,
                ],*/
                [
                    'class'           => 'kartik\grid\EditableColumn',
                    'attribute'       => 'status',
                    'pageSummary'     => true,
                    'editableOptions' => [
                        'header'             => Yii::t('site', 'Status'),
                        'format'             => Editable::FORMAT_LINK,
                        'inputType'          => Editable::INPUT_CHECKBOX,
                        'displayValueConfig' => [
                            '0' => '<i class="fa fa-circle-o color-red"></i>',
                            '1' => '<i class="fa fa-circle color-green"></i>',
                        ],
                        'formOptions'        => ['action' => ['/im/product/editstatus']],
                    ],
                    'hAlign'          => GridView::ALIGN_CENTER,
                    'filter'          => $searchModel->statuses,
                ],
                [
                    'class'           => 'kartik\grid\EditableColumn',
                    'attribute'       => 'on_index',
                    'pageSummary'     => true,
                    'editableOptions' => [
                        'header'             => Yii::t('im', 'On index'),
                        'format'             => Editable::FORMAT_LINK,
                        'inputType'          => Editable::INPUT_CHECKBOX,
                        'displayValueConfig' => [
                            '0' => '<i class="fa fa-square-o color-red"></i>',
                            '1' => '<i class="fa fa-square color-green"></i>',
                        ],
                        'formOptions'        => ['action' => ['/im/product/editonindex']],
                    ],
                    'hAlign'          => GridView::ALIGN_CENTER,
                    'filter'          => $searchModel->on_indices,
                ],
                [
                    'class'    => '\kartik\grid\ActionColumn',
                    'width'    => '100px',
                ],
            ],
            'responsive'   => true,
            'hover'        => true,
            'condensed'    => true,
            'floatHeader'  => true,
            'panel'        => [
                'heading'    => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> ' . Html::encode($this->title) . ' </h3>',
                'type'       => 'info',
                'before'     => Html::a(
                    '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('site', 'Add'),
                    ['create'],
                    ['class' => 'btn btn-success']
                ),
                'after'      => Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i> Reset List',
                    ['index'],
                    ['class' => 'btn btn-info']
                ),
                'showFooter' => false,
            ],
        ]);
        ?>
    </div>

<?php

$this->registerJs('new jBox(\'Image\');');