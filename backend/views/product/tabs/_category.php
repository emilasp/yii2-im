<?php
/* @var $this yii\web\View */
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;
use yii\web\JsExpression;

/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="category" class="tab-pane fade clearfix">

    <h2>Category</h2>

    <?= CategorySelectTree::widget([
        'rootId'  => Yii::$app->controller->module->getSetting('category_root_id'),
        'model' => $model,
        'attribute' => 'categoriesId'
    ]) ?>

</div>
