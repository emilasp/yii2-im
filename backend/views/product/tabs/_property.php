<?php
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use emilasp\taxonomy\extensions\PropsValueObjManager\PropsValueObjManager;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="property" class="tab-pane fade clearfix">
    <h2><?= Yii::t('taxonomy', 'Properties') ?></h2>

    <?= PropsValueObjManager::widget(['object' => $model, 'enableVariation' => true]) ?>
</div>
