<?php
/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="price" class="tab-pane fade clearfix">

    <?= $form->field($model, 'cost')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cost_base')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'quantity')->textInput() ?>

    <?= $form->field($model, 'discount')->textInput() ?>

    <?= $form->field($model, 'unit')->textInput() ?>

    <?= $form->field($model, 'weight')->textInput() ?>

    <?= $form->field($model, 'on_request')->textInput() ?>

</div>