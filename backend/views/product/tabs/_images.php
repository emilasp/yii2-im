<?php
use emilasp\files\extensions\FileManager\FileManager;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="images" class="tab-pane fade clearfix">

    <h2>Images</h2>

    <?= FileManager::widget(['model' => $model]) ?>

</div>