<?php
use dosamigos\ckeditor\CKEditor;
use emilasp\im\common\models\Brand;
use emilasp\im\common\models\Seller;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="base" class="tab-pane fade in active clearfix">

    <div class="row">
        <div class="col-md-6">
            <?php $type = $model->types; ?>
            <?= $form->field($model, 'type')->dropDownList($model->types) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'article')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'text')->widget(CKEditor::className(), [
                'options' => ['rows' => 3],
                'preset'  => 'standart',
            ]) ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'short_text')->widget(CKEditor::className(), [
                'options' => ['rows' => 1],
                'preset'  => 'standart',
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($model, 'brand_id')->dropDownList(
                Brand::find()->byStatus()->orderBy('name')->map()->all()
            ) ?>
        </div>
        <div class="col-md-9">
            <?= $form->field($model, 'seller_id')->dropDownList(
                Seller::find()->byStatus()->orderBy('name')->map()->all()
            ) ?>
        </div>
    </div>

    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'year')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'sex')->dropDownList($model->sexes) ?>

    <?= $form->field($model, 'on_index')->checkbox() ?>

</div>