<?php
/* @var $this yii\web\View */
use emilasp\seo\backend\widgets\SeoForm\SeoForm;

/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="seo" class="tab-pane fade clearfix">
    <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
</div>