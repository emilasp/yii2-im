<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('im', 'Product'),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('im', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('im', 'Update');
?>
<div class="product-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
