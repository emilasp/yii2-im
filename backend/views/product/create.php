<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Product */

$this->title = Yii::t('im', 'Create product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('im', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
