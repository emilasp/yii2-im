<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Client */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('im', 'Client'),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('im', 'Clients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="client-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
