<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use emilasp\user\core\models\User;

/* @var $this yii\web\View */
/* @var $searchModel emilasp\im\common\models\search\ClientSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = Yii::t('im', 'Clients');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index">

    <?php Pjax::begin(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => '\kartik\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'class'     => '\kartik\grid\DataColumn',
                'width'     => '100px',
                'hAlign'    => GridView::ALIGN_CENTER,
                'vAlign'    => GridView::ALIGN_MIDDLE,
            ],
            'name',
            'lastname',
            'surname',
            [
                'attribute' => 'region',
                'class'     => '\kartik\grid\DataColumn',
                'value'     => function ($model) {
                    return null;
                    if (!$model->regionModel) {
                        return null;
                    }
                    return $model->regionModel->formalname;
                },
                'format'    => 'raw',
                'hAlign'    => GridView::ALIGN_CENTER,
                'vAlign'    => GridView::ALIGN_MIDDLE,
            ],
            'phone:ntext',
            'address:ntext',
            [
                'attribute'           => 'manager_id',
                'value'               => function ($model) {
                    if (!$model->manager) {
                        return null;
                    }
                    return $model->manager->username;
                },
                'class'               => '\kartik\grid\DataColumn',
                'hAlign'              => GridView::ALIGN_LEFT,
                'vAlign'              => GridView::ALIGN_MIDDLE,
                'width'               => '150px',
                'filterType'          => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'language'      => \Yii::$app->language,
                    'data'          => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                    'options'       => ['placeholder' => '-выбрать-'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'attribute' => 'status',
                'value'     => function ($model, $key, $index, $column) {
                    return $model->statuses[$model->status];
                },
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '150px',
                'filter'    => $searchModel->statuses,
            ],
            [
                'attribute'           => 'created_by',
                'value'               => function ($model) {
                    return $model->createdBy->username;
                },
                'class'               => '\kartik\grid\DataColumn',
                'hAlign'              => GridView::ALIGN_LEFT,
                'vAlign'              => GridView::ALIGN_MIDDLE,
                'width'               => '150px',
                'filterType'          => GridView::FILTER_SELECT2,
                'filterWidgetOptions' => [
                    'language'      => \Yii::$app->language,
                    'data'          => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                    'options'       => ['placeholder' => '-выбрать-'],
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
            ],
            [
                'attribute' => 'created_at',
                'class'     => '\kartik\grid\DataColumn',
                'hAlign'    => GridView::ALIGN_LEFT,
                'vAlign'    => GridView::ALIGN_MIDDLE,
                'width'     => '250px',
            ],
            [
                'class' => '\kartik\grid\ActionColumn',
            ],
        ],
        'responsive'   => true,
        'hover'        => true,
        'condensed'    => true,
        'floatHeader'  => true,
        'panel'        => [
            'heading'    => '<h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i> '
                            . Html::encode($this->title) . ' </h3>',
            'type'       => 'info',
            'before'     => Html::a(
                '<i class="glyphicon glyphicon-plus"></i> ' . Yii::t('site', 'Add'),
                ['create'],
                ['class' => 'btn btn-success']
            ),
            'after'      => Html::a(
                '<i class="glyphicon glyphicon-repeat"></i> Reset List',
                ['index'],
                ['class' => 'btn btn-info']
            ),
            'showFooter' => false,
        ],
    ]);
    ?>

    <?php Pjax::end(); ?>

</div>
