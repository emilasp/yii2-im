<?php
/* @var $this yii\web\View */
use emilasp\geoapp\widgets\AddressWidget\AddressWidget;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;
use yii\widgets\MaskedInput;

?>
<div id="contacts" class="tab-pane fade clearfix">

    <h2>contacts</h2>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'phone')
                     ->widget(MaskedInput::className(), ['mask' => '+7 (999) 999-99-99']) ?>
        </div>
        <div class="col-md-6">
            <?= DynamicFields::widget([
                'form'        => $form,
                'model'       => $model,
                'attribute'   => 'address',
                'scheme'      => DynamicModel::SCHEME_ADDRESS,
                'addEmptyRow' => true,
            ]); ?>
        </div>
    </div>
</div>
