<?php
use dosamigos\ckeditor\CKEditor;
use emilasp\geoapp\models\GeoAddrobj;
use emilasp\geoapp\widgets\SelectCityWidget\SelectCityWidget;
use kartik\widgets\Select2;
use emilasp\user\backend\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="base" class="tab-pane fade in active clearfix">

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'lastname')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'surname')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'id')->textInput() ?>

            <?= $form->field($model, 'manager_id')->widget(Select2::classname(), [
                'language' => 'ru',
                'data' => ArrayHelper::map(
                    User::findAll(['role' => ['admin', 'manager']]),
                    'id',
                    'username'
                ),
                'options' => ['placeholder' => 'Выберите  менеджера..'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]) ?>
            <hr />

            <?= $form->field($model, 'city_id')->widget(SelectCityWidget::className()) ?>
            <hr />

            <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
        </div>
        <div class="col-md-6">



            <?= $form->field($model, 'comment')->widget(CKEditor::className(), [
                'options' => ['rows' => 1],
                'preset'  => 'standart',
            ]) ?>
        </div>
    </div>

</div>