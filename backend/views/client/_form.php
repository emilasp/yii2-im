<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Client */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="client-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('im', 'Tab base') ?></a></li>
        <li><a data-toggle="tab" href="#contacts"><?= Yii::t('im', 'Tab contacts') ?></a></li>
        <li><a data-toggle="tab" href="#orders"><?= Yii::t('im', 'Tab orders') ?></a></li>
        <li><a data-toggle="tab" href="#history"><?= Yii::t('im', 'Tab history') ?></a></li>
    </ul>

    <div class="tab-content">
        <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_contacts', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_orders', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_history', ['form' => $form, 'model' => $model]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
