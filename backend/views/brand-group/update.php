<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\BrandGroup */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('im', 'Brand group'),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('im', 'Brand groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="brand-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
