<?php

use emilasp\files\extensions\FileManager\FileManager;
use emilasp\files\extensions\imageInputWidget\ImageInputWidget;
use emilasp\im\common\models\Brand;
use emilasp\seo\backend\widgets\SeoForm\SeoForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Brand */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="brand-form">

    <?php $form = ActiveForm::begin([
        'id'      => 'im-brand-form-id',
        'options' => ['enctype' => 'multipart/form-data'],
    ]); ?>

    <?php echo $form->errorSummary($model); ?>


    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('taxonomy', 'Value') ?></a></li>
        <li><a data-toggle="tab" href="#seo"><?= Yii::t('seo', 'Tab Seo') ?></a></li>
    </ul>

    <div class="tab-content">
        <div id="base" class="tab-pane fade in active">
 
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'brand_id')->dropDownList(Brand::find()->map()->all()) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'text')->textarea(['rows' => 6]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'short_text')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Yii::t('site', 'Logo') ?></h3>
                </div>
                <div class="panel-body">
                    <?= ImageInputWidget::widget([
                        'form'  => $form,
                        'model' => $model,
                    ]) ?>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= Yii::t('site', 'Banners') ?></h3>
                </div>
                <div class="panel-body">
                    <?= FileManager::widget(['model' => $model, 'attribute' => 'banners']) ?>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'status')->dropDownList($model->statuses) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'order')->textInput(['maxlength' => true]) ?>
                </div>
            </div>
 
        </div>
        <div id="seo" class="tab-pane fade">
            <?= SeoForm::widget(['form' => $form, 'model' => $model]) ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
