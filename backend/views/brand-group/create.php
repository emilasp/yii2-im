<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Brand */

$this->title = Yii::t('im', 'Create Brand group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('im', 'Brand groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
