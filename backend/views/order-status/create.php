<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\OrderStatus */

$this->title = Yii::t('im', 'Create Order Status');
$this->params['breadcrumbs'][] = ['label' => Yii::t('im', 'Order Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
