<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\OrderStatus */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('im', 'Order Status'),
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('im', 'Order Statuses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="order-status-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
