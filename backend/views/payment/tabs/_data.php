<?php
/* @var $this yii\web\View */
use emilasp\geoapp\widgets\AddressWidget\AddressWidget;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;

?>
<div id="data" class="tab-pane fade clearfix">

    <h2><?= Yii::t('site', 'Options') ?></h2>

    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><?= Yii::t('site', 'Cities') ?></div>
                <div class="panel-body">
                    <?= DynamicFields::widget([
                        'form'      => $form,
                        'model'     => $model,
                        'attribute' => 'city',
                        'scheme'    => DynamicModel::SCHEME_CITY,
                    ]); ?>

                    <?= DynamicFields::widget([
                        'form'      => $form,
                        'model'     => $model,
                        'attribute' => 'city_exclude',
                        'scheme'    => DynamicModel::SCHEME_CITY,
                    ]); ?>
                </div>
            </div>

        </div>
        <div class="col-md-6">

            <?= DynamicFields::widget([
                'form'      => $form,
                'model'     => $model,
                'attribute' => 'provider',
                'scheme'    => DynamicModel::SCHEME_HANDLER,
            ]); ?>

        </div>
    </div>
</div>
