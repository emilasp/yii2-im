<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#base"><?= Yii::t('im', 'Tab base') ?></a></li>
        <li><a data-toggle="tab" href="#products"><?= Yii::t('im', 'Tab products') ?></a></li>
        <li><a data-toggle="tab" href="#contacts"><?= Yii::t('im', 'Tab contacts') ?></a></li>
        <li><a data-toggle="tab" href="#delivery"><?= Yii::t('im', 'Tab delivery') ?></a></li>
        <li><a data-toggle="tab" href="#payment"><?= Yii::t('im', 'Tab payment') ?></a></li>
        <li><a data-toggle="tab" href="#history"><?= Yii::t('im', 'Tab history') ?></a></li>
    </ul>

    <div class="tab-content">
        <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_products', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_contacts', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_delivery', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_payment', ['form' => $form, 'model' => $model]) ?>
        <?= $this->render('tabs/_history', ['form' => $form, 'model' => $model]) ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton(
            $model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Update'),
            ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']
        ) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
