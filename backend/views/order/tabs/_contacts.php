<?php
/* @var $this yii\web\View */
use emilasp\geoapp\widgets\AddressWidget\AddressWidget;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;
use emilasp\geoapp\widgets\SelectCityWidget\SelectCityWidget;
use yii\widgets\MaskedInput;

/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="contacts" class="tab-pane fade clearfix">

    <h2><?= Yii::t('site', 'Contacts') ?></h2>

    <?= $form->field($model, 'client_name')->textInput() ?>
    <?= $form->field($model, 'client_lastname')->textInput() ?>
    <?= $form->field($model, 'email')->textInput() ?>

    <?= $form->field($model, 'city_id')->widget(SelectCityWidget::className()) ?>

    <div class="row">
        <div class="col-md-6">

            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><?= Yii::t('site', 'Phone') ?></div>
                <div class="panel-body">
                    <?= $form->field($model, 'phone')
                             ->widget(MaskedInput::className(), ['mask' => '+7 (999) 999-99-99']) ?>
                    <?= $form->field($model, 'phone_other')
                             ->widget(MaskedInput::className(), ['mask' => '+7 (999) 999-99-99']) ?>
                </div>
            </div>

        </div>
        <div class="col-md-6">
            <?= DynamicFields::widget([
                'form'        => $form,
                'model'       => $model,
                'attribute'   => 'address',
                'scheme'      => DynamicModel::SCHEME_ADDRESS,
                'addEmptyRow' => true,
            ]); ?>
        </div>
    </div>

</div>
