<?php
/* @var $this yii\web\View */
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;
use yii\web\JsExpression;

/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="history" class="tab-pane fade clearfix">

    <h2>History</h2>

    <?= $form->field($model, 'history')->textInput() ?>

</div>