<?php
/* @var $this yii\web\View */
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;
use yii\web\JsExpression;

/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="delivery" class="tab-pane fade clearfix">

    <h2>Delivery</h2>

    <?= $form->field($model, 'delivery_at')->textInput() ?>

    <?= $form->field($model, 'delivery_id')->textInput() ?>

    <?= $form->field($model, 'delivery_data')->textInput() ?>

</div>
