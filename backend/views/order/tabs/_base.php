<?php
use dosamigos\ckeditor\CKEditor;
use emilasp\im\common\models\OrderStatus;
use kartik\widgets\Select2;
use emilasp\user\backend\models\User;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="base" class="tab-pane fade in active clearfix">

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'type')->dropDownList($model->types) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'sum')->textInput() ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'status_id')->widget(Select2::classname(), [
                'language'      => 'ru',
                'data'          => ArrayHelper::map(
                    OrderStatus::find()->byStatus()->all(),
                    'id',
                    'name'
                ),
                'options'       => ['placeholder' => 'Выберите  статус..'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'user_id')->widget(Select2::classname(), [
                'language'      => 'ru',
                'data'          => ArrayHelper::map(
                    User::findAll(['role' => ['client', 'admin', 'manager']]),
                    'id',
                    'username'
                ),
                'options'       => ['placeholder' => 'Выберите  клиента..'],
                'pluginOptions' => [
                    'allowClear' => true,
                ],
            ]) ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'comment_client')->textarea(['rows' => 6]) ?>

            <?= $form->field($model, 'comment_manager')->textarea(['rows' => 6]) ?>
        </div>
    </div>

</div>