<?php
/* @var $this yii\web\View */
use emilasp\geoapp\widgets\AddressWidget\AddressWidget;
use emilasp\im\backend\widgets\ImOrderProducts\ImOrderProducts;
use emilasp\geoapp\widgets\SelectCityWidget\SelectCityWidget;

?>
<div id="products" class="tab-pane fade clearfix">

    <h2><?= Yii::t('im', 'Products') ?></h2>

    <?= ImOrderProducts::widget(['order' => $model]) ?>
</div>
