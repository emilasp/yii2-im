<?php
/* @var $this yii\web\View */
use emilasp\taxonomy\extensions\CategorySelectTree\CategorySelectTree;
use yii\web\JsExpression;

/* @var $model emilasp\im\common\models\Good */
/* @var $form yii\widgets\ActiveForm */
?>
<div id="payment" class="tab-pane fade clearfix">

    <h2><?= Yii::t('im', 'Payment') ?></h2>

    <?= $form->field($model, 'paid')->checkbox() ?>

    <?= $form->field($model, 'payment_id')->textInput() ?>

    <?= $form->field($model, 'payment_data')->textInput() ?>

</div>
