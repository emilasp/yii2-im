<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Order */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
    'modelClass' => Yii::t('im', 'Order'),
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('im', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Update');
?>
<div class="order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
