<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\im\common\models\Order */

$this->title = Yii::t('im', 'Create Order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('im', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
