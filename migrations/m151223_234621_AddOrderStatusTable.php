<?php

use emilasp\im\common\models\OrderStatus;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151223_234621_AddOrderStatusTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    public function up()
    {
        $this->createTable('im_order_status', [
            'id'         => $this->primaryKey(11),
            'code'       => $this->string(50)->notNull(),
            'name'       => $this->string(255)->notNull(),
            'events'     => 'jsonb NULL DEFAULT \'{}\'',
            'fields'     => 'jsonb NULL DEFAULT \'{}\'',
            'status'     => $this->smallInteger(1)->notNull(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('im_order_status');

        $this->afterMigrate();
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
