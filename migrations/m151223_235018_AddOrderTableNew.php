<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151223_235018_AddOrderTableNew extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('im_order', [
            'id'              => $this->primaryKey(11),
            'type'            => $this->smallInteger(1)->notNull(),
            'user_id'         => $this->integer(11)->notNull(),
            'city_id'         => $this->integer(11)->notNull(),
            'client_name'     => $this->string(50),
            'client_lastname' => $this->string(50),
            'email'           => $this->string(100),
            'address'         => $this->text(),
            'phone'           => $this->string(10)->notNull(),
            'phone_other'     => $this->string(10),
            'delivery_at'     => $this->dateTime(),
            'delivery_id'     => $this->integer(11)->notNull(),
            'delivery_data'   => 'jsonb NULL DEFAULT \'{}\'',
            'payment_id'      => $this->integer(11)->notNull(),
            'payment_data'    => 'jsonb NULL DEFAULT \'{}\'',
            'sum'             => $this->decimal(12, 2)->notNull(),
            'comment_client'  => $this->text(),
            'comment_manager' => $this->text(),
            'paid'            => $this->boolean(),
            'status_id'       => $this->smallInteger(1)->notNull(),
            'history'         => 'jsonb NULL DEFAULT \'{}\'',
            'created_at'      => $this->dateTime(),
            'updated_at'      => $this->dateTime(),
            'created_by'      => $this->integer(11),
            'updated_by'      => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_im_order_delivery_id',
            'im_order',
            'payment_id',
            'im_payment',
            'id'
        );

        $this->addForeignKey(
            'fk_im_order_payment_id',
            'im_order',
            'payment_id',
            'im_payment',
            'id'
        );

        $this->addForeignKey(
            'fk_im_order_status_id',
            'im_order',
            'status_id',
            'im_order_status',
            'id'
        );

        $this->addForeignKey(
            'fk_im_order_user_id',
            'im_order',
            'user_id',
            'users_user',
            'id'
        );

        /*$this->addForeignKey(
            'fk_im_order_city_id',
            'im_order',
            'city_id',
            'users_user',
            'id'
        );*/

        $this->addForeignKey(
            'fk_im_order_created_by',
            'im_order',
            'created_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_im_order_updated_by',
            'im_order',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createIndex('im_order_city_id', 'im_order', 'city_id');
        $this->createIndex('im_order_user_id', 'im_order', 'user_id');
        $this->createIndex('im_order_delivery', 'im_order', ['delivery_id', 'delivery_at']);
        $this->createIndex('im_order_payment_id', 'im_order', 'payment_id');

        $this->addClientStatuses();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('im_order');

        $this->afterMigrate();
    }

    private function addClientStatuses()
    {
        Variety::add('order_type', 'order_type_cart', 'Корзина', 1, 1);
        Variety::add('order_type', 'order_type_inline', 'Инлайн', 2, 2);
        Variety::add('order_type', 'order_type_offline', 'Оффлайн', 3, 3);
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
