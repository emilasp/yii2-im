<?php

use emilasp\im\common\models\Product;
use emilasp\taxonomy\models\Property;
use emilasp\taxonomy\models\PropertyLink;
use emilasp\taxonomy\models\PropertyValue;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160502_183048_AddBrandGroupTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('im_brand_group', [
            'id'         => $this->primaryKey(11),
            'brand_id'   => $this->integer(11),
            'name'       => $this->string(255)->notNull(),
            'text'       => $this->text(),
            'short_text' => $this->text(),
            'image_id'   => $this->integer(11),
            'status'     => $this->smallInteger(1)->notNull(),
            'order'      => $this->smallInteger(1),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_im_brand_group_brand_id',
            'im_brand_group',
            'brand_id',
            'im_brand',
            'id'
        );

        $this->addForeignKey(
            'fk_im_brand_group_image_id',
            'im_brand_group',
            'image_id',
            'files_file',
            'id'
        );

        $this->addForeignKey(
            'fk_im_brand_group_created_by',
            'im_brand_group',
            'created_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_im_brand_group_updated_by',
            'im_brand_group',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addProductColumnBrandGroup();
        $this->db->schema->refresh();
        $this->addGroups();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropColumn('im_product', 'brand_group_id');
        $this->dropTable('im_brand_group');

        $this->afterMigrate();
    }

    private function addProductColumnBrandGroup()
    {
        $this->addColumn('im_product', 'brand_group_id', 'integer');
        $this->addForeignKey(
            'fk_im_product_brand_group_id',
            'im_product',
            'brand_group_id',
            'im_brand_group',
            'id'
        );
    }

    private function addGroups()
    {
        $property = Property::findOne(['name' => 'Модель']);
        $products = Product::find()->all();
        $groups   = [];
        foreach ($products as $product) {
            $link = PropertyLink::findOne([
                'property_id' => $property->id,
                'object'      => $product::className(),
                'object_id'   => $product->id,
            ]);

            if ($link) {
                $value = PropertyValue::findOne(['id' => $link->value_id]);

                if (!isset($groups[$value->value])) {
                    $sql = <<<SQL
            INSERT INTO im_brand_group (name, text, short_text, brand_id, image_id, "order", status, created_at, updated_at, created_by, updated_by) VALUES ('{$value->value}', '', '', {$product->brand_id}, null, 1, 1, NOW(), NOW(), 1, 1);
SQL;
                    $this->db->createCommand($sql)->execute();
                    $groups[$value->value] = $this->db->createCommand('SELECT max(id) FROM im_brand_group')->queryScalar();
                    echo 'Insert(' . $product->brand_group_id . ') ' . $value->value . PHP_EOL;
                }

                $product->brand_group_id = $groups[$value->value];
                $product->save();

                echo 'Update product ' . $product->id . ' ' . $groups[$value->value] . PHP_EOL;

                //$value->delete();

            }
        }
        //$property->delete();
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
