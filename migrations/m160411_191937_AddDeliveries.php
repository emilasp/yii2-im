<?php

use emilasp\im\common\models\Delivery;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160411_191937_AddDeliveries extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $dataParams = [
            [
                'code'      => Delivery::CODE_COURIER,
                'name'      => 'Курьерская доставка',
                'text'      => 'Курьерская доставка',
                'text_free' => 'Бесплатная доставка',
                'order'     => 1,
                'status'    => 1,
            ],
            [
                'code'      => Delivery::CODE_POSTAL,
                'name'      => 'Почтой(наложным платежом)',
                'text'      => 'Почтой(наложным платежом)',
                'text_free' => '',
                'order'     => 2,
                'status'    => 1,
            ],
            [
                'code'      => Delivery::CODE_EMS,
                'name'      => 'Почтой(EMS)',
                'text'      => 'Почтой(EMS)',
                'text_free' => '',
                'order'     => 3,
                'status'    => 0,
            ],
        ];

        foreach ($dataParams as $data) {
            $model = new Delivery($data);
            if (!$model->save()) {
                var_dump($model->getErrors());
            }
        }

        $this->afterMigrate();
    }

    public function down()
    {

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
