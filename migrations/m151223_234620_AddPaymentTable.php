<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151223_234620_AddPaymentTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('im_payment', [
            'id'               => $this->primaryKey(11),
            'code'             => $this->string(50)->notNull(),
            'name'             => $this->string(255)->notNull(),
            'text'             => $this->text(),
            'icon'             => $this->string(255),
            'city'             => 'jsonb NULL DEFAULT \'{}\'',
            'city_exclude'     => 'jsonb NULL DEFAULT \'{}\'',
            'provider'         => 'jsonb NULL DEFAULT \'{}\'',
            'field'            => 'jsonb NULL DEFAULT \'{}\'',
            'extracharge_type' => $this->smallInteger(1)->notNull(),
            'extracharge'      => $this->integer(4),
            'order'            => $this->smallInteger(2),
            'status'           => $this->smallInteger(1)->notNull(),
            'created_at'       => $this->dateTime(),
            'updated_at'       => $this->dateTime(),
            'created_by'       => $this->integer(11),
            'updated_by'       => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_im_payment_created_by',
            'im_payment',
            'created_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_im_payment_updated_by',
            'im_payment',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addExtrachargeTypes();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('im_payment');

        $this->afterMigrate();
    }

    /**
     * Добавляем наценки
     */
    private function addExtrachargeTypes()
    {
        Variety::add('extracharge_type', 'extracharge_type_fix', 'Фикс', 1, 1);
        Variety::add('extracharge_type', 'extracharge_type_percent', 'Процент', 2, 2);
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
