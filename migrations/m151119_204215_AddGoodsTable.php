<?php

use yii\db\Migration;
use emilasp\variety\models\Variety;
use emilasp\core\helpers\FileHelper;

class m151119_204215_AddGoodsTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('im_product', [
            'id'           => $this->primaryKey(11),
            'seller_id'    => $this->integer(11),
            'brand_id'     => $this->integer(11),
            'parent_id'    => $this->integer(11),
            'article'      => $this->string(255)->notNull(),
            'name'         => $this->string(255)->notNull(),
            'text'         => $this->text(),
            'short_text'   => $this->text(),
            'cost'         => $this->decimal(12, 2),
            'cost_base'    => $this->decimal(12, 2),
            'quantity'     => $this->integer(10),
            'discount'     => $this->smallInteger(2),
            'unit'         => $this->smallInteger(2),
            'weight'       => $this->smallInteger(5),
            'country'      => $this->string(50),
            'year'         => $this->smallInteger(4),
            'sex'          => $this->smallInteger(1),
            'on_request'   => $this->smallInteger(1),
            'variation_id' => $this->integer(11),
            'type'         => $this->smallInteger(1)->notNull(),
            'status'       => $this->smallInteger(1)->notNull(),
            'created_at'   => $this->dateTime(),
            'updated_at'   => $this->dateTime(),
            'created_by'   => $this->integer(11),
            'updated_by'   => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_im_product_brand_id',
            'im_product',
            'brand_id',
            'im_brand',
            'id'
        );

        $this->addForeignKey(
            'fk_im_product_seller_id',
            'im_product',
            'seller_id',
            'im_seller',
            'id'
        );

        $this->addForeignKey(
            'fk_im_product_parent_id',
            'im_product',
            'parent_id',
            'im_product',
            'id'
        );

        $this->addForeignKey(
            'fk_im_product_variation_id',
            'im_product',
            'variation_id',
            'taxonomy_property_value',
            'id'
        );

        $this->addForeignKey(
            'fk_im_product_created_by',
            'im_product',
            'created_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_im_product_updated_by',
            'im_product',
            'updated_by',
            'users_user',
            'id'
        );

        $this->createIndex('im_product_article', 'im_product', 'article', true);
        $this->createIndex('im_product_parent_id', 'im_product', 'parent_id', true);
        $this->createIndex('im_product_status_cost', 'im_product', ['status', 'cost']);
        $this->createIndex('im_product_status_type', 'im_product', ['status', 'type']);

        $this->addProductTypes();
        $this->addProductSex();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('im_product');

        $this->afterMigrate();
    }

    private function addProductTypes()
    {
        Variety::add('product_type', 'product_type_simple', 'Обычный', 1, 1);
        Variety::add('product_type', 'product_type_variate', 'Вариативный', 2, 2);
        Variety::add('product_type', 'product_type_variate_pseudo', 'Вариативный(псевдо)', 3, 3);
        Variety::add('product_type', 'product_type_virtual', 'Виртуальный', 4, 4);
        Variety::add('product_type', 'product_type_set', 'Набор', 5, 5);
    }

    private function addProductSex()
    {
        Variety::add('product_sex', 'product_sex_man', 'Мужское', 1, 1);
        Variety::add('product_sex', 'product_sex_women', 'Женское', 2, 2);
        Variety::add('product_sex', 'product_sex_unisex', 'Унисекс', 3, 3);
        Variety::add('product_sex', 'product_sex_men_child', 'Детское(мужское)', 4, 4);
        Variety::add('product_sex', 'product_sex_women_child', 'Детское(женское)', 5, 5);
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
