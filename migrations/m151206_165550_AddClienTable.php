<?php

use emilasp\variety\models\Variety;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151206_165550_AddClienTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('im_client', [
            'id'         => $this->primaryKey(11),
            'phone'      => $this->string(10)->unique()->notNull(),
            'manager_id' => $this->integer(11),
            'comment'    => $this->text(),
            'name'       => $this->string(255)->notNull(),
            'lastname'   => $this->string(255)->notNull(),
            'surname'    => $this->string(255),
            'address'    => 'jsonb NULL DEFAULT \'[]\'',
            'city_id'    => $this->integer(11)->notNull(),
            'status'     => $this->smallInteger(1)->notNull(),
            'history'    => 'jsonb NULL DEFAULT \'[]\'',
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
            'created_by' => $this->integer(11),
            'updated_by' => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_im_client_user_id',
            'im_client',
            'id',
            'users_user',
            'id'
        );

        /*$this->addForeignKey(
            'fk_im_client_region',
            'im_client',
            'region',
            'geo_addrobj',
            'aoid'
        );*/


        $this->addForeignKey(
            'fk_im_client_manager_id',
            'im_client',
            'manager_id',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_im_client_created_by',
            'im_client',
            'created_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_im_client_updated_by',
            'im_client',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addClientStatuses();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('im_client');

        $this->afterMigrate();
    }

    private function addClientStatuses()
    {
        Variety::add('client_status', 'client_status_new', 'Новый', 1, 1);
        Variety::add('client_status', 'client_status_active', 'Активный', 2, 2);
        Variety::add('client_status', 'client_status_no_active', 'Не активный', 3, 3);
        Variety::add('client_status', 'client_status_banned', 'Забанен', 4, 4);
        Variety::add('client_status', 'client_status_deleted', 'Удалён', 5, 5);
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
