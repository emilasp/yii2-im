<?php

use emilasp\core\helpers\StringHelper;
use emilasp\im\common\models\Brand;
use emilasp\im\common\models\Product;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160424_185224_UpdateArticul extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $products = Product::findAll(['status' => [0,1]]);

        foreach ($products as $index => $product) {
            $product->article = $this->getArticle($product);
            $product->save();

            echo $index;
            echo ') ' . $product->article . PHP_EOL;
        }
        $this->afterMigrate();
    }

    private function getArticle(Product $product)
    {
        $prefix = StringHelper::rus2translit($product->brand->name);
        $prefix = preg_replace('/(e|y|u|i|o|a)/ui', '', $prefix);
        $prefix = mb_strtoupper(mb_substr($prefix, 0, 2));

        return $prefix . '-' . sprintf("%05d", $product->id);
    }

    public function down()
    {

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
