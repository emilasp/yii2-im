<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160406_143801_AddTablePayment extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('im_pay', [
            'id'         => $this->primaryKey(10),
            'project_id' => $this->string(255)->notNull(),
            'account'    => $this->string(255)->notNull(),
            'sum'        => $this->float()->notNull(),
            'count'      => $this->integer(11)->notNull()->defaultValue(1),
            'created'    => $this->dateTime()->notNull(),
            'completed'  => $this->dateTime()->notNull(),
            'status'     => $this->integer(4)->notNull()->defaultValue(0),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ], $this->tableOptions);

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('im_pay');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
