<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151128_104757_AddBaseDataForIm extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        /** Category */
        $category             = new \emilasp\taxonomy\models\Category();
        $category->code       = 'products';
        $category->name       = 'Каталог товаров';
        $category->text       = 'Каталог для товаров интернет магазина';
        $category->short_text = 'Каталог для товаров ';
        $category->title      = 'Каталог для товаров ';
        $category->type       = 1;
        $category->status     = 1;
        $category->created_by = 1;
        $category->updated_by = 1;
        $category->makeRoot();

        /*$category11             = new \emilasp\taxonomy\models\Category();
        $category11->name       = 'Обувь';
        $category11->text       = 'Обувь';
        $category11->short_text = 'Обувь';
        $category11->title      = 'Обувь';
        $category11->type       = 1;
        $category11->status     = 1;
        $category->created_by = 1;
        $category->updated_by = 1;
        $category11->prependTo($category);

        $category111             = new \emilasp\taxonomy\models\Category();
        $category111->name       = 'Кроссовки';
        $category111->text       = 'Кроссовки';
        $category111->short_text = 'Кроссовки';
        $category111->title      = 'Кроссовки';
        $category111->type       = 1;
        $category111->status     = 1;
        $category->created_by = 1;
        $category->updated_by = 1;
        $category111->prependTo($category11);

        $category112             = new \emilasp\taxonomy\models\Category();
        $category112->name       = 'Кеды';
        $category112->text       = 'Кеды';
        $category112->short_text = 'Кеды';
        $category112->title      = 'Кеды';
        $category112->type       = 1;
        $category112->status     = 1;
        $category->created_by = 1;
        $category->updated_by = 1;
        $category112->prependTo($category11);

        $category113             = new \emilasp\taxonomy\models\Category();
        $category113->name       = 'Ботинки';
        $category113->text       = 'Ботинки';
        $category113->short_text = 'Ботинки';
        $category113->title      = 'Ботинки';
        $category113->type       = 1;
        $category113->status     = 1;
        $category->created_by = 1;
        $category->updated_by = 1;
        $category113->prependTo($category11);

        $category1             = new \emilasp\taxonomy\models\Category();
        $category1->name       = 'Куртки';
        $category1->text       = 'Куртки';
        $category1->short_text = 'Куртки';
        $category1->title      = 'Куртки';
        $category1->type       = 1;
        $category1->status     = 1;
        $category->created_by = 1;
        $category->updated_by = 1;
        $category1->prependTo($category);

        $category11             = new \emilasp\taxonomy\models\Category();
        $category11->name       = 'Костюмы';
        $category11->text       = 'Костюмы';
        $category11->short_text = 'Костюмы';
        $category11->title      = 'Костюмы';
        $category11->type       = 1;
        $category11->status     = 1;
        $category->created_by = 1;
        $category->updated_by = 1;
        $category11->prependTo($category);

        $category12             = new \emilasp\taxonomy\models\Category();
        $category12->name       = 'Платья';
        $category12->text       = 'Платья';
        $category12->short_text = 'Платья';
        $category12->title      = 'Платья';
        $category12->type       = 1;
        $category12->status     = 1;
        $category->created_by = 1;
        $category->updated_by = 1;
        $category12->prependTo($category);

        $category13             = new \emilasp\taxonomy\models\Category();
        $category13->name       = 'Пуховики';
        $category13->text       = 'Пуховики';
        $category13->short_text = 'Пуховики';
        $category13->title      = 'Пуховики';
        $category13->type       = 1;
        $category13->status     = 1;
        $category->created_by = 1;
        $category->updated_by = 1;
        $category13->prependTo($category1);*/

        /** @var  $category */
        $category             = new \emilasp\taxonomy\models\Category();
        $category->code       = 'posts';
        $category->name       = 'Каталог статей';
        $category->text       = 'Каталог для статей';
        $category->short_text = 'Каталог для статей';
        $category->title      = 'Каталог для статей';
        $category->type       = 1;
        $category->status     = 1;
        $category->created_by = 1;
        $category->updated_by = 1;
        $category->makeRoot();

        /** Property groups */ //Пол, цвет, размер(?), состав

        /** Свойства */
        
/*        $property = new \emilasp\taxonomy\models\Property();
        $property->name = 'Цвет';
        $property->type = 3;
        $property->filter_type_view = 1;
        $property->group_id = 1;
        $property->status = 1;
        $property->save();

        $value = new \emilasp\taxonomy\models\PropertyValue();
        $value->value = 'Белый';
        $value->data = '#fff';
        $value->property_id = 2;
        $value->order = 1;
        $value->save();

        $value = new \emilasp\taxonomy\models\PropertyValue();
        $value->value = 'Чёрный';
        $value->data = '#000';
        $value->property_id = 2;
        $value->order = 2;
        $value->save();

        $value = new \emilasp\taxonomy\models\PropertyValue();
        $value->value = 'Красный';
        $value->data = 'red';
        $value->property_id = 2;
        $value->order = 3;
        $value->save();

        $value = new \emilasp\taxonomy\models\PropertyValue();
        $value->value = 'Синий';
        $value->data = 'blue';
        $value->property_id = 2;
        $value->order = 4;
        $value->save();

        $property = new \emilasp\taxonomy\models\Property();
        $property->name = 'Размер';
        $property->type = 1;
        $property->filter_type_view = 1;
        $property->is_variation = 1;
        $property->group_id = 1;
        $property->status = 1;
        $property->save();

        $value = new \emilasp\taxonomy\models\PropertyValue();
        $value->value = 'XXL';
        $value->data = '';
        $value->property_id = 3;
        $value->order = 1;
        $value->save();
        $value = new \emilasp\taxonomy\models\PropertyValue();
        $value->value = 'XL';
        $value->data = '';
        $value->property_id = 3;
        $value->order = 2;
        $value->save();
        $value = new \emilasp\taxonomy\models\PropertyValue();
        $value->value = 'L';
        $value->data = '';
        $value->property_id = 3;
        $value->order = 3;
        $value->save();
        $value = new \emilasp\taxonomy\models\PropertyValue();
        $value->value = 'M';
        $value->data = '';
        $value->property_id = 3;
        $value->order = 4;
        $value->save();
        $value = new \emilasp\taxonomy\models\PropertyValue();
        $value->value = 'S';
        $value->data = '';
        $value->property_id = 3;
        $value->order = 5;
        $value->save();
        $value = new \emilasp\taxonomy\models\PropertyValue();
        $value->value = 'XS';
        $value->data = '';
        $value->property_id = 3;
        $value->order = 6;
        $value->save();*/

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('files_file');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
