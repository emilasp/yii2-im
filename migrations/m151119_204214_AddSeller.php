<?php

use yii\db\Migration;
use emilasp\variety\models\Variety;
use emilasp\core\helpers\FileHelper;

class m151119_204214_AddSeller extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('im_seller', [
            'id'           => $this->primaryKey(11),
            'name'         => $this->string(255)->notNull(),
            'text'         => $this->text(),
            'status'       => $this->smallInteger(1)->notNull(),
            'created_at'   => $this->dateTime(),
            'updated_at'   => $this->dateTime(),
            'created_by'   => $this->integer(11),
            'updated_by'   => $this->integer(11),
        ], $this->tableOptions);


        $this->addForeignKey(
            'fk_im_seller_created_by',
            'im_seller',
            'created_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_im_seller_updated_by',
            'im_seller',
            'updated_by',
            'users_user',
            'id'
        );

        $sqlAddSeller = <<<SQL
      INSERT INTO im_seller (name, text, status, created_at, updated_at, created_by, updated_by)
      VALUES ('Тестовый', 'Тестовый', 1, NOW(), NOW(), 1, 1);
SQL;

        Yii::$app->db->createCommand($sqlAddSeller)->execute();

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('im_seller');

        $this->afterMigrate();
    }

    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
