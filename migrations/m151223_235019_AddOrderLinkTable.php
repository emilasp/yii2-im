<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m151223_235019_AddOrderLinkTable extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $this->createTable('im_order_product', [
            'id'         => $this->primaryKey(11),
            'order_id'   => $this->integer(11)->notNull(),
            'product_id' => $this->integer(11)->notNull(),
            'variations' => 'jsonb NULL DEFAULT \'{}\'',
            'quantity'   => $this->integer(5),
            'cost'       => $this->decimal(12, 2),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_im_order_product_order_id',
            'im_order_product',
            'order_id',
            'im_order',
            'id'
        );

        $this->addForeignKey(
            'fk_im_order_product_product_id',
            'im_order_product',
            'product_id',
            'im_product',
            'id'
        );

        $this->createIndex('im_order_product_order_id', 'im_order_product', 'order_id');
        $this->createIndex('im_order_product_product_id', 'im_order_product', 'product_id');

        $this->afterMigrate();
    }

    public function down()
    {
        $this->dropTable('im_order_product');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
