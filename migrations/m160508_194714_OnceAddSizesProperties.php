<?php

use emilasp\im\common\models\Product;
use emilasp\taxonomy\models\Property;
use emilasp\taxonomy\models\PropertyLink;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160508_194714_OnceAddSizesProperties extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        ini_set('memory_limit', '-1');

        $property = Property::findOne(['name' => 'Размер']);

        $valueReplace = [];
        foreach ($property->values as $value) {
            if (strpos($value->value, 'EUR') !== false) {
                $name = (int)$value->value;
                foreach ($property->values as $valueNew) {
                    if (trim($valueNew->value) == $name) {
                        $valueReplace[$value->id] = $value;
                        break;
                    }
                }
            }
        }

        foreach ($valueReplace as $id => $value) {
            $links = PropertyLink::findAll([
                'property_id' => $property->id,
                'value_id'    => $value->id,
                'object'      => Product::className(),
            ]);

            foreach ($links as $link) {
                echo 'DELETE size EURO link ' . $link->id . '(' . $value->value . ')' . PHP_EOL;
                $link->delete();
            }
            echo 'DELETE size EURO ' . $id . '(' . $value->value . ')' . PHP_EOL;
            $value->delete();
            unset($links);
            unset($value);
        }

        unset($property);
        unset($products);
        unset($valueReplace);


        $property = Property::findOne(['name' => 'Размер']);
        $products = Product::find()->all();


        $valueMen   = [];
        $valueWoMen = [];
        $valueUni   = [];
        foreach ($property->values as $value) {
            if ($value->property_id === $property->id) {
                $valueInt = (int)$value->value;
                if ($valueInt > 35 && $valueInt < 47) {
                    if ($valueInt < 38) {
                        $valueWoMen[] = $value;
                        $valueUni[]   = $value;
                    } elseif ($valueInt > 42) {
                        $valueMen[] = $value;
                        $valueUni[] = $value;
                    } else {
                        $valueWoMen[] = $value;
                        $valueMen[]   = $value;
                        $valueUni[]   = $value;
                    }
                }
            }
        }


        foreach ($products as $product) {
            $values = [];
            foreach ($product->values as $value) {
                if ($value->property_id === $property->id) {
                    $values[$value->id] = $value;
                }
            }

            $sizes = $valueUni;
            if ($product->sex === 1) {
                $sizes = $valueMen;
            } elseif ($product->sex === 2) {
                $sizes = $valueWoMen;
            }

            if (count($values)) {
                foreach ($sizes as $size) {
                    if (!isset($values[$size->id])) {
                        $valueLink = new PropertyLink();

                        $valueLink->object      = $product::className();
                        $valueLink->object_id   = $product->id;
                        $valueLink->property_id = $property->id;
                        $valueLink->value_id    = $size->id;

                        $valueLink->save();
                        unset($valueLink);
                        //var_dump($valueLink->getErrors());
                        echo 'Add size to ' . $product->id . '(' . $size->id . ')' . PHP_EOL;
                    }
                }
                unset($sizes);
                unset($values);
            }
        }


        $this->afterMigrate();
    }

    public function down()
    {

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
