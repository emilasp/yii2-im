<?php

use emilasp\im\common\models\OrderStatus;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160408_045939_AddOrderStatuses extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $data = [
            ['code' => OrderStatus::CODE_NEW, 'name' => 'Новый', 'events' => '{}', 'fields' => '{}', 'status' => 1],
            ['code' => OrderStatus::CODE_CONFIRMED, 'name' => 'Подтвержден', 'events' => '{}', 'fields' => '{}', 'status' => 1],
            ['code' => OrderStatus::CODE_PAYED, 'name' => 'Оплачен', 'events' => '{}', 'fields' => '{}', 'status' => 1],
            ['code' => OrderStatus::CODE_SENDED, 'name' => 'Отправлен', 'events' => '{}', 'fields' => '{}', 'status' => 1],
            ['code' => OrderStatus::CODE_COMPLETED, 'name' => 'Завершен', 'events' => '{}', 'fields' => '{}', 'status' => 1],
            ['code' => OrderStatus::CODE_CANCELED, 'name' => 'Отменен', 'events' => '{}', 'fields' => '{}', 'status' => 1],
        ];

        foreach ($data as $statusData) {
            $status = new OrderStatus($statusData);
            $status->save();
        }

        $this->afterMigrate();
    }

    public function down()
    {
        $this->afterMigrate();
    }


    /**
    * Initializes the migration.
    * This method will set [[db]] to be the 'db' application component, if it is null.
    */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
    * Устанавливаем дефолтные параметры для таблиц
    */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
    * Устанавливаем начальные параметры времени и памяти
    */
    private function beforeMigrate()
    {
        echo 'Start..'.PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time = microtime(true);
    }

    /**
    * Выводим параметры времени и памяти
    */
    private function afterMigrate()
    {
        echo 'End..'.PHP_EOL;
        echo 'Использовано памяти: '.FileHelper::formatSizeUnits((memory_get_usage()-$this->memory)).PHP_EOL;
        echo 'Время выполнения скрипта: '.(microtime(true) - $this->time).' сек.'.PHP_EOL;
    }
}
