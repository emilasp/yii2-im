<?php

use emilasp\im\common\models\Payment;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

class m160411_191945_AddPayments extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;


    public function up()
    {
        $dataParams = [
            [
                'code'             => Payment::CODE_CASHE,
                'name'             => 'Наличными курьеру',
                'text'             => 'Наличными курьеру',
                'extracharge_type' => 1,
                'order'            => 1,
                'status'           => 1,
            ],
            [
                'code'             => Payment::CODE_CARD_VISA,
                'name'             => 'Карта VISA',
                'text'             => 'Карта VISA',
                'extracharge_type' => 1,
                'order'            => 2,
                'status'           => 0,
            ],
            [
                'code'             => Payment::CODE_CARD_MASTERCARD,
                'name'             => 'Карта MASTERCARD',
                'text'             => 'Карта MASTERCARD',
                'extracharge_type' => 1,
                'order'            => 3,
                'status'           => 0,
            ],
            [
                'code'             => Payment::CODE_CARD_MAESTRO,
                'name'             => 'Карта MAESTRO',
                'text'             => 'Карта MAESTRO',
                'extracharge_type' => 1,
                'order'            => 4,
                'status'           => 0,
            ],
        ];

        foreach ($dataParams as $data) {
            $model = new Payment($data);
            if (!$model->save()) {
                var_dump($model->getErrors());
            }
        }

        $this->afterMigrate();
    }

    public function down()
    {

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
