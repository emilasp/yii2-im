<?php
namespace emilasp\im\common\models;

use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\json\models\DynamicModel;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "im_payment".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $text
 * @property string $icon
 * @property string $city
 * @property string $city_exclude
 * @property string $provider
 * @property string $field
 * @property integer $extracharge_type
 * @property integer $extracharge
 * @property integer $order
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Order[] $imOrders
 * @property Order[] $imOrders0
 * @property User $createdBy
 * @property User $updatedBy
 */
class Payment extends \emilasp\core\components\base\ActiveRecord
{
    const CODE_CASHE           = 'cache';
    const CODE_CARD_VISA       = 'visa';
    const CODE_CARD_MASTERCARD = 'mastercard';
    const CODE_CARD_MAESTRO    = 'maestro';
    const CODE_QIWI            = 'cache';


    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status'           => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            'variety_extracharge_type' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'extracharge_type',
                'group'     => 'extracharge_type',
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'city',
                'scheme'    => DynamicModel::SCHEME_CITY,
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'city_exclude',
                'scheme'    => DynamicModel::SCHEME_CITY,
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'provider',
                'scheme'    => DynamicModel::SCHEME_HANDLER,
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'field',
                'scheme'    => DynamicModel::SCHEME_FIELDS,
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'im_payment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'extracharge_type', 'status'], 'required'],
            [['text', 'city', 'city_exclude', 'provider', 'field'], 'string'],
            [['extracharge_type', 'extracharge', 'order', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['code'], 'string', 'max' => 50],
            [['name', 'icon'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('site', 'ID'),
            'code'             => Yii::t('site', 'Code'),
            'name'             => Yii::t('site', 'Name'),
            'text'             => Yii::t('site', 'Text'),
            'icon'             => Yii::t('site', 'Icon'),
            'city'             => Yii::t('site', 'Cities'),
            'city_exclude'     => Yii::t('site', 'Cities Exclude'),
            'provider'         => Yii::t('site', 'Provider'),
            'field'            => Yii::t('site', 'Fields'),
            'extracharge_type' => Yii::t('im', 'Extracharge Type'),
            'extracharge'      => Yii::t('im', 'Extracharge'),
            'order'            => Yii::t('im', 'Order'),
            'status'           => Yii::t('site', 'Status'),
            'created_at'       => Yii::t('site', 'Created At'),
            'updated_at'       => Yii::t('site', 'Updated At'),
            'created_by'       => Yii::t('site', 'Created By'),
            'updated_by'       => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImOrders()
    {
        return $this->hasMany(Order::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImOrders0()
    {
        return $this->hasMany(Order::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
