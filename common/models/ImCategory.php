<?php

namespace emilasp\im\common\models;

use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\taxonomy\models\Category;
use Yii;
use yii\helpers\ArrayHelper;
use emilasp\user\core\models\User;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use creocoder\nestedsets\NestedSetsBehavior;
use emilasp\core\components\base\ActiveRecord;
use emilasp\seo\common\behaviors\SeoModelBehavior;
use emilasp\variety\behaviors\VarietyModelBehavior;

/**
 * This is the model class for table "taxonomy_category".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property string $short_text
 * @property integer $image_id
 * @property integer $type
 * @property integer $status
 * @property integer $tree
 * @property integer $lft
 * @property integer $rgt
 * @property integer $depth
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class ImCategory extends Category
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'seo' => [
                'class' => SeoModelBehavior::className(),
                'route' => '/im/category/view',
            ],
        ]);
    }
}
