<?php
namespace emilasp\im\common\models;

use emilasp\files\behaviors\ImagesBehavior;
use emilasp\files\models\File;
use emilasp\taxonomy\models\Category;
use emilasp\taxonomy\models\Property;
use emilasp\taxonomy\models\PropertyLink;
use emilasp\taxonomy\models\PropertyValue;
use emilasp\user\core\models\User;
use emilasp\variety\models\Variety;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use emilasp\taxonomy\models\CategoryLink;
use emilasp\core\components\base\ActiveRecord;
use emilasp\seo\common\behaviors\SeoModelBehavior;
use emilasp\variety\behaviors\VarietyModelBehavior;
use emilasp\core\behaviors\relations\ManyManyAttributeModelBehavior;

/**
 * This is the model class for table "im_good".
 *
 * @property integer $id
 * @property string $seller_id
 * @property string $brand_id
 * @property string $brand_group_id
 * @property string $article
 * @property string $name
 * @property string $text
 * @property string $short_text
 * @property string $cost
 * @property string $cost_base
 * @property integer $quantity
 * @property integer $discount
 * @property integer $unit
 * @property integer $weight
 * @property string $country
 * @property integer $year
 * @property integer $sex
 * @property integer $on_request
 * @property integer $variation_id
 * @property integer $on_index
 * @property integer $type
 * @property integer $status
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Product extends ActiveRecord
{
    const TYPE_SIMPLE          = 1;
    const TYPE_VARIATE         = 2;
    const TYPE_VARIATE_PSEUDO  = 3;
    const TYPE_VARIATE_VIRTUAL = 4;
    const TYPE_VARIATE_SET     = 5;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'images'           => [
                'class' => ImagesBehavior::className(),
            ],
            'variety_type'     => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'product_type',
            ],
            'variety_sex'      => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'sex',
                'group'     => 'product_sex',
            ],
            'variety_status'   => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            'variety_on_index' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'on_index',
                'group'     => 'bool',
            ],
            [
                'class'           => SeoModelBehavior::className(),
                'attributeUnique' => 'article',
                'route'           => '/im/product/view',
            ],
            [
                'class'         => ManyManyAttributeModelBehavior::className(),
                'attribute'     => 'categoriesId',
                'linkedClass'   => CategoryLink::className(),
                'taxonomyClass' => ImCategory::className(),
                'relationName'  => 'categories',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'im_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['article', 'name', 'status', 'type'], 'required'],
            [['text', 'short_text'], 'string'],
            [['cost', 'cost_base'], 'number'],
            [
                [
                    'quantity',
                    'discount',
                    'weight',
                    'sex',
                    'unit',
                    'on_request',
                    'status',
                    'type',
                    'created_by',
                    'updated_by',
                    'brand_id',
                    'brand_group_id',
                    'seller_id',
                    'year',
                    'on_index',
                ],
                'integer',
            ],
            [['created_at', 'updated_at', 'categoriesId'], 'safe'],
            [['article', 'name'], 'string', 'max' => 255],
            ['country', 'string', 'max' => 50],
            [['article'], 'unique'],

            [['title', 'title_h', 'keywords', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => Yii::t('site', 'ID'),
            'seller_id'      => Yii::t('im', 'Seller'),
            'brand_id'       => Yii::t('im', 'Brand'),
            'brand_group_id' => Yii::t('im', 'Model'),
            'article'        => Yii::t('im', 'Article'),
            'name'           => Yii::t('site', 'Name'),
            'text'           => Yii::t('site', 'Text'),
            'short_text'     => Yii::t('site', 'Short Text'),
            'cost'           => Yii::t('im', 'Cost'),
            'cost_base'      => Yii::t('im', 'Cost Base'),
            'quantity'       => Yii::t('im', 'Quantity'),
            'discount'       => Yii::t('im', 'Discount'),
            'unit'           => Yii::t('im', 'Unit'),
            'weight'         => Yii::t('im', 'Weight'),
            'country'        => Yii::t('site', 'Country'),
            'year'           => Yii::t('im', 'Year'),
            'sex'            => Yii::t('im', 'Sex'),
            'on_request'     => Yii::t('im', 'On Request'),
            'variation_id'   => Yii::t('im', 'Property pseudo variation'),
            'type'           => Yii::t('site', 'Type'),
            'status'         => Yii::t('site', 'Status'),
            'on_index'       => Yii::t('im', 'On index'),
            'created_at'     => Yii::t('site', 'Created At'),
            'updated_at'     => Yii::t('site', 'Updated At'),
            'created_by'     => Yii::t('site', 'Created By'),
            'updated_by'     => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeller()
    {
        return $this->hasOne(Seller::className(), ['id' => 'seller_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return $this|static
     */
    public function getProductHasCategory()
    {
        return $this->hasMany(CategoryLink::className(), ['object_id' => 'id'])
                    ->where(['object' => self::className()]);
    }

    /**
     * @return $this
     */
    public function getCategories()
    {
        return $this->hasMany(ImCategory::className(), ['id' => 'taxonomy_id'])->via('productHasCategory');
    }

    /**
     * @return $this|static
     */
    public function getProductHasProperties()
    {
        return $this->hasMany(PropertyLink::className(), ['object_id' => 'id'])
                    ->where(['object' => self::className()]);
    }

    /**
     * @return $this
     */
    public function getProperties()
    {
        return $this->hasMany(Property::className(), ['id' => 'property_id'])
                    ->via('productHasProperties');
    }

    /**
     * @return $this
     */
    public function getValues()
    {
        return $this->hasMany(PropertyValue::className(), ['id' => 'value_id'])
                    ->via('productHasProperties');
    }

    /** @TODO - пока сделал вариации в свойствах Property. Иначе, если ка кв этом методе, вариация товара (псевдо) будет ограничена всего одной вариацией.
     * @return $this
     */
    public function getVariations()
    {
        if ($this->type === Variety::getValue('product_type_variate_pseudo')) {
            return $this->hasMany(Property::className(), ['id' => 'variation_id']);
        } elseif ($this->type === Variety::getValue('product_type_variation')) {
            return $this->hasMany(Product::className(), ['parent_id' => 'id']);
        }
        return null;
    }

    /** Получаем наборы
     * @return $this
     */
    public function getSets()
    {
        if ($this->type === Variety::getValue('product_type_variation_set')) {
            return $this->hasMany(Product::className(), ['parent_id' => 'id']);
        }
        return null;
    }

    /**
     * @return $this|static
     */
    public function getImages()
    {
        return $this->hasMany(File::className(), ['object_id' => 'id'])
                    ->where(['object' => self::className(), 'type' => File::TYPE_FILE_IMAGE]);
    }


    /** формируем код товара
     * @return string
     */
    public function getCode()
    {
        return sprintf("%03d", $this->brand_id) . '-' . sprintf("%010d", $this->id);
    }

    /** Получаем значения свойств для вариация товара(сгурппировано по property id)
     * @return array
     */
    public function getVariationValues()
    {
        $values = [];
        foreach ($this->properties as $property) {
            if ($property->is_variation) {
                foreach ($this->values as $value) {
                    if ($property->id === $value->property_id) {
                        $values[$property->id][] = $value;
                    }
                }
            }
        }
        return $values;
    }

    /**
     * @param bool $base
     *
     * @return integer
     */
    public function getPrice($base = false) : int
    {
        if ($base && $this->cost_base <= 0) {
            return 0;
        }

        if ($base) {
            return $this->cost_base;
        }

        if (!$this->cost && $this->discount) {
            return $this->cost_base - $this->cost_base / 100 * $this->discount;
        }

        return $this->cost;
    }


    /** Is discount
     * @return bool
     */
    public function isDiscount()
    {
        return ($this->discount || $this->getPrice(true) !== $this->getPrice());
    }

    /** Получаем скидку в процентах
     * @return float|int
     */
    public function getDiscountPercent()
    {
        if (!$this->isDiscount()) {
            return 0;
        }
        if ($this->discount) {
            return $this->discount;
        }

        return 100 - floor($this->cost / $this->cost_base * 100);
    }

}
