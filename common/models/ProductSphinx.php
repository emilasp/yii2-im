<?php

namespace emilasp\im\common\models;

use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\sphinx\Query;

/**
 * This is the model class for index "idx_product".
 *
 * @property integer $id
 * @property string $article
 * @property string $name
 * @property string $text
 * @property string $short_text
 * @property integer $parent_id
 * @property integer $brand_id
 * @property double $cost
 * @property integer $year
 * @property integer $sex
 * @property integer $status
 * @property string $created_at
 * @property array $property_value
 * @property array $category
 */
class ProductSphinx extends \yii\sphinx\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function indexName()
    {
        return 'idx_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'unique'],
            [['id', 'parent_id', 'brand_id', 'year', 'sex', 'status'], 'integer'],
            [['article', 'name', 'text', 'short_text'], 'string'],
            [['cost'], 'number'],
            [['created_at', 'property_value', 'category'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'             => 'ID',
            'article'        => 'Article',
            'name'           => 'Name',
            'text'           => 'Text',
            'short_text'     => 'Short Text',
            'parent_id'      => 'Parent ID',
            'brand_id'       => 'Brand ID',
            'cost'           => 'Cost',
            'year'           => 'Year',
            'sex'            => 'Sex',
            'status'         => 'Status',
            'created_at'     => 'Created At',
            'property_value' => 'Property Value',
            'category'       => 'Category',
        ];
    }

    public static function getProductQueryByIds($sphinxIds, $conditions = [])
    {

        $conditions = ArrayHelper::merge(['im_product.status' => Product::STATUS_ENABLED], $conditions);

        $query = Product::find()
                        ->with(['categories', 'images', 'brand'])
                        ->where($conditions);

        if ($sphinxIds) {
            $query->andWhere(['im_product.id' => $sphinxIds]);
            $query->orderBy(new Expression('idx(array[' . implode(',', $sphinxIds) . '],im_product.id )'));
        } else {
            $query->andWhere(['im_product.id' => 0]);
        }
        return $query;
    }


    /** Получаем похожие товары
     *
     * @param Product $product
     *
     * @return array
     */
    public static function getSphinxSimilarProductsIds(Product $product)
    {
        $sex          = $product->sex;
        $brandId      = $product->brand_id;
        $categoryIds  = implode('|', array_keys(ArrayHelper::map($product->categories, 'id', 'id')));
        $propValueIds = implode('|', array_keys(ArrayHelper::map($product->values, 'id', 'id')));

        $sql = <<<SQL
        SELECT *, weight() as w FROM idx_product 
        where match('@gender {$sex} @brand {$brandId} @categories {$categoryIds} @properties {$propValueIds}') AND id<>{$product->id}
        ORDER BY w DESC
        LIMIT 10
        option field_weights=(sex=15,brand=5,category=15,properties=5)
SQL;
        return Yii::$app->sphinx->createCommand($sql)->queryColumn();
    }

    /** Получаем ID продуктов по поисковой фразе
     *
     * @param $search
     *
     * @return array
     */
    public static function getSphinxSearchProductsIds($search)
    {
        $query = (new Query)->from('idx_product')->limit(20)
                            ->andWhere(['status' => 1])
                            ->match(Yii::$app->sphinx->escapeMatchValue($search));

        return $query->column();
    }


    /** Получаем общий фасет для категорий
     *
     * @param array $filters
     *
     * @return array
     */
    public static function getSphinxCategoryAllFacet($filters = [])
    {
        $query = (new Query)->from('idx_product')->facets([
            'category' => ['limit' => 10000],
        ]);

        $query->andWhere(ArrayHelper::merge(['status' => 1], $filters));

        $facets = $query->search();

        $result = [];
        if (isset($facets['facets']['category'])) {
            foreach ($facets['facets']['category'] as $facet) {
                $result[$facet['category']] = $facet;
            }
        }

        return $result;
    }


    /** Выбраем id продуктов по категории и свойствам
     *
     * @param array $brandIds
     * @param array $categoryIds
     * @param array $propertyValueIds
     * @param null $sort
     *
     * @return null|static
     */
    public static function getSphinxProductQuery(
        $search,
        array $cost,
        array $brandIds,
        array $brandGroupIds,
        array $categoryIds,
        array $propertyValueIds,
        $sort = null)
    {
        $query = self::find()->select('id')->limit(10000);

        $query = self::setConditions($query, $search, $cost, $brandIds, $brandGroupIds, $categoryIds,
            $propertyValueIds);

        if (trim($sort)) {
            $query->addOrderBy($sort);
        } else {
            $query->addOrderBy('cost ASC');
        }

        return $query;
    }


    /** Выбраем максимальную и минимальную стоимость продуктов
     *
     * @param array $categoryIds
     * @param array $propertyValueIds
     *
     * @return null|static
     */
    public static function getSphinxPriceRange(
        $search,
        $cost,
        array $brandIds,
        array $brandGroupIds,
        array $categoryIds,
        array $propertyValueIds)
    {
        $query = (new Query)->select('max(cost) as max_cost, min(cost) as min_cost')
                            ->from('idx_product')->limit(1);

        $query      = self::setConditions($query, $search, $brandIds, $brandGroupIds, $cost, $categoryIds,
            $propertyValueIds);
        $priceRange = $query->one();
        return [(int)$priceRange['min_cost'], (int)$priceRange['max_cost']];
    }

    /**
     * @param $search
     * @param $brandIds
     * @param $cost
     * @param array $categoryIds
     * @param array $propertyValueIds
     *
     * @return array
     */
    public static function getSphinxFacets(
        $search,
        $cost,
        array $brandIds,
        array $brandGroupIds,
        array $categoryIds,
        array $propertyValueIds,
        $conditions = [])
    {

        $query = self::find()->from('idx_product')->facets([
            'brand_id'       => ['limit' => 100000],
            'brand_group_id' => ['limit' => 100000],
            'property'       => ['limit' => 100000],
            'property_value' => ['limit' => 100000],
        ]);

        $query = self::setConditions($query, $search, $cost, $brandIds, $brandGroupIds, $categoryIds,
            $propertyValueIds);

        $query->andWhere(ArrayHelper::merge(['status' => 1], $conditions));
        $propertyValues = $query->search();

        if (!count($propertyValues['facets'])) {
            return [];
        }
        return $propertyValues;
    }

    /** Устаналиваем условия выборки
     *
     * @param Query $query
     * @param string $search
     * @param array $brandIds
     * @param array $categoryIds
     * @param array $propertyValueIds
     *
     * @return bool
     */
    private static function setConditions(
        Query $query,
        $search,
        $cost,
        array $brandIds,
        array $brandGroupIds,
        array $categoryIds,
        array $propertyValueIds)
    {
        if (!empty($search[0])) {
            $query->match(Yii::$app->sphinx->escapeMatchValue($search[0]));
        }

        if ($brandIds) {
            $query->andWhere(['brand_id' => $brandIds]);
        }

        if ($brandGroupIds) {
            $query->andWhere(['brand_group_id' => $brandGroupIds]);
        }

        if ($cost && $cost[0] && strpos($cost[0], ',')) {
            $range = explode(',', $cost[0]);
            $query->andWhere('cost >= ' . $range[0]);
            $query->andWhere('cost <= ' . $range[1]);
        }

        if ($categoryIds) {
            $query->andWhere(['category' => $categoryIds]);
        }

        //$propertyValueIds = array_diff($propertyValueIds, [null]);

        if ($propertyValueIds) {
            foreach ($propertyValueIds as $propValues) {
                $query->andWhere(['property_value' => $propValues]);
                /*foreach ($propValues as $valueId) {
                    $query->andWhere(['property_value' => $valueId]);
                }*/
            }
        }
        return $query->andWhere('cost > 0');
    }
}
