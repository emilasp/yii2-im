<?php
namespace emilasp\im\common\models;

use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\json\models\DynamicModel;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "im_order_status".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $events
 * @property string $fields
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Order[] $orders
 */
class OrderStatus extends \emilasp\core\components\base\ActiveRecord
{
    const CODE_NEW       = 'new';
    const CODE_CONFIRMED = 'confirmed';
    const CODE_PAYED     = 'payed';
    const CODE_SENDED    = 'sended';
    const CODE_COMPLETED = 'completed';
    const CODE_CANCELED  = 'canceled';

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'events',
                'scheme'    => DynamicModel::SCHEME_HANDLER,
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'fields',
                'scheme'    => DynamicModel::SCHEME_FIELDS,
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'im_order_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'status'], 'required'],
            [['events', 'fields'], 'string'],
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['code'], 'string', 'max' => 50],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'code'       => Yii::t('site', 'Code'),
            'name'       => Yii::t('site', 'Name'),
            'events'     => Yii::t('site', 'Events'),
            'fields'     => Yii::t('site', 'Fields'),
            'status'     => Yii::t('site', 'Status'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['status_id' => 'id']);
    }
}
