<?php

namespace emilasp\im\common\models;

use emilasp\core\behaviors\data\HistoryBehavior;
use emilasp\core\validators\PhoneValidator;
use emilasp\geoapp\models\GeoAddrobj;
use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\json\models\DynamicModel;
use emilasp\user\core\events\UserEvent;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "im_client".
 *
 * @property integer $id
 * @property string $phone
 * @property integer $manager_id
 * @property string $comment
 * @property string $name
 * @property string $lastname
 * @property string $surname
 * @property string $address
 * @property integer $city_id
 * @property integer $status
 * @property string $history
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $user
 * @property User $manager
 * @property User $createdBy
 * @property User $updatedBy
 */
class Client extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'im_client';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'client_status',
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'address',
                'scheme'    => DynamicModel::SCHEME_ADDRESS,
            ],
            [
                'class' => HistoryBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user) || Yii::$app->user->isGuest) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['manager_id', 'status', 'city_id', 'created_by', 'updated_by'], 'integer'],
            [['phone', 'name', 'status'], 'required'],
            [['address', 'history', 'comment'], 'string'],

            [['phone'], PhoneValidator::className()],

            [['created_at', 'updated_at'], 'safe'],
            [['name', 'lastname', 'surname'], 'string', 'max' => 255],

            [
                ['id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['id' => 'id'],
            ],
            [
                ['manager_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['manager_id' => 'id'],
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'manager_id' => Yii::t('site', 'Manager ID'),
            'comment'    => Yii::t('site', 'Manager comment'),
            'name'       => Yii::t('site', 'User Name'),
            'lastname'   => Yii::t('site', 'Lastname'),
            'surname'    => Yii::t('site', 'Surname'),
            'address'    => Yii::t('site', 'Address'),
            'city_id'    => Yii::t('site', 'City ID'),
            'phone'      => Yii::t('site', 'Phone'),
            'status'     => Yii::t('site', 'Status'),
            'history'    => Yii::t('site', 'History'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'created_by' => Yii::t('site', 'Created By'),
            'updated_by' => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(User::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegionModel()
    {
        return $this->hasOne(GeoAddrobj::className(), ['aoid' => 'region']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }


    /**
     * @param Order $order
     *
     * @return Client|null|static
     */
    public static function getClient(Order $order)
    {
        $client = self::findOne(['phone' => $order->phone]);

        if (!$client) {
            $user           = new User();
            $user->username = $order->phone;
            $user->phone    = $order->phone;
            $user->email    = $order->email;
            $user->city_id  = $order->city_id;
            $user->role     = 'client';
            $user->auth_key = $user->generateAuthKey();
            
            $password = Yii::$app->security->generateRandomString(8);
            $user->password = $user->generatePassword($password);
            $user->status   = User::STATUS_ACTIVE;
                        
            if ($user->save()) {
                $client           = new Client();
                $client->id       = $user->id;
                $client->name     = $order->client_name;
                $client->lastname = $order->client_lastname;
                $client->address  = $order->address;
                $client->phone    = $order->phone;
                $client->city_id  = $order->city_id;
                $client->status   = 1;
                $client->save();

                Yii::$app->getModule('user')->trigger(UserEvent::EVENT_NEW_USER, new UserEvent([
                    'user' => $user,
                    'params' => ['password' => $password],
                ]));
            }
        }
        return $client;
    }


    /** Получаем ФИО 
     * @return string
     */
    public function getFio()
    {
        return $this->lastname . ' ' . $this->name . ' ' . $this->surname;
    }
}
