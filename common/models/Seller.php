<?php

namespace emilasp\im\common\models;

use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * Модель Поставщики
 * This is the model class for table "im_seller".
 *
 * @property integer $id
 * @property string $name
 * @property string $text
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Seller extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'im_seller';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status'], 'required'],
            [['text'], 'string'],
            [['status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'name'       => Yii::t('site', 'Name'),
            'text'       => Yii::t('site', 'Text'),
            'status'     => Yii::t('site', 'Status'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'created_by' => Yii::t('site', 'Created By'),
            'updated_by' => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
