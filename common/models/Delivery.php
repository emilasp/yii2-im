<?php

namespace emilasp\im\common\models;

use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\json\models\DynamicModel;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "im_delivery".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $text
 * @property string $text_free
 * @property string $icon
 * @property string $city
 * @property string $city_exclude
 * @property string $provider
 * @property string $payment
 * @property string $field
 * @property boolean $show_on_product_page
 * @property integer $order
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property User $createdBy
 * @property User $updatedBy
 */
class Delivery extends \emilasp\core\components\base\ActiveRecord
{
    const CODE_COURIER = 'courier';
    const CODE_POSTAL  = 'postal';
    const CODE_EMS     = 'ems';


    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'city',
                'scheme'    => DynamicModel::SCHEME_CITY,
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'city_exclude',
                'scheme'    => DynamicModel::SCHEME_CITY,
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'payment',
                'scheme'    => [
                    'payment_id' => ['label' => 'Payment', 'rules' => [['integer']]],
                ],
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'provider',
                'scheme'    => DynamicModel::SCHEME_HANDLER,
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'field',
                'scheme'    => DynamicModel::SCHEME_FIELDS,
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'im_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'status'], 'required'],
            [['text', 'city', 'city_exclude', 'provider', 'payment', 'field'], 'string'],
            [['show_on_product_page'], 'boolean'],
            [['order', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['code'], 'string', 'max' => 50],
            [['name', 'text_free', 'icon'], 'string', 'max' => 255],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'                   => Yii::t('site', 'ID'),
            'code'                 => Yii::t('site', 'Code'),
            'name'                 => Yii::t('site', 'Name'),
            'text'                 => Yii::t('site', 'Text'),
            'text_free'            => Yii::t('site', 'Text Free'),
            'icon'                 => Yii::t('site', 'Icon'),
            'city'                 => Yii::t('site', 'Cities'),
            'city_exclude'         => Yii::t('site', 'Cities Exclude'),
            'provider'             => Yii::t('site', 'Provider'),
            'payment'              => Yii::t('im', 'Payments'),
            'field'                => Yii::t('site', 'Fields'),
            'show_on_product_page' => Yii::t('im', 'Show On Product Page'),
            'order'                => Yii::t('im', 'Order'),
            'status'               => Yii::t('site', 'Status'),
            'created_at'           => Yii::t('site', 'Created At'),
            'updated_at'           => Yii::t('site', 'Updated At'),
            'created_by'           => Yii::t('site', 'Created By'),
            'updated_by'           => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
