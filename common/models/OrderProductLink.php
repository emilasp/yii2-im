<?php
namespace emilasp\im\common\models;

use emilasp\taxonomy\models\PropertyValue;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "im_order_product".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $product_id
 * @property string $variations
 * @property integer $quantity
 * @property string $cost
 * @property string $created_at
 * @property string $updated_at
 *
 * @property Order $order
 * @property Product $product
 */
class OrderProductLink extends \emilasp\core\components\base\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'im_order_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id'], 'required'],
            [['order_id', 'product_id', 'quantity'], 'integer'],
            [['cost'], 'number'],
            [['created_at', 'updated_at', 'variations'], 'safe'],
            [
                ['order_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Order::className(),
                'targetAttribute' => ['order_id' => 'id'],
            ],
            [
                ['product_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Product::className(),
                'targetAttribute' => ['product_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'order_id'   => Yii::t('im', 'Order'),
            'product_id' => Yii::t('im', 'Product'),
            'variations' => Yii::t('im', 'Variations'),
            'quantity'   => Yii::t('im', 'Quantity'),
            'cost'       => Yii::t('im', 'Cost'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }


    /** Получаем значения для вариаций
     * @return array|static[]
     */
    public function getVariation()
    {
        $variations = [];
        if ($this->variations) {
            $variations = json_decode($this->variations);

            if (count($variations)) {

                $variation = [];
                foreach ($variations as $propVariations) {
                    foreach ($propVariations as $var) {
                        $variation[] = $var;
                    }
                }
                if ($variation) {
                    $variations = PropertyValue::findAll($variation);
                }
            }
        }
        return $variations;
    }
}
