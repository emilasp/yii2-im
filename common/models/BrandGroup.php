<?php

namespace emilasp\im\common\models;

use emilasp\files\behaviors\FileSingleBehavior;
use emilasp\files\behaviors\ImagesBehavior;
use emilasp\files\models\File;
use emilasp\seo\common\behaviors\SeoModelBehavior;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "im_brand_group".
 *
 * @property integer $id
 * @property integer $brand_id
 * @property string $name
 * @property string $text
 * @property string $short_text
 * @property integer $image_id
 * @property integer $order
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property File $image
 * @property User $createdBy
 * @property User $updatedBy
 */
class BrandGroup extends \emilasp\core\components\base\ActiveRecord
{
    public $uploadBanner;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'status',
                'group'     => 'status',
            ],
            [
                'class' => FileSingleBehavior::className(),
            ],
            'images'         => [
                'class' => ImagesBehavior::className(),
            ],

            'seo' => [
                'class' => SeoModelBehavior::className(),
                'route' => '/im/brand-group/view',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user)) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'im_brand_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status', 'brand_id'], 'required'],
            [['text', 'short_text'], 'string'],
            [['image_id', 'brand_id', 'order', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['uploadBanner'], 'safe'],
            [
                ['image_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => File::className(),
                'targetAttribute' => ['image_id' => 'id'],
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],

            [['title', 'title_h', 'keywords', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'         => Yii::t('site', 'ID'),
            'brand_id'   => Yii::t('im', 'Brand'),
            'name'       => Yii::t('site', 'Name'),
            'text'       => Yii::t('site', 'Text'),
            'short_text' => Yii::t('site', 'Short Text'),
            'image_id'   => Yii::t('site', 'Image ID'),
            'order'      => Yii::t('site', 'Order'),
            'status'     => Yii::t('site', 'Status'),
            'created_at' => Yii::t('site', 'Created At'),
            'updated_at' => Yii::t('site', 'Updated At'),
            'created_by' => Yii::t('site', 'Created By'),
            'updated_by' => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return $this|static
     */
    public function getImages()
    {
        return $this->hasMany(File::className(), ['object_id' => 'id'])
                    ->where(['object' => self::className(), 'attribute' => 'banners', 'type' => File::TYPE_FILE_IMAGE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
