<?php

namespace emilasp\im\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\im\common\models\Payment;

/**
 * PaymentSearch represents the model behind the search form about `emilasp\im\common\models\Payment`.
 */
class PaymentSearch extends Payment
{
    public $search = true;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'extracharge_type', 'extracharge', 'order', 'status', 'created_by', 'updated_by'], 'integer'],
            [['code', 'name', 'text', 'icon', 'city', 'city_exclude', 'provider', 'field', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Payment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'extracharge_type' => $this->extracharge_type,
            'extracharge' => $this->extracharge,
            'order' => $this->order,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'code', $this->code])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'text', $this->text])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'city_exclude', $this->city_exclude])
            ->andFilterWhere(['like', 'provider', $this->provider])
            ->andFilterWhere(['like', 'field', $this->field]);

        return $dataProvider;
    }
}
