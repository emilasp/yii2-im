<?php

namespace emilasp\im\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\im\common\models\Order;

/**
 * OrderSearch represents the model behind the search form about `emilasp\im\common\models\Order`.
 */
class OrderSearch extends Order
{
    public $search = true;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'type',
                    'user_id',
                    'city_id',
                    'delivery_id',
                    'payment_id',
                    'status_id',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [
                [
                    'client_name',
                    'client_lastname',
                    'email',
                    'address',
                    'phone',
                    'delivery_at',
                    'delivery_data',
                    'payment_data',
                    'comment_client',
                    'comment_manager',
                    'history',
                    'created_at',
                    'updated_at',
                ],
                'safe',
            ],
            [['sum'], 'number'],
            [['paid'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'          => $this->id,
            'type'        => $this->type,
            'user_id'     => $this->user_id,
            'city_id'     => $this->city_id,
            'delivery_at' => $this->delivery_at,
            'delivery_id' => $this->delivery_id,
            'payment_id'  => $this->payment_id,
            'sum'         => $this->sum,
            'paid'        => $this->paid,
            'status_id'   => $this->status_id,
            'created_at'  => $this->created_at,
            'updated_at'  => $this->updated_at,
            'created_by'  => $this->created_by,
            'updated_by'  => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'address', $this->address])
              ->andFilterWhere(['like', 'phone', $this->phone])
              ->andFilterWhere(['like', 'email', $this->email])
              ->andFilterWhere(['like', 'client_name', $this->client_name])
              ->andFilterWhere(['like', 'client_lastname', $this->client_lastname])
              ->andFilterWhere(['like', 'delivery_data', $this->delivery_data])
              ->andFilterWhere(['like', 'payment_data', $this->payment_data])
              ->andFilterWhere(['like', 'comment_client', $this->comment_client])
              ->andFilterWhere(['like', 'comment_manager', $this->comment_manager])
              ->andFilterWhere(['like', 'history', $this->history]);

        return $dataProvider;
    }
}
