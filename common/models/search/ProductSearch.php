<?php
namespace emilasp\im\common\models\search;

use emilasp\im\common\models\Product;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * GoodSearch represents the model behind the search form about `emilasp\im\common\models\search\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'seller_id',
                    'brand_id',
                    'brand_group_id',
                    'quantity',
                    'discount',
                    'unit',
                    'weight',
                    'sex',
                    'year',
                    'on_request',
                    'type',
                    'on_index',
                    'status',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [
                [
                    'article',
                    'name',
                    'text',
                    'country',
                    'short_text',
                    'created_at',
                    'updated_at',
                ],
                'safe',
            ],
            [['cost', 'cost_base'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();
        $query->with(['images', 'createdBy']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort'  => ['defaultOrder' => ['id' => SORT_DESC]],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id'             => $this->id,
            'seller_id'      => $this->seller_id,
            'brand_id'       => $this->brand_id,
            'brand_group_id' => $this->brand_group_id,
            'cost'           => $this->cost,
            'cost_base'      => $this->cost_base,
            'quantity'       => $this->quantity,
            'discount'       => $this->discount,
            'unit'           => $this->unit,
            'weight'         => $this->weight,
            'sex'            => $this->sex,
            'on_request'     => $this->on_request,
            'type'           => $this->type,
            'status'         => $this->status,
            'on_index'       => $this->on_index,
            'year'           => $this->year,
            'created_at'     => $this->created_at,
            'updated_at'     => $this->updated_at,
            'created_by'     => $this->created_by,
            'updated_by'     => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'article', $this->article])
              ->andFilterWhere(['like', 'name', $this->name])
              ->andFilterWhere(['like', 'text', $this->text])
              ->andFilterWhere(['like', 'country', $this->country])
              ->andFilterWhere(['like', 'short_text', $this->short_text]);

        return $dataProvider;
    }
}
