<?php
namespace emilasp\im\common\models\forms;

use emilasp\core\validators\PhoneValidator;
use emilasp\im\common\models\Client;
use emilasp\im\common\models\Order;
use emilasp\im\common\models\OrderProductLink;
use emilasp\im\frontend\components\CartComponent;
use emilasp\im\frontend\events\ImEvent;
use yii;
use yii\helpers\ArrayHelper;

/**
 * Class OrderForm
 * @package emilasp\im\common\models\forms
 */
class OrderForm extends Order
{
    /** @var  Client */
    public $client;

    public $client_surname;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'type',
                    'city_id',
                    //'user_id',
                    'delivery_id',
                    'payment_id',
                    'phone',
                    //'email',
                    'sum',
                    'status_id',
                    'client_name',
                    //'client_lastname',
                ],
                'required',
            ],
            [
                [
                    'type',
                    'user_id',
                    'city_id',
                    'delivery_id',
                    'payment_id',
                    'status_id',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [
                [
                    'client_name',
                    'client_lastname',
                    'client_surname',
                    'address',
                    'delivery_data',
                    'payment_data',
                    'comment_client',
                    'comment_manager',
                    'history',
                ],
                'string',
            ],
            [['delivery_at', 'created_at', 'updated_at'], 'safe'],
            [['email'], 'email'],
            [['sum'], 'number'],
            [['paid'], 'boolean'],
            ['type', 'default', 'value' => self::TYPE_ONLINE],
            ['status_id', 'default', 'value' => self::STATUS_NEW],

            [['phone', 'phone_other'], PhoneValidator::className()],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('site', 'ID'),
            'type'            => Yii::t('site', 'Type'),
            'user_id'         => Yii::t('site', 'User ID'),
            'city_id'         => Yii::t('site', 'City ID'),
            'client_name'     => Yii::t('site', 'User Name'),
            'client_lastname' => Yii::t('site', 'Lastname'),
            'client_surname'  => Yii::t('site', 'Surname'),
            'address'         => Yii::t('site', 'Address'),
            'phone'           => Yii::t('site', 'Phone'),
            'email'           => Yii::t('site', 'Email'),
            'delivery_at'     => Yii::t('im', 'Delivery At'),
            'delivery_id'     => Yii::t('im', 'Delivery'),
            'delivery_data'   => Yii::t('im', 'Delivery Data'),
            'payment_id'      => Yii::t('im', 'Payment'),
            'payment_data'    => Yii::t('im', 'Payment Data'),
            'sum'             => Yii::t('im', 'Sum'),
            'comment_client'  => Yii::t('im', 'Comment Client'),
            'comment_manager' => Yii::t('im', 'Comment Manager'),
            'paid'            => Yii::t('im', 'Paid'),
            'status_id'       => Yii::t('site', 'Status'),
            'history'         => Yii::t('site', 'History'),
            'created_at'      => Yii::t('site', 'Created At'),
            'updated_at'      => Yii::t('site', 'Updated At'),
            'created_by'      => Yii::t('site', 'Created By'),
            'updated_by'      => Yii::t('site', 'Updated By'),
        ];
    }


    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (count($this->addresses)) {
            $this->city_id = $this->addresses[0]->city;
        }
        return parent::beforeValidate();
    }


    /**
     * @return bool
     */
    public function afterValidate()
    {
        $this->client  = Client::getClient($this);
        $this->user_id = $this->client->id;
        parent::afterValidate();
    }

    /**
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->setProducts();

            Yii::$app->getModule('im')->trigger(ImEvent::EVENT_NEW_ORDER, new ImEvent([
                'params' => [
                    'user'  => $this->user,
                    'order' => $this,
                ],
            ]));
        }
        parent::afterSave($insert, $changedAttributes);
    }

    private function setProducts()
    {
        $cart = new CartComponent();

        $data     = $cart->data['items'];
        $products = $cart->items;

        foreach ($data as $id => $hashs) {
            foreach ($hashs as $hash => $item) {
                $itemCount = $item['count'];

                if ($itemCount) {
                    $product = $products[$id];

                    $variations = $item['variations'];

                    $link             = new OrderProductLink();
                    $link->order_id   = $this->id;
                    $link->product_id = $id;
                    $link->quantity   = (integer)$itemCount;
                    $link->cost       = $itemCount * $product->getPrice();
                    $link->variations = json_encode($variations);
                    $link->save();
                }
            }
        }
        $cart->dropCart();
    }

    /**
     * Устанавливаем дефолтные и косвенные атрибуты заказа
     */
    public function setData()
    {
        $this->status_id = self::STATUS_NEW;
        $this->type      = self::TYPE_ONLINE;

        $cart      = new CartComponent();
        $this->sum = $cart->data['summ'];
    }
}
