<?php
namespace emilasp\im\common\models;

use emilasp\core\behaviors\data\HistoryBehavior;
use emilasp\core\validators\PhoneValidator;
use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\json\models\DynamicModel;
use emilasp\user\core\models\User;
use emilasp\variety\behaviors\VarietyModelBehavior;
use yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "im_order".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $user_id
 * @property integer $city_id
 * @property string $client_name
 * @property string $client_lastname
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property string $phone_other
 * @property string $delivery_at
 * @property integer $delivery_id
 * @property string $delivery_data
 * @property integer $payment_id
 * @property string $payment_data
 * @property string $sum
 * @property string $comment_client
 * @property string $comment_manager
 * @property boolean $paid
 * @property integer $status_id
 * @property string $history
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property OrderStatus $status
 * @property Payment $payment
 * @property Delivery $delivery
 * @property User $user
 * @property User $createdBy
 * @property User $updatedBy
 * @property OrderProductLink[] $orderProducts
 */
class Order extends \emilasp\core\components\base\ActiveRecord
{
    const TYPE_CART    = 1;
    const TYPE_ONLINE  = 2;
    const TYPE_OFFLINE = 3;

    const STATUS_NEW       = 1;
    const STATUS_CONFIRMED = 2;
    const STATUS_PAYED     = 3;
    const STATUS_SENDED    = 4;
    const STATUS_COMPLETED = 5;
    const STATUS_CANCELED  = 6;

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'variety_status' => [
                'class'     => VarietyModelBehavior::className(),
                'attribute' => 'type',
                'group'     => 'order_type',
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'address',
                'scheme'    => DynamicModel::SCHEME_ADDRESS,
            ],
            /*[
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'delivery_data',
            ],
            [
                'class'     => JsonFieldBehavior::className(),
                'attribute' => 'payment_data',
            ],*/
            [
                'class' => HistoryBehavior::className(),
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => date('Y-m-d H:i:s'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    if (!isset(Yii::$app->user) || Yii::$app->user->isGuest) {
                        return 1;
                    }
                    return Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'im_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'city_id', 'user_id', 'phone', 'delivery_id', 'payment_id', 'sum', 'status_id'], 'required'],
            [
                [
                    'type',
                    'user_id',
                    'city_id',
                    'delivery_id',
                    'payment_id',
                    'status_id',
                    'created_by',
                    'updated_by',
                ],
                'integer',
            ],
            [
                [
                    'client_name',
                    'client_lastname',
                    'address',
                    'delivery_data',
                    'payment_data',
                    'comment_client',
                    'comment_manager',
                    'history',
                ],
                'string',
            ],
            [['delivery_at', 'created_at', 'updated_at'], 'safe'],
            [['email'], 'email'],
            [['sum'], 'number'],
            [['paid'], 'boolean'],
            [['phone','phone_other'], PhoneValidator::className()],


            [
                ['status_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => OrderStatus::className(),
                'targetAttribute' => ['status_id' => 'id'],
            ],
            [
                ['payment_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Payment::className(),
                'targetAttribute' => ['payment_id' => 'id'],
            ],
            [
                ['delivery_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Delivery::className(),
                'targetAttribute' => ['delivery_id' => 'id'],
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['user_id' => 'id'],
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id'],
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'              => Yii::t('site', 'ID'),
            'type'            => Yii::t('site', 'Type'),
            'user_id'         => Yii::t('site', 'User ID'),
            'city_id'         => Yii::t('site', 'City ID'),
            'client_name'     => Yii::t('site', 'User Name'),
            'client_lastname' => Yii::t('site', 'Lastname'),
            'address'         => Yii::t('site', 'Address'),
            'phone'           => Yii::t('site', 'Phone'),
            'email'           => Yii::t('site', 'Email'),
            'delivery_at'     => Yii::t('im', 'Delivery At'),
            'delivery_id'     => Yii::t('im', 'Delivery'),
            'delivery_data'   => Yii::t('im', 'Delivery Data'),
            'payment_id'      => Yii::t('im', 'Payment'),
            'payment_data'    => Yii::t('im', 'Payment Data'),
            'sum'             => Yii::t('im', 'Sum'),
            'comment_client'  => Yii::t('im', 'Comment Client'),
            'comment_manager' => Yii::t('im', 'Comment Manager'),
            'paid'            => Yii::t('im', 'Paid'),
            'status_id'       => Yii::t('site', 'Status'),
            'history'         => Yii::t('site', 'History'),
            'created_at'      => Yii::t('site', 'Created At'),
            'updated_at'      => Yii::t('site', 'Updated At'),
            'created_by'      => Yii::t('site', 'Created By'),
            'updated_by'      => Yii::t('site', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelivery()
    {
        return $this->hasOne(Delivery::className(), ['id' => 'delivery_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return Product
     */
    public function getProductLinks()
    {
        return $this->hasMany(OrderProductLink::className(), ['order_id' => 'id']);
    }


    public function beforeDelete()
    {
        foreach ($this->productLinks as $link) {
            $link->delete();
        }
        return parent::beforeDelete();
    }

}
