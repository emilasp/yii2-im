<?php
/**
 * Created by PhpStorm.
 * User: stepancher <stepancher@mail.ru>
 * Date: 07.02.16
 * Time: 11:41
 */
namespace stepancher\payeer\actions;
use yii\base\Action;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
/**
 * Это пример обработчика успешного платежа, использовать вчистую не получится!
 * Страницы success и fail делаем сами, посути просто говорим все хорошо или все плохо на них
 * Class ResultAction
 * @package stepancher\payeer\actions\
 */
class ResultAction extends Action
{
    public function run()
    {
//        LogPayment::log('status'); //полное тупое логирование входящих данных
        $post = \Yii::$app->request->post();
        $orderId = ArrayHelper::getValue($post, 'm_orderid', false);
        if (false === $orderId)
            throw new BadRequestHttpException('Missing order id');
        if (\Yii::$app->getModule('payeer')->processResult($post)) //логируем и проверяем не повтор ли
        {
            /** @var LogPayeer $log */
            $log = \Yii::$app->getModule('payeer')->log($post);
            if ($log) {
                $order = Order::findOne($orderId); // todo: ищем заказа или invoce, то что оплачивает юзер
                /** @var Invoce $invoce */
                if (is_object($order)) {
                    $log->order_find = true;
                    $log->save();
                    if ($order->verifyAutoPay($post)) { //очень советую реализоватб этот метод, проверка статуса заказа и тп
                        $log->verified = true;
                        $log->save();
                        $newPayment = $order->payment(); //создаем платеж
                        if (!$newPayment->hasErrors()) { //если без ошибок
                            $order->setStatus(Invoce::STATUS_PROCESSED);
                            $order->save(false);
                            $newPayment->run(); //запускаем
                            $log->payment_confirmed = true;
                            $log->save();
                            echo $orderId . '|success';
                            exit;
                        } else {
                            $log->payment_errors = json_encode($newPayment->errors);
                            $log->save();
                        }
                    }
                }
            }
        }
        echo $orderId . '|error';
    }
}