<?php
namespace emilasp\im\frontend;

use Yii;
use yii\helpers\ArrayHelper;
use emilasp\core\CoreModule;
use emilasp\settings\models\Setting;
use emilasp\settings\behaviors\SettingsBehavior;

/**
 * Class ImModule
 * @package emilasp\im\frontend
 */
class ImModule extends CoreModule
{
    public $eventClass = '\emilasp\im\frontend\events\ImEvent';
    
    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'setting' => [
                'class'    => SettingsBehavior::className(),
                'meta'     => [
                    'name' => Yii::t('im', 'Im'),
                    'type' => Setting::TYPE_MODULE,
                ],
                'settings' => [
                    [
                        'code'    => 'category_root_id',
                        'name'    => 'Категория ИМ',
                        'description' => 'ID root таксономии категория',
                        'default' => 1,
                    ],
                    [
                        'code'    => 'im_phone',
                        'name'    => 'Im phone: Phone number',
                        'description' => 'Номер телефона',
                        'default' => '+7 (495) 518-47-67',
                    ],
                    /*[
                        'code'    => 'page_cache_duration',
                        'name'    => 'Кеширования страниц',
                        'description' => 'Время кеширования страниц',
                        'default' => 16,
                    ],*/
                ],
            ],
        ], parent::behaviors());
    }
}
