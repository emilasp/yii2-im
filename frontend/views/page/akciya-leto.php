<?php
use emilasp\site\frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('im', 'Sport Shoes') . '   ' . Yii::$app->params['name'];

$this->params['breadcrumbs'][] = Yii::$app->params['name'];
?>
<!--h1 class="index-page"><?= Html::encode(Yii::t('im', 'Sport Shoes')) ?></h1-->


<?php $bundle = AppAsset::register($this); ?>

<div class="im-index-page">

    <h1 class="text-center">Туалетная вода в подарок</h1>

    <p>В предверии начала лета мы решили приготовить сюрприз нашим любимым клиентам и дарим Вам замечательную туалетную воду от самых известных парфюмерных домов!</p>
    
    <hr />
    
    <div class="row">
        <div class="col-md-6">
            <h3>Условия акции</h3>
            <ul>
                <li> сделать заказ на сумму более 5 000 рублей в нашем интернет магазине</li>
                <li> в комментарии к заказу написать: "Акция - лето"</li>
                <li> акция продлится до <?= date('d.m.Y', strtotime('sunday')); ?></li>
            </ul>
        </div>
        <div class="col-md-6">
            <h3>Информация</h3>

            <p>Каждому клиенту сделавшему заказ до 31 мая и указавшему в комментарии "Акция - лето" мы подарим парфюм со свежим ароматом лета.</p>

            <p>После совершения заказа с Вами свяжется наш менеджер и предоставит Вам на выбор туалетную воду Boss, Chanel, Aqua. Есть возможность выбрать как мужскую, так и женскую туалетную воду.</p>
            <p class="help-block">Ассортимент производителей и ароматов уточняйте у нашего менеджера.</p>
            
        </div>
    </div>


    <hr />
    <div class="index-page-banner-discount-all text-center">
        <img src="<?= $bundle->baseUrl ?>/images/akcii/leto.jpg" />
    </div>
    <hr />
</div>
