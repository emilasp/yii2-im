<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('im', 'Contacts');

$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="im-contacts">

    <p>Ответим на все Ваши вопросы, примем заказы и пожалания.</p>

    <div>
        <strong><i class="fa fa-phone"></i> <?= Yii::t('site', 'Phone') ?>: </strong>
         <?= Yii::$app->getModule('im')->getSetting('im_phone') ?>
    </div>

    <div>
        <strong><i class="fa fa-envelope-o"></i> Email: </strong>
        <a href="mailto:manager@sport-run.ru"> manager@sport-run.ru</a>
    </div>
<br />
<br />
</div>
