<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('im', 'Catalog');

$parents = $category->parents()->all();

foreach ($parents as $parent) {
    $this->params['breadcrumbs'][] = [
        'label' => $parent->name,
        'url' => $parent->getUrl()
    ];
}

$this->params['breadcrumbs'][] = $category->name;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="im-filter">
    <div class="im-grid-setting-row clearfix">
        <div class="float-left">
            <?= $sorter ?>
        </div>
        <div class="float-right">
            <?= $perPage ?>
        </div>
    </div>

    <div class="im-grid-filters-row clearfix">
        <?= $filters ?>
    </div>
</div>

<div class="im-index">

    <?= $catalog ?>

</div>
