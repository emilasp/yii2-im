<?php
use emilasp\site\frontend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('im', 'Sport Shoes') . '   ' . Yii::$app->params['name'];

$this->params['breadcrumbs'][] = Yii::$app->params['name'];
?>
<!--h1 class="index-page"><?= Html::encode(Yii::t('im', 'Sport Shoes')) ?></h1-->


<?php $bundle = AppAsset::register($this); ?>

<div class="im-index-page">

    <h1 class="text-center">
        <img src="<?= $bundle->baseUrl ?>/images/title-index-page.jpg" />
    </h1>


    <div class="index-page-banner index-page-banner-discount-all text-center">
        <a href="/im/page/akcii-leto.html">
            <img src="<?= $bundle->baseUrl ?>/images/akcii/leto.jpg" />
        </a>
    </div>

    <!--div class="shoes-gender-category-select row">
        <a class="col-md-6 img-men-shoes" href="<?= Url::toRoute(['/im/category/view', 'id' => 3]) ?>">
            <div class="text-index-category-select">
                <i class="fa fa-male fa-5x"></i>
            </div>
        </a>
        <a class="col-md-6 img-women-shoes" href="<?= Url::toRoute(['/im/category/view', 'id' => 4]) ?>">
            <div class="text-index-category-select">
                <i class="fa fa-female fa-5x"></i>
            </div>
        </a>
    </div-->

    <hr />
    <h1><?= Yii::t('im', 'New products') ?></h1>
    <?= $catalog ?>

</div>
