<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('im', 'Sizes');

?>
<h1><?= Html::encode($this->title) ?></h1>


<div class="im-sizes">

    <ul class="nav nav-tabs">
        <li class="active"><a data-toggle="tab" href="#men"><?= Yii::t('im', 'Men') ?></a></li>
        <li><a data-toggle="tab" href="#women"><?= Yii::t('im', 'Women') ?></a></li>
    </ul>

    <div class="tab-content">
        <div id="men" class="tab-pane fade in active clearfix">
            <table cellpadding="0" cellspacing="0" width="100%" class="table table-hover table-striped">
                <colgroup span="1"></colgroup>
                <colgroup span="2"></colgroup>
                <colgroup span="4"></colgroup>
                <thead>
                <tr>
                    <th>Российский размер</th>
                    <th>Размер стопы (см)</th>
                    <th>Америка (US)</th>
                    <th>Европа (EU)</th>
                    <th>Китай (CN)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>38</td>
                    <td>24,5</td>
                    <td>-</td>
                    <td>38</td>
                    <td>23</td>
                </tr>
                <tr>
                    <td>39</td>
                    <td>25</td>
                    <td>6</td>
                    <td>39</td>
                    <td>24</td>
                </tr>
                <tr>
                    <td>40</td>
                    <td>25,5</td>
                    <td>7</td>
                    <td>40</td>
                    <td>25</td>
                </tr>
                <tr>
                    <td>40,5</td>
                    <td>26</td>
                    <td>8</td>
                    <td>41</td>
                    <td>26</td>
                </tr>
                <tr>
                    <td>41</td>
                    <td>26,5</td>
                    <td>8,5</td>
                    <td>41,5</td>
                    <td>26,5</td>
                </tr>
                <tr>
                    <td>42</td>
                    <td>27</td>
                    <td>9</td>
                    <td>42</td>
                    <td>27</td>
                </tr>
                <tr>
                    <td>43</td>
                    <td>27,5</td>
                    <td>10</td>
                    <td>43</td>
                    <td>28</td>
                </tr>
                <tr>
                    <td>43,5</td>
                    <td>28</td>
                    <td>11</td>
                    <td>44</td>
                    <td>29</td>
                </tr>
                <tr>
                    <td>44</td>
                    <td>28,5</td>
                    <td>11,5</td>
                    <td>44,5</td>
                    <td>29,5</td>
                </tr>
                <tr>
                    <td>45</td>
                    <td>29</td>
                    <td>12</td>
                    <td>45</td>
                    <td>30</td>
                </tr>
                <tr>
                    <td>46</td>
                    <td>29,5</td>
                    <td>13</td>
                    <td>46</td>
                    <td>31</td>
                </tr>
                <tr>
                    <td>46,5</td>
                    <td>30</td>
                    <td>14</td>
                    <td>47</td>
                    <td>32</td>
                </tr>
                <tr>
                    <td>47</td>
                    <td>30,5</td>
                    <td>14,5</td>
                    <td>47,5</td>
                    <td>32,5</td>
                </tr>
                <tr>
                    <td>48</td>
                    <td>31</td>
                    <td>15</td>
                    <td>48</td>
                    <td>33</td>
                </tr>
                </tbody>
            </table>
        </div>

        <div id="women" class="tab-pane fade clearfix">
            <table cellpadding="0" cellspacing="0" width="100%" class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>Российский размер</th>
                    <th>Размер стопы (см)</th>
                    <th>Америка (US)</th>
                    <th>Европа (EU)</th>
                    <th>Китай (CN)</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>34</td>
                    <td>21,5</td>
                    <td>4</td>
                    <td>34</td>
                    <td>21</td>
                </tr>
                <tr>
                    <td>34,5</td>
                    <td>22</td>
                    <td>5</td>
                    <td>35</td>
                    <td>22</td>
                </tr>
                <tr>
                    <td>35</td>
                    <td>22,5</td>
                    <td>5,5</td>
                    <td>35,5</td>
                    <td>22,5</td>
                </tr>
                <tr>
                    <td>36</td>
                    <td>23</td>
                    <td>6</td>
                    <td>36</td>
                    <td>23</td>
                </tr>
                <tr>
                    <td>37</td>
                    <td>23,5</td>
                    <td>7</td>
                    <td>37</td>
                    <td>24</td>
                </tr>
                <tr>
                    <td>37,5</td>
                    <td>24</td>
                    <td>8</td>
                    <td>38</td>
                    <td>25</td>
                </tr>
                <tr>
                    <td>38</td>
                    <td>24,3</td>
                    <td>8,5</td>
                    <td>38,5</td>
                    <td>25,5</td>
                </tr>
                <tr>
                    <td>38,5</td>
                    <td>24,5</td>
                    <td>9</td>
                    <td>39</td>
                    <td>26</td>
                </tr>
                <tr>
                    <td>39</td>
                    <td>25</td>
                    <td>9,5</td>
                    <td>39,5</td>
                    <td>26,5</td>
                </tr>
                <tr>
                    <td>40</td>
                    <td>25,5</td>
                    <td>10</td>
                    <td>40</td>
                    <td>27</td>
                </tr>
                <tr>
                    <td>40,5</td>
                    <td>26</td>
                    <td>11</td>
                    <td>41</td>
                    <td>28</td>
                </tr>
                <tr>
                    <td>41</td>
                    <td>26,5</td>
                    <td>11,5</td>
                    <td>41,5</td>
                    <td>28,5</td>
                </tr>
                <tr>
                    <td>42</td>
                    <td>27</td>
                    <td>12</td>
                    <td>42</td>
                    <td>29</td>
                </tr>
                <tr>
                    <td>43</td>
                    <td>27,5</td>
                    <td>13</td>
                    <td>43</td>
                    <td>30</td>
                </tr>
                <tr>
                    <td>-</td>
                    <td>-</td>
                    <td>14</td>
                    <td>44</td>
                    <td>31</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>

</div>
