<?php
use emilasp\geoapp\widgets\SelectCityWidget\SelectCityUserWidget;
use emilasp\im\common\models\Delivery;
use emilasp\im\common\models\Payment;
use emilasp\json\models\DynamicModel;
use emilasp\json\widgets\DynamicFields\DynamicFields;
use kartik\widgets\Select2;
use yii\helpers\Html;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\MaskedInput;

$this->title = Yii::t('im', 'Order');

$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="im-order">

    <?php if ($count) : ?>

        <?php $form = ActiveForm::begin(); ?>

        <?php echo $form->errorSummary($model); ?>

        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'client_name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'client_lastname')->textInput(['maxlength' => true]) ?>
            </div>
            <!--div class="col-md-4">
                <?= $form->field($model, 'client_surname')->textInput(['maxlength' => true]) ?>
            </div-->
        </div>

        <hr/>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'phone')
                         ->widget(MaskedInput::className(), ['mask' => '+7 (999) 999-99-99']) ?>
            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-4"></div>
        </div>

        <hr/>
        <?php
        $addressAttributes = [];
        $city = Yii::$app->request->cookies->getValue(SelectCityUserWidget::COOKIE_KEY, null);
        if ($city) {
            $addressAttributes['city'] = $city;
        }
        ?>
        <?= DynamicFields::widget([
            'form'        => $form,
            'model'       => $model,
            'attribute'   => 'address',
            'attributes'   => $addressAttributes,
            'scheme'      => DynamicModel::SCHEME_ADDRESS,
            'addEmptyRow' => true,
        ]); ?>


        <hr/>

        <div class="row">
            <div class="col-md-6">
                <?php
                $url = '/images/delivery/';
                $format = <<< SCRIPT
function formatDelivery(model) {
    if (!model.id) return model.text;
    
    var iconClass = 'fa-archive';
    switch (model.id) {
        case '1': iconClass = 'fa-car'; break;
        case '2': iconClass = 'fa-archive'; break;
        case '3': iconClass = 'fa-rocket'; break;
    }
    
    return '<i class="fa ' + iconClass + '"></i> ' + model.text;
}
SCRIPT;
                $escape = new JsExpression("function(m) { return m; }");
                $this->registerJs($format, View::POS_HEAD);

                $deliveries = Delivery::find()->where(['status' => 1])->orderBy('order')->map()->all();


                if (!$model->delivery_id) {
                    $model->delivery_id = 1;
                }

                echo $form->field($model, 'delivery_id')->widget(Select2::className(), [
                    'data'          => $deliveries,
                    'options'       => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                        'templateResult'    => new JsExpression('formatDelivery'),
                        'templateSelection' => new JsExpression('formatDelivery'),
                        'escapeMarkup'      => $escape,
                        'allowClear'        => true,
                    ],
                ]);
                ?>

                <?php

                $url = '/images/delivery/';
                $format = <<< SCRIPT
function formatPay(model) {
    if (!model.id) return model.text;
    
    var iconClass = 'fa-money';
    switch (model.id) {
        case '1': iconClass = 'fa-cash'; break;
        case '2': iconClass = 'fa-money'; break;
        case '3': iconClass = 'fa-money'; break;
    }
    
    return '<i class="fa ' + iconClass + '"></i> ' + model.text;
}
SCRIPT;
                $escape = new JsExpression("function(m) { return m; }");
                $this->registerJs($format, View::POS_HEAD);

                $payments = Payment::find()->where(['status' => 1])->orderBy('order')->map()->all();

                if (!$model->payment_id) {
                    $model->payment_id = 1;
                }

                echo $form->field($model, 'payment_id')->widget(Select2::className(), [
                    'data'          => $payments,
                    'options'       => ['placeholder' => 'Select ...'],
                    'pluginOptions' => [
                        'templateResult'    => new JsExpression('formatPay'),
                        'templateSelection' => new JsExpression('formatPay'),
                        'escapeMarkup'      => $escape,
                        'allowClear'        => true,
                    ],
                ]);
                ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, "comment_client")->textarea(['rows' => 6]) ?>
            </div>
        </div>


        <div class="form-group text-right">
            <?= Html::submitButton(Yii::t('im', 'Checkout order'),
                ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    <?php else : ?>
        <?= Yii::t('im', 'You cart is empty') ?>
    <?php endif; ?>

</div>
