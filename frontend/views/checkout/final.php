<?php
use emilasp\im\backend\widgets\ImOrderProducts\ImOrderProducts;
use yii\helpers\Html;

$this->title = Yii::t('im', 'Order');

$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Yii::t('im', 'Final order page: h2') ?></h1>

<div class="im-order">

    <h3><?= Yii::t('im', 'Number order') ?>: <?= $order->id ?></h3>
    <p><?= Yii::t('im', 'Final order page: our manager call') ?></p>

    <?= ImOrderProducts::widget(['order' => $order]) ?>
    
    
</div>
