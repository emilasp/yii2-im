<?php
use emilasp\im\frontend\widgets\ImCatalog\ImCatalog;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalogFilter;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalogPerPage;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalogSorter;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('im', 'Catalog');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="im-filter well">
    <div class="im-grid-setting-row clearfix">
        <div class="float-left">
            <?= ImCatalogSorter::widget([]) ?>
        </div>
        <div class="float-right">
            <?= ImCatalogPerPage::widget([]) ?>
        </div>
    </div>
</div>

<?= ImCatalogFilter::widget([]) ?>


<div class="im-index well">

    <?= ImCatalog::widget([]) ?>

</div>
