<?php
use emilasp\files\models\File;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $brandGroup->name;

$this->params['breadcrumbs'][] = Yii::t('im', 'Catalog');
$this->params['breadcrumbs'][] = $this->title ;
?>
<h1><?= Html::encode($this->title) ?></h1>

<?php if ($brandGroup->images) : ?>
    <?php $img = $brandGroup->images[0]; ?>
    <img class="brand-banner" src="<?= $img->getUrl(File::SIZE_MAX) ?>" alt="<?= Yii::t('im', 'Brand') ?><?= $img->title ?>" />
<?php endif; ?>

<div class="clearfix">
    <div class="sidebar-left">
        <?= $filtersSidebar ?>
    </div>


    <div class="content-inner width-66">

        <div class="im-filter">
            <div class="im-grid-setting-row clearfix">
                <div class="float-right">
                    <?= $perPage ?>
                </div>
                <div class="float-right">
                    <?= $sorter ?>
                </div>
            </div>

            <div class="im-grid-filters-row clearfix">
                <?= $filters ?>
            </div>
        </div>

        <div class="im-index">

            <?= $catalog ?>

        </div>

    </div>


</div>

