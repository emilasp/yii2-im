<?php
use emilasp\core\assets\OwlCaruselAsset;
use emilasp\files\models\File;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
?>
    <section class="im-product product-modal-view  container" data-url="<?= Url::toRoute('/im/cart/change') ?>">

        <div class="row">
            <div class="col-md-5">
                <div class="product-view-image">
                    <?php if (count($model->images) > 1) : ?>
                        <div class="image-nav-prev "><i class="fa fa-chevron-circle-left fa-3x"></i></div>
                        <div class="image-nav-next"><i class="fa fa-chevron-circle-right fa-3x"></i></div>
                        <div class="product-images owl-carousel owl-theme">
                            <?php foreach ($model->images as $image) : ?>
                                <div class="item">
                                    <img src="<?= $image->getUrl(File::SIZE_MED) ?>"
                                         data-src-retina="<?= $image->getUrl(File::SIZE_MAX) ?>"
                                         alt="<?= $image->title ?>"/>
                                </div>

                            <?php endforeach; ?>
                        </div>
                    <?php else : ?>
                        <?php $imageFirst = $model->getImage(); ?>
                        <div class="active">
                            <img src="<?= $imageFirst->getUrl(File::SIZE_MED) ?>" alt="<?= $imageFirst->title ?>"/>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-md-7">
                <h2><?= $model->name ?></h2>
                <h4><?= $model->categories[0]->name ?></h4>
                <div class="clearfix">
                    <?php
                    $priceBase = $model->getPrice(true);
                    $price     = $model->getPrice();
                    $discount  = $model->getDiscountPercent();
                    $delta     = $priceBase - $price;
                    ?>
                    <?php if ($model->isDiscount()) : ?>
                        <div class="col-md-12 ceil-price text-right">
                            <span class="product-price price-old">
                            <?= number_format($priceBase, 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                            </span>
                            <span class="product-price price-sale">
                            <?= number_format($price, 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                            </span>
                            <div class="discount">
                                <span class="green">
                                <?= Yii::t('im', 'Discount') ?>:
                                    <?= $discount ?>%
                                </span>
                                <span>
                                     <?= Yii::t('im', 'Delta') ?>:
                                    <?= number_format($delta, 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                                </span>
                            </div>
                        </div>
                    <?php else : ?>
                        <div class="col-md-12 ceil-price text-right">
                            <?= number_format($price, 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                        </div>
                    <?php endif ?>
                </div>

                <div class="row product-variation-block">
                    <div class="col-md-7">
                        <?php $variations = $model->getVariationValues(); ?>
                        <?php if ($variations) : ?>
                            <span><?= Yii::t('im', 'Select variations') ?>: </span>
                            <?php foreach ($variations as $property_id => $variateValues) : ?>
                                <?php foreach ($model->properties as $property) : ?>
                                    <?php if ($property_id === $property->id) : ?>
                                        <div class="product-variation form-horizontal">

                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">
                                                    <?= $property->name ?>
                                                </label>
                                        <span class="col-sm-7">
                                            <select class="form-control variated-select">
                                                <option value=""><?= Yii::t('site', '-select-') ?></option>
                                                <?php foreach ($variateValues as $value) : ?>
                                                    <option value="<?= $value->id ?>"><?= $value->value ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </span>
                                            </div>


                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-5 text-right">
                        <button class="btn btn-success add-to-cart" data-id="<?= $model->id ?>">
                            <?= Yii::t('im', 'Add to cart') ?>
                        </button>

                    </div>
                </div>

                <div class="product-info">
                    <h3><?= Yii::t('im', 'Detail information') ?></h3>
                    <?php
                    $attributes = [];

                    $attributes['code'] = ['label' => Yii::t('site', 'Code'), 'value' => $model->getCode()];

                    foreach ($model->properties as $property) {
                        if (!$property->is_variation) {
                            $values = [];
                            foreach ($model->values as $value) {
                                if ($property->id === $value->property_id) {
                                    $values[] = $value->value;
                                }
                            }
                            $attributes[$property->id] = [
                                'label' => $property->name,
                                'value' => implode(', ', $values),
                            ];
                        }
                    }
                    ?>


                    <?= DetailView::widget([
                        'model'      => $model,
                        'attributes' => $attributes,
                    ]) ?>
                </div>

            </div>
        </div>

        <div class="product-text"><?= $model->text ?></div>

    </section>

<?php
OwlCaruselAsset::register($this);
$js = <<<JS
       
JS;

$this->registerJs($js);