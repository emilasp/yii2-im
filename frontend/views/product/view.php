<?php
use emilasp\core\assets\OwlCaruselAsset;
use emilasp\files\models\File;
use emilasp\im\common\models\ProductSphinx;
use emilasp\im\frontend\components\ViewedProductsComponent;
use emilasp\im\frontend\widgets\ImAssocProducts\ImAssocProducts;
use emilasp\site\frontend\assets\AppAsset;
use emilasp\user\core\models\UserIssue;
use emilasp\user\core\widgets\UserIssueWidget\UserIssueWidget;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->name;

$parents = $model->categories[0]->parents()->all();

foreach ($parents as $parent) {
    $this->params['breadcrumbs'][] = [
        'label' => $parent->name,
        'url'   => $parent->getUrl(),
    ];
}

$this->params['breadcrumbs'][] = [
    'label' => $model->categories[0]->name,
    'url'   => $model->categories[0]->getUrl(),
];


$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="im-product">

    <section class="im-product product-modal-view  container" data-url="<?= Url::toRoute('/im/cart/change') ?>">

        <div class="row">
            <div class="col-md-5">
                <div class="product-view-image">
                    <?php if (count($model->images) > 1) : ?>
                        <div class="image-nav-prev "><i class="fa fa-chevron-circle-left fa-3x"></i></div>
                        <div class="image-nav-next"><i class="fa fa-chevron-circle-right fa-3x"></i></div>
                        <div class="product-images owl-carousel owl-theme">
                            <?php foreach ($model->images as $image) : ?>
                                <div class="item">
                                    <img src="<?= $image->getUrl(File::SIZE_MED) ?>"
                                         data-src-retina="<?= $image->getUrl(File::SIZE_MAX) ?>"
                                         alt="<?= $image->title ?>"/>
                                </div>

                            <?php endforeach; ?>
                        </div>
                    <?php else : ?>
                        <?php $imageFirst = $model->getImage(); ?>
                        <div class="active">
                            <img src="<?= $imageFirst->getUrl(File::SIZE_MED) ?>" alt="<?= $imageFirst->title ?>"/>
                        </div>
                    <?php endif ?>
                </div>
            </div>
            <div class="col-md-7">

                <div class="row">
                    <div class="col-xs-6"><h4><?= $model->categories[0]->name ?></h4></div>
                    <div class="col-xs-6">
                        <div class="clearfix">
                            <?php
                            $priceBase = $model->getPrice(true);
                            $price     = $model->getPrice();
                            $discount  = $model->getDiscountPercent();
                            $delta     = $priceBase - $price;
                            ?>
                            <?php if ($model->isDiscount()) : ?>
                                <div class="col-md-12 ceil-price text-right">
                            <span class="product-price price-old">
                            <?= number_format($priceBase, 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                            </span>
                            <span class="product-price price-sale">
                            <?= number_format($price, 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                            </span>
                                    <div class="discount">
                                <span class="green">
                                <?= Yii::t('im', 'Discount') ?>:
                                    <?= $discount ?>%,
                                </span>
                                <span>
                                     <?= Yii::t('im', 'Delta') ?>:
                                    <?= number_format($delta, 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                                </span>
                                    </div>
                                </div>
                            <?php else : ?>
                                <div class="col-md-12 ceil-price text-right">
                                    <?= number_format($price, 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>

                <div class="row product-variation-block">
                    <div class="col-md-7">
                        <?php $variations = $model->getVariationValues(); ?>
                        <?php if ($variations) : ?>
                            <span><?= Yii::t('im', 'Select variations') ?>: </span>
                            <?php foreach ($variations as $property_id => $variateValues) : ?>
                                <?php foreach ($model->properties as $property) : ?>
                                    <?php if ($property_id === $property->id) : ?>
                                        <div class="product-variation form-horizontal">

                                            <div class="form-group">
                                                <label class="col-sm-5 control-label">
                                                    <?= $property->name ?>
                                                </label>
                                        <span class="col-sm-7">
                                            <select class="form-control variated-select">
                                                <option value=""><?= Yii::t('site', '-select-') ?></option>
                                                <?php foreach ($variateValues as $value) : ?>
                                                    <option value="<?= $value->id ?>"><?= $value->value ?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </span>
                                            </div>


                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                    <div class="col-md-5 text-right">
                        <button class="btn btn-success add-to-cart" data-id="<?= $model->id ?>">
                            <?= Yii::t('im', 'Add to cart') ?>
                        </button>

                    </div>
                </div>

                <div class="product-info">


                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#text">
                                <?= Yii::t('site', 'Description') ?>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#characters">
                                <?= Yii::t('im', 'Characters') ?>
                            </a>
                        </li>
                        <li>
                            <a data-toggle="tab" href="#product-answer">
                                <?= Yii::t('im', 'Product answer') ?>
                            </a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div id="text" class="tab-pane fade in active clearfix">

                            <?php
                            $attributes = [];

                            $attributes['article'] = ['label' => Yii::t('im', 'Article'), 'value' => $model->article];

                            if ($model->brand) {
                                $attributes['brand'] = [
                                    'label'  => Yii::t('im', 'Brand'),
                                    'value'  => Html::a($model->brand->name, $model->brand->getUrl()),
                                    'format' => 'raw',
                                ];
                            }
                            if ($model->categories) {
                                $attributes['category'] = [
                                    'label'  => Yii::t('im', 'Category'),
                                    'value'  => Html::a($model->categories[0]->name, $model->categories[0]->getUrl()),
                                    'format' => 'raw',
                                ];
                            }

                            $attributes['sex'] = [
                                'label' => Yii::t('im', 'Sex'),
                                'value' => $model->sexes[$model->sex],
                            ];

                            if ($model->text) {
                                $attributes['text'] = [
                                    'label' => Yii::t('im', 'Description'),
                                    'value' => $model->text,
                                ];
                            }
                            ?>

                            <?= DetailView::widget([
                                'model'      => $model,
                                'attributes' => $attributes,
                            ]) ?>

                        </div>

                        <div id="characters" class="tab-pane fade clearfix">
                            <?php
                            $attributes = [];

                            $attributes['article'] = ['label' => Yii::t('im', 'Article'), 'value' => $model->article];
                            $attributes['code']    = ['label' => Yii::t('site', 'Code'), 'value' => $model->getCode()];

                            foreach ($model->properties as $property) {
                                if (!$property->is_variation) {
                                    $values = [];
                                    foreach ($model->values as $value) {
                                        if ($property->id === $value->property_id) {
                                            $values[] = $value->value;
                                        }
                                    }
                                    $attributes[$property->id] = [
                                        'label' => $property->name,
                                        'value' => implode(', ', $values),
                                    ];
                                }
                            }
                            ?>


                            <?= DetailView::widget([
                                'model'      => $model,
                                'attributes' => $attributes,
                            ]) ?>
                        </div>

                        <div id="product-answer" class="tab-pane fade clearfix">
                            <?= UserIssueWidget::widget([
                                'type'  => UserIssue::TYPE_PRODUCT,
                                'title' => $model->article,
                            ]) ?>
                        </div>
                    </div>


                </div>


                <?php $bundle = AppAsset::register($this); ?>


                <div class="akcii-banners owl-carousel owl-theme">
                    <div class="item">
                        <a href="/im/page/akcii-leto.html">
                            <img src="<?= $bundle->baseUrl ?>/images/akcii/free-delivery-min.jpg"
                                 alt="Акция бесплатная доставка"/>
                        </a>
                    </div>
                    <div class="item">
                        <img src="<?= $bundle->baseUrl ?>/images/akcii/leto-min.jpg"
                             alt="Акция лето"/>
                    </div>
                </div>


            </div>
        </div>

        <?= ImAssocProducts::widget([
            'label'  => Yii::t('im', 'Similar products'),
            'model'  => $model,
            'models' => function ($model) {
                $query = ProductSphinx::getProductQueryByIds(
                    ProductSphinx::getSphinxSimilarProductsIds($model)
                );
                return $query->all();
            },
        ]) ?>

        <?= ImAssocProducts::widget([
            'label'  => Yii::t('im', 'Viewed products'),
            'model'  => $model,
            'models' => function ($model) {
                return (new ViewedProductsComponent())->getProducts($model->id);
            },
        ]) ?>

    </section>

    <?php
    OwlCaruselAsset::register($this);

    $js = <<<JS
    
    $('.akcii-banners').owlCarousel({
            margin: 0,
            lazyLoad: false,
            loop: false,
            items: 1,
            autoplay:true,
            autoplayTimeout:5000,
            autoplayHoverPause:true,
            animateOut: 'fadeOut'
        });
        
        $('.product-images.owl-carousel').each(function () {
        var _this = $(this);
        _this.owlCarousel({
            margin: 0,
            lazyLoad: true,
            loop: true,
            items: 1
        });
        
        var items = _this.find('.item');
        
        items.on('click', function() {
            _this.trigger('next.owl.carousel');
        });
        
        _this.closest('.product-view-image').find(".image-nav-prev").click(function () {
            _this.trigger('prev.owl.carousel');
        });

        _this.closest('.product-view-image').find(".image-nav-next").click(function () {
            _this.trigger('next.owl.carousel');
        });
    });
JS;

    $this->registerJs($js);
    ?>

</div>
