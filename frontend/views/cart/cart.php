<?php
use emilasp\files\models\File;
use kartik\widgets\TouchSpin;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('im', 'Cart');

$this->params['breadcrumbs'][] = $this->title;
?>
<h1><?= Html::encode($this->title) ?></h1>

<div class="im-cart">

    <div class="cart-items">
        <?php foreach ($items as $id => $hashs) : ?>
            <?php foreach ($hashs as $hash => $item) : ?>
                <?php $product = $products[$id] ?>
                <?= Html::beginTag('div', [
                    'class' => 'cart-item row',
                    'data'  => [
                        'id'     => $id,
                        'hash'   => $hash,
                        'count'  => $item['count'],
                        'url'    => Url::toRoute('/im/cart/change-count'),
                        'remove' => 0,
                    ],
                ]) ?>
                <div class="col-xs-2">
                    <img src="<?= $product->getImage()->getUrl(File::SIZE_MIN) ?>"/>
                </div>
                <div class="col-xs-7 padding-top-20">
                    <a href="<?= $product->getUrl() ?>" class="cart-page-name-item"><?= $product->name ?></a>

                    <div class="variations">
                        <?php $variations = $product->getVariationValues(); ?>
                        <?php if ($variations) : ?>
                            <?php foreach ($variations as $property_id => $variateValues) : ?>
                                <?php foreach ($product->properties as $property) : ?>
                                    <?php if ($property_id === $property->id) : ?>
                                        <div class="product-variation">

                                            <strong><?= $property->name ?>: </strong>
                                            <i>
                                                <?php foreach ($variateValues as $value) : ?>
                                                    <?php foreach ($item['variations'] as $cartVariate) : ?>
                                                        <?php if (in_array($value->id, $cartVariate)) : ?>
                                                            <span><?= $value->value ?></span>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endforeach; ?>
                                            </i>

                                        </div>
                                    <?php endif; ?>
                                <?php endforeach; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>

                    <div class="row">
                        <?php if ($product->categories) : ?>
                            <div class="col-md-6 ceil-category">
                                <a href="<?= $product->categories[0]->getUrl() ?>">
                                    <?= $product->categories[0]->name ?>
                                </a>
                            </div>
                        <?php endif ?>
                        <?php if ($product->brand) : ?>
                            <div class="col-md-6 ceil-brand">
                                <a href="<?= $product->brand->getUrl() ?>">
                                    <?= $product->brand->name ?>
                                </a>
                            </div>
                        <?php endif ?>
                    </div>

                </div>
                <div class="col-xs-2 padding-top-20">
                    <?= TouchSpin::widget([
                        'name'          => 'volume',
                        'value'         => $item['count'],
                        'options'       => ['class' => 'cart-item-spinner'],
                        'pluginOptions' => [
                            'step'             => 1,
                            'min' => 0,
                            'max' => 100,
                            'buttonup_class'   => 'cart-page-spninner-up',
                            'buttondown_class' => 'cart-page-spninner-down',
                            'buttonup_txt'     => '<i class="fa fa-plus"></i>',
                            'buttondown_txt'   => '<i class="fa fa-minus"></i>',
                        ],
                    ]) ?>
                    <div class="cart-page-item-price"><?= $product->getPrice() ?> <?= Yii::t('im', 'Rub') ?></div>
                </div>
                <div class="col-xs-1 padding-top-20">

                    <div class="text-right">
                        <button class="btn btn-danger btn-xs btn-cart-remove">
                            <i class="fa fa-times"></i>
                        </button>
                    </div>
                </div>
                <?= Html::endTag('div') ?>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
    <hr/>

    <div class="row">
        <div class="col-md-12 text-right">
            <a href="<?= Url::toRoute('/im/checkout') ?>" class="btn btn-success">
                <?= Yii::t('im', 'Checkout') ?>
            </a>
        </div>
    </div>

</div>

