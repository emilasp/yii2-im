<div class="cart-page-sidebar">
    <h4><?= Yii::t('im', 'Total summ') ?>:</h4>
    <div class="text-right">
        <span class="cart-total-summ cart-total"><?= number_format($summ) ?></span>
        <?= Yii::t('im', 'Rub') ?>
    </div>

    
    <h4><?= Yii::t('im', 'Count products') ?>: </h4>
    <div class="text-right">
        <span class="cart-count-title cart-total">
            <?= $count ?> <?= Yii::t('im', 'Un') ?>
        </span>
    </div>
     
</div>
