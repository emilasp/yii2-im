<?php
namespace emilasp\im\frontend\assets;

use yii\web\AssetBundle;

/**
 * Class ImAsset
 * @package emilasp\im\backend\assets
 */
class ImAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl  = '@web';
    public $css      = [];
    public $js       = [];
    public $depends  = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
