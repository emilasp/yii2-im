<?php
namespace emilasp\im\frontend\controllers;

use emilasp\im\common\models\Brand;
use emilasp\im\common\models\BrandGroup;
use emilasp\im\common\models\ImCategory;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalog;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalogFilter;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalogPerPage;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalogSorter;
use emilasp\im\frontend\widgets\ImSidebar\ImSidebar;
use emilasp\taxonomy\models\PropertyGroup;
use yii;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * BrandGroupController
 */
class BrandGroupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index', 'view'],
                'rules'        => [
                    [
                        'actions' => [
                            'index',
                            'view',
                        ],
                        'allow'   => true,
                        'roles'   => ['@', '?'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Goal models.
     * @return mixed
     */
    public function actionIndex()
    {
        //$searchModel = new GoalSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            //'searchModel' => $searchModel,
            //'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $category = ImCategory::findOne(1);
        $brandGroup    = $this->findModel($id, BrandGroup::className(), true);
        $brand    = Brand::findOne($brandGroup->brand_id);

        $catalog = ImCatalog::widget(['brand' => $brand, 'brandGroup' => $brandGroup, 'category' => $category]);

        $sorter         = ImCatalogSorter::widget([]);
        $perPage        = ImCatalogPerPage::widget([]);
        $filters        = ImCatalogFilter::widget([]);
        $filtersSidebar = ImCatalogFilter::widget(['viewType' => PropertyGroup::VIEW_TYPE_SIDEBAR_FILTER]);

        return $this->render('view', [
            'brandGroup'          => $brandGroup,
            'sorter'         => $sorter,
            'perPage'        => $perPage,
            'filters'        => $filters,
            'filtersSidebar' => $filtersSidebar,
            'catalog'        => $catalog,
        ]);
    }
}
