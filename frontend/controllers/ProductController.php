<?php
namespace emilasp\im\frontend\controllers;


use emilasp\core\helpers\StringHelper;
use emilasp\files\models\File;
use emilasp\im\common\models\Product;
use emilasp\im\common\models\ProductSphinx;
use emilasp\im\frontend\components\ViewedProductsComponent;
use yii;
use emilasp\core\components\base\Controller;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProductController
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['view-ajax', 'view', 'search'],
                'rules'        => [
                    [
                        'actions' => [
                            'view-ajax',
                            'view',
                            'search',
                        ],
                        'allow'   => true,
                        'roles'   => ['@', '?'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'view-ajax' => ['POST'],
                ],
            ],
        ];
    }


    /** Быстрый просмотр
     *
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionViewAjax($id)
    {
        $model = $this->findModel($id, Product::className());

        (new ViewedProductsComponent())->addProduct($model);
        
        return $this->renderPartial('view-ajax', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model = $this->findModel($id, Product::className(), true);

        (new ViewedProductsComponent())->addProduct($model);
        
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Отадём результаты поиска
     */
    public function actionSearch()
    {
        $results = [];
        $row     = [];
        $term    = Yii::$app->request->get('q');
        if ($term) {
            if ($sphinxIds = ProductSphinx::getSphinxSearchProductsIds($term)) {
                $query = Product::find()->with('images')
                                ->where(['im_product.status' => Product::STATUS_ENABLED])
                                ->andWhere(['im_product.id' => $sphinxIds])
                                ->orderBy(
                                    new Expression('idx(array[' . implode(',', $sphinxIds) . '],im_product.id )')
                                );

                $products = $query->limit(10)->all();

                foreach ($products as $product) {
                    $row['id']        = $product->id;
                    $row['name']      = $product->name;
                    $row['cost_base'] = $product->getPrice(true);
                    $row['cost']      = $product->getPrice();
                    $row['text']      = StringHelper::truncateString($product->text, 200, ' ', '..');
                    $row['image']     = $product->getImage()->getUrl(File::SIZE_ICO);
                    array_push($results, $row);
                }
            }
        } else {
            $row['id']        = 0;
            $row['name']      = '....';
            $row['cost_base'] = '';
            $row['cost']      = '';
            $row['text']      = '';
            $row['image']     = '';
            array_push($results, $row);
        }

        echo json_encode(['results' => $results]);
    }

    /** Костыль на проверку csrf для renderAjax
     * @param yii\base\Action $action
     *
     * @return bool
     */
    public function beforeAction($action) {
        $this->enableCsrfValidation = ($action->id !== 'view-ajax');
        return parent::beforeAction($action);
    }
}
