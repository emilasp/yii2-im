<?php
namespace emilasp\im\frontend\controllers;

use emilasp\im\frontend\components\CartComponent;
use emilasp\im\frontend\widgets\ImCart\ImCart;
use yii;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CartController
 */
class CartController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['changeCount', 'change', 'cart'],
                'rules'        => [
                    [
                        'actions' => [
                            'changeCount',
                            'change',
                            'cart',
                        ],
                        'allow'   => true,
                        'roles'   => ['@', '?'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'change'      => ['POST'],
                    'changeCount' => ['POST'],
                ],
            ],
        ];
    }


    /** Страница корзины
     * @return string
     */
    public function actionIndex()
    {
        $cartComponent = new CartComponent();

        $rightView = $this->renderPartial('cartSidebar', [
            'count'    => $cartComponent->data['count'],
            'summ'     => $cartComponent->data['summ'],
        ]);

        $this->view->params['sidebar']['right'] = $rightView;

        return $this->render('cart', [
            'items'    => $cartComponent->data['items'],
            'cartVariations'    => $cartComponent->data['items'],
            'products' => $cartComponent->items,
        ]);
    }

    /** Изменяем товары в корзине
     * @return mixed
     */
    public function actionChangeCount()
    {
        $cartComponent = new CartComponent();
        $count         = $cartComponent->data['count'];
        $summ          = number_format($cartComponent->data['summ']);
        return json_encode([1, 'count' => $count, 'summ' => $summ]);
    }

    /** Обновляем корзину
     * @return string
     * @throws \Exception
     */
    public function actionChange()
    {
        return ImCart::widget();
    }
}
