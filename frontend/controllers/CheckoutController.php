<?php
namespace emilasp\im\frontend\controllers;

use emilasp\im\common\models\forms\OrderForm;
use emilasp\im\common\models\Order;
use emilasp\im\common\models\Payment;
use emilasp\im\frontend\components\CartComponent;
use emilasp\im\frontend\components\CheckoutComponent;
use yii;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CheckoutController
 */
class CheckoutController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index', 'payment', 'delivery'],
                'rules'        => [
                    [
                        'actions' => [
                            'index',
                            'payment',
                            'delivery',
                        ],
                        'allow'   => true,
                        'roles'   => ['@', '?'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }

    /** @var  CheckoutComponent */
    public $checkoutComponent;

    /**
     * Lists all Goal models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var OrderForm $model */
        $model = $this->checkoutComponent->getModel();
        $model->setData();

        $cartComponent = new CartComponent();
        $rightView = $this->renderPartial('checkoutSidebar', [
            'count'    => $cartComponent->data['count'],
            'summ'     => $cartComponent->data['summ'],
        ]);

        $this->view->params['sidebar']['right'] = $rightView;


        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $this->checkoutComponent->setLastOrderId($model->id);

            return $this->redirect(['final']);
        } else {
            return $this->render('order', [
                'count' => $cartComponent->data['count'],
                'model' => $model,
            ]);
        }
    }


    /**
     * @return string
     * @throws yii\web\ForbiddenHttpException
     */
    public function actionFinal()
    {
        $lastOrderId = $this->checkoutComponent->getLastOrderId();
        
        if (!$lastOrderId) {
            throw new yii\web\ForbiddenHttpException('Forbidden');
        }
        
        $order = Order::findOne($lastOrderId);
        
        if ($order->payment->code !== Payment::CODE_CASHE) {
            //links to pay
            return $this->render('final', [
                'order' => $order,
            ]);
        } else {
            return $this->render('final', [
                'order' => $order,
            ]); 
        }
    }


    /**
     * Lists all Goal models.
     * @return mixed
     */
    /*public function actionDelivery()
    {
        $order = [];
        return $this->render('delivery', [
            'model' => $order,
        ]);
    }*/

    /**
     * Lists all Goal models.
     * @return mixed
     */
    /*public function actionPayment()
    {
        $order = [];
        return $this->render('payment', [
            'model' => $order,
        ]);
    }*/

    public function beforeAction($action)
    {
        $this->checkoutComponent = new CheckoutComponent();

        return parent::beforeAction($action);
    }
}
