<?php
namespace emilasp\im\frontend\controllers;

use emilasp\im\common\models\ImCategory;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalog;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalogFilter;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalogPerPage;
use emilasp\im\frontend\widgets\ImCatalog\ImCatalogSorter;
use emilasp\im\frontend\widgets\ImSidebar\ImSidebar;
use emilasp\taxonomy\models\PropertyGroup;
use yii;
use emilasp\core\components\base\Controller;
use yii\filters\AccessControl;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PageController
 */
class PageController extends Controller
{
    private $currentCategory;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class'        => AccessControl::className(),
                'only'         => ['index'],
                'rules'        => [
                    [
                        'actions' => [
                            'index',
                        ],
                        'allow'   => true,
                        'roles'   => ['@', '?'],
                    ],
                ],
                'denyCallback' => Yii::$app->getModule('user')->denyCallback,
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['POST'],
                ],
            ],
        ];
    }


    /**
     * Lists all Goal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $currentCategory = $this->findModel(1, ImCategory::className());

        $catalog = ImCatalog::widget(['category' => $currentCategory, 'conditions' => ['on_index' => 1]]);

        return $this->render('index', [
            'catalog' => $catalog,
        ]);
    }

    /**
     * Lists all Goal models.
     * @return mixed
     */
    public function actionDelivery()
    {
        return $this->render('delivery', []);
    }

    /**
     * Lists all Goal models.
     * @return mixed
     */
    public function actionAkciiLeto()
    {
        return $this->render('akciya-leto', []);
    }

    /**
     * Lists all Goal models.
     * @return mixed
     */
    public function actionSizes()
    {
        return $this->render('sizes', []);
    }

    /**
     * Lists all Goal models.
     * @return mixed
     */
    public function actionContacts()
    {
        return $this->render('contacts', []);
    }
}
