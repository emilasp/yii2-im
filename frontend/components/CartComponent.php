<?php
namespace emilasp\im\frontend\components;

use emilasp\im\common\models\Product;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\web\Cookie;

/**
 * Class CartComponent
 * @package emilasp\im\frontend\components
 */
class CartComponent extends Component
{
    const COOKIE_CART_KEY = 'it';

    public $items = [];

    public $data = [
        'items' => [],
        'count' => 0,
        'summ'  => 0,
    ];

    public $structCartItemRequest = [
        'variatons' => [],
        'count'     => 1,
    ];


    public function init()
    {
        parent::init();

        $this->setData();
        $this->pjaxAction();
        $this->setItems();
    }

    /** Add product to cookie
     *
     * @param $data
     */
    public function addItem($data)
    {
        foreach ($data as $id => $item) {
            $count = $item['count'];
            if (!isset($item['hash'])) {
                $variations = $item['variations'];
                $hash       = $this->getHashVariations($variations);
            } else {
                $hash = $item['hash'];
            }

            if (!isset($this->data['items'][$id][$hash]['count'])) {
                $this->data['items'][$id][$hash]['count'] = 1;
            } else {
                $this->data['items'][$id][$hash]['count'] = $count;
            }
            if (isset($item['variations'])) {
                $this->data['items'][$id][$hash]['variations'] = $variations;
            }
        }
    }

    /** Remove product from cookie
     *
     * @param $data
     */
    public function removeItem($data)
    {
        foreach ($this->data['items'] as $itemId => $itemData) {
            $count      = $itemData['count'];
            $variations = $itemData['variations'];
            $hash       = md5(serialize($variations));

            foreach ($data as $id => $item) {
                $countRemove      = $item['count'];
                $variationsRemove = $item['variations'];
                $hashRemove       = $this->getHashVariations($variationsRemove);

                if ($id == $itemId && $hash === $hashRemove) {
                    if ($count > 1) {
                        $this->data['items'][$itemId][$hash] -= $countRemove;
                    } else {
                        unset($this->data['items'][$itemId][$hash]);
                    }
                    break;
                }
            }
        }
    }

    /**
     * Очищаем корзину
     */
    public function dropCart()
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name'  => self::COOKIE_CART_KEY,
            'value' => null,
        ]));
    }

    /**
     * Обновлем куки
     */
    private function updateCookie()
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name'  => self::COOKIE_CART_KEY,
            'value' => $this->data,
        ]));
    }

    /**
     * Get products from cookie
     */
    private function setData()
    {
        $this->data = Yii::$app->request->cookies->getValue(self::COOKIE_CART_KEY, $this->data);
    }

    /**
     * Get products from cookie
     */
    private function setItems()
    {
        $ids   = array_keys($this->data['items']);
        $items = Product::findAll($ids);

        foreach ($items as $product) {
            $this->items[$product->id] = $product;
        }

        foreach ($this->data['items'] as $id => $hashs) {
            foreach ($hashs as $hash => $item) {
                $itemCount = $item['count'];

                if ($itemCount) {
                    $this->data['count'] += $itemCount;
                    $this->data['summ'] += $itemCount * $this->items[$id]->getPrice();
                } else {
                    unset($this->data['items'][$id][$hash]);
                }
            }
        }
    }

    /** Формируем хеш из вариаций
     *
     * @param $variations
     *
     * @return mixed
     */
    private function getHashVariations($variations)
    {
        ksort($variations);
        return md5(serialize($variations));
    }

    /**
     * Add and remove products from cookie
     */
    private function pjaxAction()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post('object') === 'cart') {
            if ($action = Yii::$app->request->post('action')) {
                $data = Yii::$app->request->post('data');
                if ($action === 'add') {
                    $this->addItem($data);
                } elseif ($action === 'remove') {
                    $this->removeItem($data);
                }
                $this->updateCookie();
            }
        }
    }
}
