<?php
namespace emilasp\im\frontend\components;

use emilasp\im\common\models\Product;
use yii;
use yii\base\Component;
use yii\web\Cookie;

/**
 * Class ViewedProductsComponent
 * @package emilasp\im\frontend\components
 */
class ViewedProductsComponent extends Component
{
    const COOKIE_VIEWED_KEY  = 'wid';
    const COUNT_SAVE_PRODUCT = 20;

    /** Добавляем продукт в просмотренные товары
     *
     * @param $category
     */
    public function addProduct(Product $product)
    {
        $ids = $this->getIdsProducts();
        if (!in_array($product->id, $ids)) {
            $ids[] = $product->id;
        }
        $count = count($ids);
        if ($count > self::COUNT_SAVE_PRODUCT) {
            $ids = array_slice($ids, self::COUNT_SAVE_PRODUCT * -1);
        }
        Yii::$app->response->cookies->add(new Cookie([
            'name'  => self::COOKIE_VIEWED_KEY,
            'value' => $ids,
        ]));
    }
    
    /** Получаем все просмотренные продукты
     * @param integer|null $id
     *
     * @return array|yii\db\ActiveRecord[]
     */
    public function getProducts($id)
    {
        $ids = $this->getIdsProducts();

        if ($id) {
            if (($key = array_search($id, $ids)) !== false) {
                unset($ids[$key]);
            }
        }

        $products = Product::find()->byStatus()->with(['images', 'categories'])
            ->where(['id' => $ids])->limit(self::COUNT_SAVE_PRODUCT)->all();
        return $products;
    }

    /** Получаем все id последних просмотренных товаров
     * @return mixed
     */
    private function getIdsProducts()
    {
        return Yii::$app->request->cookies->getValue(self::COOKIE_VIEWED_KEY, []);
    }
}
