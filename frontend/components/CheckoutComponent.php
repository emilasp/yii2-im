<?php
namespace emilasp\im\frontend\components;

use emilasp\im\common\models\forms\OrderForm;
use emilasp\im\common\models\Order;
use yii;
use yii\base\Component;
use yii\web\Cookie;

/**
 * Class CheckoutComponent
 * @package emilasp\im\frontend\components
 */
class CheckoutComponent extends Component
{
    const COOKIE_CHECKOUT_KEY = 'checkout';
    const COOKIE_LAST_ORDER_KEY = 'last_order';

    public $data = [];

    private $model = [];

    public function init()
    {
        parent::init();

        $this->setData();
        $this->pjaxAction();
        $this->setItems();
    }

    public function getModel()
    {
        return $this->model;
    }



    /**
     * Добавляем последний заказ в сессию
     */
    public function setLastOrderId($id)
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name'  => self::COOKIE_LAST_ORDER_KEY,
            'value' => $id,
        ]));
    }

    /** Получаем последний заказ из сессии
     * @return mixed|null
     */
    public function getLastOrderId()
    {
        return Yii::$app->request->cookies->getValue(self::COOKIE_LAST_ORDER_KEY);;
    }


    /**
     * Обновлем куки
     */
    private function updateCookie()
    {
        Yii::$app->response->cookies->add(new Cookie([
            'name'  => self::COOKIE_CHECKOUT_KEY,
            'value' => $this->model,
        ]));
    }

    /**
     * Get products from cookie
     */
    private function setData()
    {
        $this->model = new OrderForm();
        
        $this->data = Yii::$app->request->cookies->getValue(self::COOKIE_CHECKOUT_KEY, $this->model->getAttributes());
        if ($this->data) {
            if (!empty($this->data['order_id'])) {
                $this->model = OrderForm::findOne($this->data['order_id']);
            } elseif (!empty($this->data['data'])) {
                $this->model->setAttributes($this->data['data']);
            }
        }
    }

    /**
     * Get products from cookie
     */
    private function setItems()
    {
        
    }

    /**
     * Add and remove products from cookie
     */
    private function pjaxAction()
    {
        if (Yii::$app->request->isAjax && Yii::$app->request->post('object') === 'checkout') {
            switch (Yii::$app->request->post('action')) {
                case 'order': break;
                case 'delivery': break;
                case 'payment': break;
            }
            $this->updateCookie();
        }
    }
    
}
