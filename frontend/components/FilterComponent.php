<?php
namespace emilasp\im\frontend\components;

use emilasp\im\common\models\ProductSphinx;
use emilasp\taxonomy\models\Property;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class FilterComponent
 * @package emilasp\im\frontend\components
 */
class FilterComponent extends Component
{
    public $queryLimit;
    public $queryFacetsLimit;

    public $values = [];

    public $facetBrandLimit = false;
    public $facetBrandGroupLimit = false;

    public $defaultPerPage = 20;
    public $defaultSorter  = 'cost_asc';

    public $groupId     = 'filters';
    public $categoryIds = [];

    public $categoryAllFacet;
    public $facets;
    public $facetsAll;
    public $priceRange;
    public $search;
    public $brand;
    public $brandGroup;


    private $dataFromSession;

    private $id;

    public function init()
    {
        parent::init();

        $this->id = Yii::$app->controller->id;
    }

    /** Устанавливаем фильтры из поста и сессии
     *
     * @param $category
     */
    public function setFilters($category, $data = [])
    {
        $this->setCategoryIds($category);
        //$this->setFilterValues();

        $this->values['filter-brand-group'] = $this->getFilterValue('filters', 'filter-brand-group', 'all', $data);
        $this->values['filter-brand'] = $this->getFilterValue('filters', 'filter-brand', 'all', $data);
        $this->values['search']       = $this->getFilterValue('filters', 'search');
        $this->values['per_page']     = $this->getFilterValue('filters', 'per_page');
        $this->values['sorter']       = $this->getFilterValue('filters', 'sorter');
        $this->values['filter-cost']  = $this->getFilterValue('filters', 'filter-cost', $category->id);

        if (!$this->values['per_page']) {
            $this->values['per_page'] = [$this->defaultPerPage];
        }
        if (!$this->values['sorter']) {
            $this->values['sorter'] = [$this->defaultSorter];
        }

        $attrs = $this->getFilterNames();
        foreach ($attrs as $attr) {
            $filter = $this->getFilterValue('filters', $attr, $category->id);
            if ($filter) {
                $this->values[$attr] = $filter;
            }
        }
        $this->setDataFromSphinx();
    }


    /** Получаем все возможные атрибуты
     * @return array
     */
    private function getFilterNames()
    {
        $filters    = [];
        $properties = Property::find()->byStatus()->all();
        foreach ($properties as $property) {
            $filters[] = 'filter-' . $property->id;
        }
        return $filters;
    }


    /**
     * Устанавливаем фасеты, поисковый запрос и максимальную/минимальную стоимость
     */
    private function setDataFromSphinx()
    {
        if (!$this->categoryAllFacet) {
            $this->categoryAllFacet = ProductSphinx::getSphinxCategoryAllFacet(($this->facetBrandLimit) ? [ 'brand_id' => $this->facetBrandLimit] : []);
        }
        if (!$this->facets) {
            $this->facets = ProductSphinx::getSphinxFacets(
                $this->getValue('search'),
                $this->getValue('filter-cost'),
                $this->getValue('filter-brand'),
                $this->getValue('filter-brand-group'),
                $this->categoryIds,
                $this->getValue()
            );
        }

        if (!$this->facetsAll) {
            $this->facetsAll = ProductSphinx::getSphinxFacets(
                [],
                [],
                ($this->facetBrandLimit) ? $this->facetBrandLimit : [],
                ($this->facetBrandGroupLimit) ? $this->facetBrandGroupLimit : [],
                $this->categoryIds,
                []
            );
        }
        if (!$this->priceRange) {
            $this->priceRange = ProductSphinx::getSphinxPriceRange(
                [],
                [],
                ($this->facetBrandLimit) ? $this->facetBrandLimit : [],
                ($this->facetBrandGroupLimit) ? $this->facetBrandGroupLimit : [],
                $this->categoryIds,
                []
            );
        }
    }


    /** Получаем даныне по фильтрам из сессии
     *
     * Pjax - навигация             +
     * POST - обновление фильтров   -
     * GET - переход по страницам   +
     *
     *
     * @param $data
     * @param $categoryId
     *
     * @return array
     */
    private function getDataFromSession($data, $categoryId)
    {
        if (!empty($this->dataFromSession[$categoryId])) {
            return $this->dataFromSession[$categoryId];
        }



        /*
         * Pjax, Post - pjaxPager
         * Post, ajax - filters
         * Get - new page
         */
        if (Yii::$app->request->isPost && !Yii::$app->request->isPjax) {
            $restore = false;
        } else {
            $restore = true;
        }

        $sessionName = 'filters' . $this->id . $categoryId;

        $session = Yii::$app->session;
        if ($session->isActive) {
            $dataFilters = null;
            if ($session->has($sessionName)) {
                $dataFilters = $session->get($sessionName);

                foreach ($dataFilters as $filterName => $value) {
                    if ($restore && !isset($data[$filterName])) {
                        $data[$filterName] = $value;
                    }
                }
            }
            $session->set($sessionName, $data);
        }
        $this->dataFromSession[$categoryId] = $data;

        return $data;
    }

    /** Получаем пришедшие данные из фильтра
     *
     * @param $groupId
     * @param $attribute
     * @param string $categoryId
     * @param array $defaultValue
     *
     * @return mixed|null
     */
    public function getFilterValue($groupId, $attribute, $categoryId = 'all', $defaultValue = [])
    {
        $value = null;
        $data  = ArrayHelper::merge(Yii::$app->request->post($groupId), $defaultValue);

        $data = $this->getDataFromSession($data, $categoryId);

        if (isset($data[$attribute])) {
            $value = $data[$attribute];
        }
        return $value;
    }

    /**
     * @param $attr
     *
     * @return null
     */
    public function getValue($attr = null)
    {
        if (!$attr) {
            $values = [];
            foreach ($this->values as $filterName => $value) {
                if (!in_array($filterName, ['search', 'filter-brand', 'filter-brand-group', 'filter-cost', 'sorter', 'per_page'])) {
                    if ($value !== null) {
                        $values[] = $value;
                    }
                }
            }
            return $values;
        } else {
            if (isset($this->values[$attr])) {
                return $this->values[$attr];
            }
        }
        return [];
    }

    /**
     * Получаем все id подкатегорий и текущей категории
     */
    private function setCategoryIds($category)
    {
        if (!$this->categoryIds && $category) {
            $allCats   = array_keys($category->children()->map()->all());
            $allCats[] = $category->id;

            $this->categoryIds = $allCats;
        }
    }
}
