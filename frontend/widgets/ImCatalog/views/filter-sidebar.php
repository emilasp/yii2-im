<?php
use emilasp\im\frontend\widgets\SessionFilter\SessionFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;

?>

<?php
$filterIds = [];
foreach (array_keys($properties) as $propId) {
    $filterIds[] = '#pjax-filter-filter-' . $propId;
}
?>

<div class="im-grid-setting-sorter filters">

    <?php if (true) : ?>

        <div class="filter-ceil">
            <span class="float-left title"><?= Yii::t('im', 'Filter brand') ?></span>

            <div class="im-filters im-filters-ceil">

                <?= SessionFilter::widget([
                    'id'              => 'filter-brand',
                    'type'            => SessionFilter::TYPE_CHECKBOX_LIST,
                    'items'           => $brands,
                    'facet'           => $brand,
                    'facetAll'        => $brandAll,
                    'containers'      => ['.im-filters'],
                    'scrollToChecked' => true,
                    'events'          => [
                        'change' => [
                            'selector' => '#filter-filter-brand',
                            'reload'   => array_merge($filterIds, ['#pjax-filter-filter-brand', '#pjax-filter-filter-brand-group', '#im-grid']),
                        ],
                    ],
                ]) ?>

            </div>
        </div>

        <div class="filter-ceil">
            <span class="float-left title"><?= Yii::t('im', 'Brand group') ?></span>

            <div class="im-filters im-filters-ceil">

                <?= SessionFilter::widget([
                    'id'              => 'filter-brand-group',
                    'type'            => SessionFilter::TYPE_CHECKBOX_LIST,
                    'items'           => $brandGroups,
                    'facet'           => $brandGroup,
                    'facetAll'        => $brandGroupAll,
                    'containers'      => ['.im-filters'],
                    'scrollToChecked' => true,
                    'events'          => [
                        'change' => [
                            'selector' => '#filter-filter-brand-group',
                            'reload'   => array_merge($filterIds, ['#pjax-filter-filter-brand','#pjax-filter-filter-brand-group', '#im-grid']),
                        ],
                    ],
                ]) ?>

            </div>
        </div>
    <?php endif ?>

    <?php foreach ($propertyGroups as $group) : ?>
        <?php if ($group->hasProperties(array_keys($properties))) : ?>
            <div class="filter-ceil">

                <span class="float-left title"><?= $group->name ?></span>

                <?php foreach ($group->properties as $property) : ?>
                    <?php if (isset($properties[$property->id])) : ?>

                        <?php $filterId = 'filter-' . $property->id; ?>

                        <div class="im-filters im-filters-ceil" data-group="<?= $group->id ?>">

                            <?= SessionFilter::widget([
                                'id'              => $filterId,
                                'type'            => SessionFilter::TYPE_CHECKBOX_LIST,
                                'items'           => ArrayHelper::map($property->values, 'id', 'value'),
                                'facet'           => $propertyValues,
                                'facetAll'        => $propertyValuesAll,
                                'containers'      => ['.im-filters'],
                                'scrollToChecked' => true,
                                'events'          => [
                                    'change' => [
                                        'selector' => '#filter-' . $filterId . '',
                                        'reload'   => ['#im-grid', '#pjax-filter-filter-brand'] + $filterIds,
                                    ],
                                ],
                            ]) ?>

                        </div>
                    <?php endif ?>
                <?php endforeach; ?>

            </div>
        <?php endif ?>
    <?php endforeach; ?>
</div>
