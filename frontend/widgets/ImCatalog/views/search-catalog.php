<?php
use emilasp\im\frontend\widgets\SessionFilter\SessionFilter;
use yii\helpers\Html;
use yii\web\JsExpression;
?>

<div class="im-grid-setting-sorter filters">
    <label class="float-left"><?= Yii::t('im', 'Sort') ?></label>
    <div class="im-filters radio-filter float-left">

        <?= SessionFilter::widget([
            'id'           => 'search',
            'type'         => SessionFilter::TYPE_TEXT,
            'items'        => [''],
            'containers' => ['.im-filters'],
            'events' => [
                /*'change' => [
                    'selector' => '#filter-' . $filterId . ' .input-val',
                    'reload' => $filterIds
                ],*/
                'click' => [
                    'selector' => '.btn-filter',
                    'reload' => ['#im-grid']
                ],
            ],
        ]) ?>
    </div>
</div>

