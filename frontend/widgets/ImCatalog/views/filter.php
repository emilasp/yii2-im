<?php
use emilasp\im\frontend\widgets\SessionFilter\SessionFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;

?>

<?php
$filterIds = [];
foreach (array_keys($properties) as $propId) {
    $filterIds[] = '#pjax-filter-filter-' . $propId;
}

\dosamigos\selectize\SelectizeAsset::register($this);
?>

<div class="im-grid-setting-sorter filters">
    <div class="float-left"><?= Yii::t('im', 'Filters:') ?></div>

    <?php if ($priceRange[0] || $priceRange[1]) : ?>

        <div class="filter-ceil">
            <div id="filterTitlecost" class="filterTitlecost filter-title">
                <span class="float-left title"><?= Yii::t('im', 'Filter price') ?></span>
                <span class="filters-items"></span>

                <div id="filter-cost-block" class="filter-hidden-block">

                    <div class="text-right">
                        <button class="btn-filter btn btn-success">
                            <?= Yii::t('im', 'Filter send btn') ?>
                        </button>
                    </div>

                    <div class="im-filters im-filters-ceil" data-group="cost">
                        <strong><?= Yii::t('im', 'Filter price') ?></strong>

                        <?= SessionFilter::widget([
                            'id'         => 'filter-cost',
                            'type'       => SessionFilter::TYPE_SLIDER,
                            'minmax'     => $priceRange,
                            'containers' => ['.im-filters'],
                            'events'     => [
                                'change' => [
                                    'selector' => '#filter-filter-cost .input-val',
                                    'reload'   => $filterIds + ['#pjax-filter-filter-brand'],
                                ],
                                'click'  => [
                                    'selector' => '.btn-filter',
                                    'reload'   => ['#im-grid'],
                                ],
                            ],
                        ]) ?>

                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>

    <?php foreach ($propertyGroups as $group) : ?>
        <?php if ($group->hasProperties(array_keys($properties))) : ?>
            <div class="filter-ceil">

                <div id="filterTitle<?= $group->id ?>" class="filterTitle<?= $group->id ?>  filter-title">
                    <span class="float-left title"><?= $group->name ?></span>
                    <span class="filters-items"></span>


                    <div id="filter-<?= $group->id ?>-block" class="filter-hidden-block">

                        <div class="text-right">
                            <button class="btn-filter btn btn-success">
                                <?= Yii::t('im', 'Filter send btn') ?>
                            </button>
                        </div>

                        <?php foreach ($group->properties as $property) : ?>
                            <?php if (isset($properties[$property->id])) : ?>

                                <?php $filterId = 'filter-' . $property->id; ?>

                                <div class="im-filters im-filters-ceil" data-group="<?= $group->id ?>">
                                    <strong><?= $property->name ?></strong>

                                    <?= SessionFilter::widget([
                                        'id'         => $filterId,
                                        'type'       => SessionFilter::TYPE_CHECKBOX_LIST,
                                        'items'      => ArrayHelper::map($property->values, 'id', 'value'),
                                        'facet'      => $propertyValues,
                                        'facetAll'   => $propertyValuesAll,
                                        'containers' => ['.im-filters'],
                                        'events'     => [
                                            'change' => [
                                                'selector' => '#filter-' . $filterId . ' .input-val',
                                                'reload'   => array_merge($filterIds, ['#pjax-filter-filter-brand']),
                                            ],
                                            'click'  => [
                                                'selector' => '.btn-filter',
                                                'reload'   => ['#im-grid'],
                                            ],
                                        ],
                                    ]) ?>

                                </div>
                            <?php endif ?>
                        <?php endforeach; ?>

                    </div>
                </div>
            </div>
        <?php endif ?>
    <?php endforeach; ?>
</div>
