<?php
use dosamigos\chartjs\ChartJs;
use emilasp\core\assets\OwlCaruselAsset;
use emilasp\core\helpers\DateHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\widgets\Pjax;

?>
<?php OwlCaruselAsset::register($this); ?>

<?php Pjax::begin(['id' => 'im-grid', 'scrollTo' => 1]); ?>

<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'emptyText' => 'Товаров не найдено',
    'summary' => '<div class="text-right">Показано <strong>{begin}-{end}</strong> из <strong>{totalCount}</strong> товаров.</div>',
    'itemOptions' => ['class' => 'im-catalog-' . $typeItemView],
    'itemView' => function ($model, $key, $index, $widget) use ($typeItemView)  {
        return $this->render('parts/' . $typeItemView, ['model' => $model]);
    },
]) ?>

<?php Pjax::end(); ?>

<div class="remodal " data-remodal-id="product-preview">
    <div class="product-preview-container"></div>
</div>