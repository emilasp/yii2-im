<?php
use emilasp\im\frontend\widgets\SessionFilter\SessionFilter;
use yii\helpers\Html;
use yii\web\JsExpression;
?>

<div class="im-grid-setting-per-row filters">
    <div class="float-left"><?= Yii::t('im', 'Per page') ?>: </div>
    <div class="im-filters radio-filter float-left">

        <?= SessionFilter::widget([
            'id'           => 'per_page',
            'type'         => SessionFilter::TYPE_SELECT,
            'items'        => [
                '20'  => '20',
                '40' => '40',
                '60' => '60',
                '100' => '100',
            ],
            'defaultValue' => '20',

            'containers' => ['.im-filters'],
            'events' => [
                'change' => [
                    'selector' => '#filter-per_page',
                    'reload' => ['#im-grid', '#pjax-filter-per_page']
                ],
            ],
        ]) ?>
    </div>
</div>

