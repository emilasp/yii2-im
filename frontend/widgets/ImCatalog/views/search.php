<?php
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;


// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function (data, params) {
console.log(data);
    params.page = params.page || 1;
    return {
        results: data.results,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;
// render your widget
echo Select2::widget([
    'id'            => 'im-search',
    'language'      => 'ru',
    'name'          => 'im-search',
    'value'         => '',
    'initValueText' => '',
    'options'       => ['placeholder' => Yii::t('im', 'Search products')],
    'pluginOptions' => [
        'allowClear'         => true,
        'minimumInputLength' => 4,
        'ajax'               => [
            'url'            => Url::toRoute('/im/product/search'),
            'dataType'       => 'json',
            'delay'          => 250,
            'data'           => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
            'processResults' => new JsExpression($resultsJs),
            'cache'          => true,
        ],
        'escapeMarkup'       => new JsExpression('function (markup) { return markup; }'),
        'templateResult'     => new JsExpression('formatRepo'),
        'templateSelection'  => new JsExpression('formatRepoSelection'),
        'inputTooShort'      => new JsExpression('function () { return "Enter 1 Character"; }'),
    ],
    'pluginEvents'  => [
        'change' => 'function(event) { location.href = "' . Url::toRoute('/im/product/') . '/" + $("#im-search").val() + "/title.html"; }',
    ],
]);