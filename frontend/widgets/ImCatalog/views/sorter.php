<?php
use emilasp\im\frontend\widgets\SessionFilter\SessionFilter;
use yii\helpers\Html;
use yii\web\JsExpression;
?>

<div class="im-grid-setting-sorter filters">
    <div class="float-left"><?= Yii::t('im', 'Sort') ?>: </div>
    <div class="im-filters radio-filter float-left">

        <?= SessionFilter::widget([
            'id'           => 'sorter',
            'type'         => SessionFilter::TYPE_SELECT,
            'items'        => [
                'cost_asc'  => Yii::t('im', 'Sort price min'),
                'cost_desc' => Yii::t('im', 'Sort price max'),
                'date_desc'  => Yii::t('im', 'Sort date'),
                'rate_desc'  => Yii::t('im', 'Sort rate'),
            ],
            'defaultValue' => 'cost_asc',

            'containers' => ['.im-filters'],
            'events' => [
                'change' => [
                    'selector' => '#filter-sorter',
                    'reload' => ['#im-grid', '#pjax-filter-sorter']
                ],
            ],
        ]) ?>
    </div>
</div>

