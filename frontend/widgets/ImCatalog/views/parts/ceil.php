<?php
use emilasp\files\models\File;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="im-grid-ceil-outline">

    <div class="im-preview-product" data-url="<?= Url::toRoute(['/im/product/view-ajax', 'id' => $model->id]) ?>">
        <?= Yii::t('im', 'Product preview') ?>
    </div>

    <?php if ($model->discount) : ?>
        <div class="corner-ribbon top-right sticky green"><?= $model->discount ?><span>%</span></div>
    <?php endif ?>
    <div class="product-view-image">
        <?php if (count($model->images) > 1) : ?>

                <div class="image-nav-prev"><i class="fa fa-chevron-circle-left fa-2x"></i></div>
                <div class="image-nav-next"><i class="fa fa-chevron-circle-right fa-2x"></i></div>

            <a href="<?= $model->getUrl() ?>">
                <div class="product-images owl-carousel owl-theme">
                    <?php foreach ($model->images as $image) : ?>
                        <div class="item">
                            <img class="owl-lazy" data-src="<?= $image->getUrl(File::SIZE_MID) ?>"
                                 data-src-retina="<?= $image->getUrl(File::SIZE_MAX) ?>" alt="<?= $image->title ?>"/>
                        </div>

                    <?php endforeach; ?>
                </div>
            </a>
        <?php else : ?>
            <?php $imageFirst = $model->getImage(); ?>
            <a href="<?= $model->getUrl() ?>">
                <img src="<?= $imageFirst->getUrl(File::SIZE_MID) ?>" alt="<?= $imageFirst->title ?>"/>
            </a>
        <?php endif ?>
    </div>

    <div class="lineProduct"></div>
    <div class="ceil-block-info">

        <div class="row">
            <?php if ($model->getPrice() !== $model->getPrice(true)) : ?>
                <div class="col-md-6 ceil-price price-old">
                    <?= number_format($model->getPrice(true), 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                </div>
                <div class="col-md-6 ceil-price price-sale text-right">
                    <?= number_format($model->getPrice(), 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                </div>
            <?php else : ?>
                <div class="col-md-6 ceil-price text-right">
                    <?= number_format($model->getPrice(), 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                </div>
            <?php endif ?>
        </div>

        <div class="row">
            <?php if ($model->brand) : ?>
                <div class="col-md-6 ceil-brand">
                    <a href="<?= $model->brand->getUrl() ?>">
                        <?= $model->brand->name ?>
                    </a>
                </div>
            <?php endif ?>
            <?php if ($model->categories) : ?>
                <div class="col-md-6 ceil-category text-right">
                    <a href="<?= $model->categories[0]->getUrl() ?>">
                        <?= $model->categories[0]->name ?>
                    </a>
                </div>
            <?php endif ?>
        </div>
        <?php if ($model->name) : ?>
            <div class="ceil-name"><?= $model->name ?></div>
        <?php endif ?>
        <div class="ceil-info">

            <?php if ($model->country) : ?>
                <div class="row">
                    <div class="col-md-4"><?= Yii::t('im', 'Country') ?>:</div>
                    <div class="col-md-8"><?= $model->country ?></div>
                </div>
            <?php endif ?>
            <?php if ($model->weight) : ?>
                <div class="row">
                    <div class="col-md-4"><?= Yii::t('im', 'Weight') ?>:</div>
                    <div class="col-md-8 text-right"><?= $model->weight ?> <?= Yii::t('im', 'Weight: Gramm') ?></div>
                </div>
            <?php endif ?>
            <?php if ($model->year) : ?>
                <div class="row">
                    <div class="col-md-4"><?= Yii::t('im', 'Year') ?>:</div>
                    <div class="col-md-8"><?= $model->year ?></div>
                </div>
            <?php endif ?>
            <?php if ($model->short_text) : ?>
                <strong><?= Yii::t('site', 'Short text') ?>:</strong>
                <div class="col-md-8"><?= $model->short_text ?></div>
            <?php endif ?>
        </div>
    </div>
</div>