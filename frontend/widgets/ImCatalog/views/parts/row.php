<?php
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="ceil-block-image">
    image, short detail, label
</div>
<div class="ceil-block-info">
    <div class="ceil-cart">cart</div>
    <div class="ceil-price">price</div>
    <div class="ceil-info">info(name, description)</div>
</div>