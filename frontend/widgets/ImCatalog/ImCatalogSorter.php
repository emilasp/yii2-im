<?php
namespace emilasp\im\frontend\widgets\ImCatalog;

use emilasp\core\components\base\Widget;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class ImCatalogSorter
 * @package emilasp\im\frontend\widgets\ImCatalog
 */
class ImCatalogSorter extends Widget
{
    public function init()
    {

    }

    public function run()
    {
        echo $this->render('sorter');
    }

}
