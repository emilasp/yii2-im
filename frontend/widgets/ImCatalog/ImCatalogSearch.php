<?php
namespace emilasp\im\frontend\widgets\ImCatalog;

use emilasp\core\components\base\Widget;
use emilasp\im\common\models\ProductSphinx;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class ImCatalogSearch
 * @package emilasp\im\frontend\widgets\ImCatalog
 */
class ImCatalogSearch extends Widget
{
    private $search;

    public function init()
    {
        parent::init();
        $this->setPropertyValues();
    }

    public function run()
    {
        echo $this->render('search-catalog', ['search' => $this->search]);
    }

    private function setPropertyValues()
    {
        $this->search = Yii::$app->getModule('im')->filters->search;
    }
}
