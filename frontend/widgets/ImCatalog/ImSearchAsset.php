<?php
namespace emilasp\im\frontend\widgets\ImCatalog;

use emilasp\core\components\base\AssetBundle;

/**
 * Class ImSearchAsset
 * @package emilasp\im\frontend\widgets\ImSearchAsset
 */
class ImSearchAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'search'
    ];
    public $js = [
        'search'
    ];
}
