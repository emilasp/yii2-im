<?php
namespace emilasp\im\frontend\widgets\ImCatalog;

use emilasp\core\components\base\AssetBundle;

/**
 * Class ImCatalog
 * @package emilasp\im\frontend\widgets\ImCatalogAsset
 */
class ImCatalogAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'catalog'
    ];
    public $js = [
        'catalog'
    ];
}
