<?php
namespace emilasp\im\frontend\widgets\ImCatalog;

use emilasp\core\components\base\Widget;
use emilasp\im\common\models\ProductSphinx;
use yii;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class ImSearch
 * @package emilasp\im\frontend\widgets\ImCatalog
 */
class ImSearch extends Widget
{
    private $search;

    public function init()
    {
        parent::init();
        
        $this->registerAssets();
    }

    public function run()
    {
        echo $this->render('search', ['search' => $this->search]);
    }

    private function registerAssets()
    {
        ImSearchAsset::register($this->view);
    }
}
