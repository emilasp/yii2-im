<?php
namespace emilasp\im\frontend\widgets\ImCatalog;

use emilasp\core\components\base\Widget;
use emilasp\im\common\models\Brand;
use emilasp\im\common\models\BrandGroup;
use emilasp\im\common\models\ProductSphinx;
use emilasp\taxonomy\models\PropertyGroup;
use yii;
use yii\caching\DbDependency;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class GoalCalendarFilter
 * @package emilasp\im\frontend\widgets\ImCatalog
 */
class ImCatalogFilter extends Widget
{
    public $viewType = PropertyGroup::VIEW_TYPE_TOP_FILTER;

    private $facet;
    private $properties        = [];
    private $propertyValues    = [];
    private $propertyValuesAll = [];
    private $priceRange        = [];
    private $brand             = [];
    private $brandAll          = [];
    private $brandGroup        = [];
    private $brandGroupAll     = [];

    public function init()
    {
        parent::init();
        $this->setPropertyValues();
    }

    public function run()
    {
        echo $this->render($this->getViewName(), [
            'properties'        => $this->properties,
            'propertyValues'    => $this->propertyValues,
            'propertyValuesAll' => $this->propertyValuesAll,
            'priceRange'        => $this->priceRange,

            'brands'   => $this->getBrands(),
            'brand'    => $this->brand,
            'brandAll' => $this->brandAll,

            'brandGroups'   => $this->getBrandGroups(),
            'brandGroup'    => $this->brandGroup,
            'brandGroupAll' => $this->brandGroupAll,

            'propertyGroups' => $this->getPropertyGroups(),
        ]);
    }

    /** Получаем бренды
     * @return mixed
     */
    private function getBrands()
    {
        return Yii::$app->db->cache(function ($db) {
            return Brand::find()->map()->byStatus()->orderBy('name')->all();
        }, null, new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM im_brand']));
    }

    /** Получаем бренды
     * @return mixed
     */
    private function getBrandGroups()
    {
        $groups = Yii::$app->db->cache(function ($db) {
            return BrandGroup::find()->byStatus()->orderBy('name')->all();
        }, null, new DbDependency(['sql' => 'SELECT MAX(updated_at) FROM im_brand_group']));

        if(isset($this->facet['facets']['brand_id'])) {
            $brandIds = ArrayHelper::getColumn($this->facet['facets']['brand_id'], 'brand_id');

            foreach ($groups as $index => $group) {
                if (!in_array($group->brand_id, $brandIds)) {
                    unset($groups[$index]);
                }
            }
        }

        return ArrayHelper::map($groups, 'id', 'name');
    }

    /** Получаем имя вьюхи в зависимости от типа групп виджета
     * @return string
     */
    private function getViewName()
    {
        if ($this->viewType === PropertyGroup::VIEW_TYPE_TOP_FILTER) {
            return 'filter';
        }
        return 'filter-sidebar';
    }

    /** Устанавливаем значения для фильтров из компанента
     * @return null
     */
    private function setPropertyValues()
    {
        $this->priceRange = Yii::$app->getModule('im')->filters->priceRange;
        $this->brandGroup = Yii::$app->getModule('im')->filters->brandGroup;
        $this->brand      = Yii::$app->getModule('im')->filters->brand;
        $this->facet      = Yii::$app->getModule('im')->filters->facets;
        $facetAll         = Yii::$app->getModule('im')->filters->facetsAll;

        if (!$this->facet) {
            $this->facet = $facetAll;
        }

        if (!$this->facet) {
            return null;
        }

        foreach ($this->facet['facets']['brand_id'] as $brand) {
            $this->brand[$brand['value']] = $brand;
        }
        foreach ($facetAll['facets']['brand_id'] as $value) {
            $this->brandAll[$value['value']] = $value;
        }

        foreach ($this->facet['facets']['brand_group_id'] as $brand) {
            $this->brandGroup[$brand['value']] = $brand;
        }
        foreach ($facetAll['facets']['brand_group_id'] as $value) {
            $this->brandGroupAll[$value['value']] = $value;
        }

        foreach ($this->facet['facets']['property'] as $value) {
            $this->properties[$value['value']] = $value;
        }
        foreach ($this->facet['facets']['property_value'] as $value) {
            $this->propertyValues[$value['value']] = $value;
        }
        foreach ($facetAll['facets']['property_value'] as $value) {
            $this->propertyValuesAll[$value['value']] = $value;
        }
    }

    /** Получаем группы для формирования фильтров
     * @return array
     */
    private function getPropertyGroups()
    {
        $viewType = $this->viewType;
        return Yii::$app->db->cache(function ($db) use ($viewType) {
            return PropertyGroup::find()->byStatus()->orderBy('order')->where(['view_type' => $viewType])->all();
        }, 5);
    }
}
