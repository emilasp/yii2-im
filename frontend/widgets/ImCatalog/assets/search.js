var formatRepo = function (repo) {
    console.log(repo);
    if (repo.loading) {
        return repo.text;
    }
    var markup =
        '<div class="row">' +
        '<div class="col-sm-2">' +
        '<img src="' + repo.image + '" class="img-rounded" />' +
        '</div>' +
        '<div class="col-sm-10">' +
        '<div class="row">' +
        '<div class="col-sm-12"><strong><i class="fa fa-star"></i> ' + repo.name + '</strong></div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-9"><i>' + repo.text + '</i></div>' +
        '<div class="col-sm-3">' +
        '<div class="row">' +
        '<div class="col-md-6 ceil-price price-old">' +
        repo.cost_base + 'р ' +
        '</div>' +
        '<div class="col-md-6 ceil-price price-sale">' +
        repo.cost + 'р ' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    return repo.text || repo.name;
}