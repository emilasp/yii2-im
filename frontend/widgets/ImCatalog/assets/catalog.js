function afterApplyFilter() {
    $('.im-filters-ceil').each(function (index, ceil) {
        var ceil = $(ceil);
        var titleBlock = ceil.closest('.filter-title');
        var filterBlock = ceil.find('.filter-field');
        var group = ceil.data('group');
        var blockTitleItems = $('.filterTitle' + group + ' .filters-items');

        if (filterBlock.attr('id') === 'filter-filter-cost') {
            var min = filterBlock.find('.input-min').val();
            var max = filterBlock.find('.input-max').val();
            var value = filterBlock.find('.input-val').val().split(',');

            if (value.length === 2 && (value[0] != min || value[1] != max)) {
                blockTitleItems.html('<span class="filter-item" data-title="' + titleBlock.attr('id') + '"  data-value="'
                    + '" data-min="' + min + '" data-max="' + max + '">' + value[0] + ' - ' + value[1]
                    + '<span class="filter-item-remove">&times;</span></span>');
            } else {
                blockTitleItems.html('');
            }
        } else {
            var items = '';
            var countChecked = 0;
            titleBlock.find('.input-val').each(function (ind, input) {
                var input = $(input);

                if (input.is(':checked')) {
                    countChecked++;
                    var label = input.parent().find('label').clone();
                    label.find('small').remove();
                    var labelSmall = label.text().substring(0, 6) + '..';

                    items += '<span class="filter-item" data-title="' + titleBlock.attr('id') + '"  data-value="' + input.attr('value') + '" title="' + label.text() + '">' + labelSmall
                        + '<span class="filter-item-remove">&times;</span></span>';
                }
            });
            if (countChecked > 2) {
                blockTitleItems.html('<span class="filter-item" data-title="' + titleBlock.attr('id') + '" data-value="all"> ' + countChecked + ' фильтра<span class="filter-item-remove">&times;</span></span>');
            } else {
                blockTitleItems.html(items);
            }
        }
        $('#im-grid').focus();
    });
}


function loaDoWLLazy()
{
    $('.owl-carousel').each(function () {
        var $this = $(this);
        $this.owlCarousel({
            margin: 0,
            lazyLoad: true,
            loop: false,
            items: 1
        });
        $this.closest('.product-view-image').find(".image-nav-prev").click(function () {
            $this.trigger('prev.owl.carousel');
        });

        $this.closest('.product-view-image').find(".image-nav-next").click(function () {
            $this.trigger('next.owl.carousel');
        });
    });
}

$(document).ready(function () {

    loaDoWLLazy();


    $('body').on("pjax:end", "#im-grid", function () {
        loaDoWLLazy();
    });


    $(document).on('imCatalog:reload', function () {
        afterApplyFilter();
    });

    $('.filter-title').on('click', '.filter-item', function (event) {
        var el = $(this);
        var title = el.data('title');
        var value = el.data('value');
        event.stopPropagation();
        event.preventDefault();
        var titleBlock = $('#' + title);

        var input;
        if (title && value === 'all') {
            titleBlock.find('input[type="checkbox"]').each(function (index, input) {
                input = $(input);
                input.attr('checked', false);
            });
        } else {
            if (!value) {
                input = titleBlock.find('[data-field="filter-cost"]');
                input.val(el.data('min') + ',' + el.data('max'));
            } else {
                input = titleBlock.find('[value="' + value + '"]');
                input.attr('checked', false);
            }
        }

        titleBlock.find('.btn-filter').click();
        el.remove();
        event.preventDefault();
        event.stopPropagation();
        console.log('remove');
        return false;
    });

    $(document).on('click', function () {
        var target = $(event.target);

        var hiddenBlock = target.closest('.filter-ceil').find(".filter-hidden-block");
        var hiddenBlockId = hiddenBlock.attr('id');

        if (target.hasClass('btn-filter')) {
            hiddenBlockId = null;
        }

        $('.filter-hidden-block').each(function (index, el) {
            var block = $(el);
            if (block.attr('id') !== hiddenBlockId) {
                block.slideUp();
            }
        });
        event.stopPropagation();
    });

    $('.filter-title').on('click', '.title', function () {
        var title = $(this).closest('.filter-title');

        console.log(title);
        if (title.hasClass('filter-item')) {
            console.log('has');
            return true;
        }


        console.log('title');
        if (!$(event.target).hasClass('btn-filter')) {
            var filterContainer = title.closest('.im-filter');
            var hiddenBlock = title.find('.filter-hidden-block');
            var width = filterContainer.width();

            hiddenBlock.css('width', width);
            hiddenBlock.slideDown();
        }
    });

    afterApplyFilter();
    $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
        options.async = true;
    });
    $('body').on('click', '.im-preview-product', function () {
        var button = $(this);
        var url = button.data('url');
        console.log('preview ');

      /*  var inst = $('[data-remodal-id=product-preview]').remodal();
        inst.open();
       */
        var myModal = new jBox('Modal', {
            position: {x: 'center', y: 'top'},
            offset: {x: 0, y: 50}
        });
        myModal.setContent('<div class="loadings"></div>');
        $('.loadings').loading(true)
        myModal.open();

        $.post(url, function (data) {
            myModal.setContent(data);

            $('.owl-carousel').each(function () {
                var _this = $(this);
                _this.owlCarousel({
                    margin: 0,
                    lazyLoad: true,
                    loop: false,
                    items: 1
                });

                _this.closest('.product-view-image').find(".image-nav-prev").click(function () {
                    _this.trigger('prev.owl.carousel');
                });

                _this.closest('.product-view-image').find(".image-nav-next").click(function () {
                    _this.trigger('next.owl.carousel');
                });
            });
        });
    });
});