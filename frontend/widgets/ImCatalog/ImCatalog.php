<?php
namespace emilasp\im\frontend\widgets\ImCatalog;

use emilasp\core\components\base\Widget;
use emilasp\im\common\models\Brand;
use emilasp\im\common\models\Product;
use emilasp\im\common\models\ProductSphinx;
use emilasp\taxonomy\models\Category;
use yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;

/**
 * Class ImCatalog
 * @package emilasp\im\frontend\widgets\ImCatalog
 */
class ImCatalog extends Widget
{
    public $typeItemView = 'ceil';

    /** @var  Category */
    public $category;

    /** @var  Brand */
    public $brand;
    public $brandGroup;

    public $conditions = [];

    private $dataProvider;


    private $categoryIds;


    public function init()
    {
        $this->registerAssets();

        $data = [];
        if ($this->brand) {
            $data['filter-brand'] = [$this->brand->id];
            //Yii::$app->getModule('im')->filters->facetBrandLimit = [$this->brand->id];
        }
        if ($this->brandGroup) {
            $data['filter-brand-group'] = [$this->brandGroup->id];
            //Yii::$app->getModule('im')->filters->facetBrandLimit = [$this->brand->id];
        }

        Yii::$app->getModule('im')->filters->setFilters($this->category, $data);

        $this->getDataProvider();
    }

    public function run()
    {
        echo $this->render('grid', [
            'typeItemView' => $this->typeItemView,
            'dataProvider' => $this->dataProvider,
        ]);
    }


    /**
     * Формируем датапровайдер с продуктами
     */
    private function getDataProvider()
    {
        $this->dataProvider = new ActiveDataProvider([
            'query'      => ProductSphinx::getProductQueryByIds($this->getSphinxResult(), $this->conditions),
            'pagination' => [
                'pageSize' => Yii::$app->getModule('im')->filters->getValue('per_page')[0],
            ],
        ]);
    }

    /** Собираем запрос
     * @return yii\db\Query
     */
    /*private function getQuery()
    {
        $query = Product::find()
                        ->with(['categories', 'images', 'brand'])
                        //->joinWith(['categories'])
                        ->where(['im_product.status' => Product::STATUS_ENABLED]);

        if ($sphinxIds = $this->getSphinxResult()) {

            //$sphinxIds = array_slice($sphinxIds, 0, 99);

            $query->andWhere(['im_product.id' => $sphinxIds]);
            //$query->orderBy(['id' => implode(',', $sphinxIds)]);
            $query->orderBy(new Expression('idx(array[' . implode(',', $sphinxIds) . '],im_product.id )'));
        } else {
            $query->andWhere(['im_product.id' => 0]);
        }
        return $query;
    }*/


    private function getSphinxResult()
    {
        $sphinxIds = [];
        $sort      = $this->getSortValue();

        $brand = ($this->brand) ? [$this->brand->id] : Yii::$app->getModule('im')->filters->getValue('filter-brand');
        $brandGroup = ($this->brandGroup) ? [$this->brandGroup->id] : Yii::$app->getModule('im')->filters->getValue('filter-brand-group');



        $query = ProductSphinx::getSphinxProductQuery(
            Yii::$app->getModule('im')->filters->getValue('search'),
            Yii::$app->getModule('im')->filters->getValue('filter-cost'),
            $brand,
            $brandGroup,
            Yii::$app->getModule('im')->filters->categoryIds,
            Yii::$app->getModule('im')->filters->getValue(null),
            $sort);

        if ($query) {
            $sphinxIds = $query->column();
        }

        return $sphinxIds;
    }

    /** Получаем текущую сортиорвку
     * @return array
     */
    private function getSortValue()
    {
        $sorters = [
            'id'   => 'id',
            'cost' => 'cost',
            'date' => 'created_at',
            'rate' => 'created_at',
        ];

        $value = Yii::$app->getModule('im')->filters->getValue('sorter')[0];

        $attr = '';
        $sort = '';
        if (strpos($value, '_') !== false) {
            $sortData = explode('_', $value);
            $attr     = $sorters[$sortData[0]];
            $sort     = $sortData[1];
        }
        return $attr . ' ' . $sort;
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        ImCatalogAsset::register($view);
    }
}
