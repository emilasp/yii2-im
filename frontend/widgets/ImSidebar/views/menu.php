<?php
use emilasp\im\frontend\widgets\ImCatalog\ImCatalogSearch;
use emilasp\im\frontend\widgets\ImSidebar\ImSidebarCategories;
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="sidebar">
    <h3><?= Yii::t('taxonomy', 'Categories') ?></h3>
    <?= ImSidebarCategories::widget(['curentCategory' => $category]) ?>

    <hr />
    <?= $filters ?>
</div>