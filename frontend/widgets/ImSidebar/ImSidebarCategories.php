<?php
namespace emilasp\im\frontend\widgets\ImSidebar;

use emilasp\core\components\base\Widget;
use emilasp\im\common\models\ImCategory;
use yii;
use yii\helpers\Html;

/**
 * Class ImSidebarCategories
 * @package emilasp\im\frontend\widgets\ImSidebar
 */
class ImSidebarCategories extends Widget
{
    public $rootId;
    public $curentCategory;

    private $facet;

    public function init()
    {
        $this->registerAssets();

        $this->rootId = Yii::$app->getModule('im')->getSetting('category_root_id');

        $this->facet  = Yii::$app->getModule('im')->filters->categoryAllFacet;
    }

    public function run()
    {
        $items = $this->getChildsRoot();
        $items = $this->prepareItems($items);
        $tree  = $this->renderTree($items);

        echo $tree;
    }


    /** Преобразовываем nested set в массив с иерархией
     *
     * @param $items
     *
     * @return array
     */
    private function prepareItems($items)
    {
        $stack    = [];
        $arraySet = [];
        foreach ($items as $item) {
            $stackSize = count($stack);
            while ($stackSize > 0 && $stack[$stackSize - 1]['rgt'] < $item->lft) {
                array_pop($stack);
                $stackSize--;
            }
            $link =& $arraySet;
            for ($i = 0; $i < $stackSize; $i++) {
                $link =& $link[$stack[$i]['index']]['children']; //navigate to the proper children array
            }
            $tmp = array_push($link, [
                'item'     => $item,
                'children' => [],
            ]);
            array_push($stack, ['index' => ($tmp - 1), 'rgt' => $item->rgt,]);
        }
        return $arraySet;
    }


    /** Получаем детей от рута
     * @return array
     */
    private function getChildsRoot()
    {
        $root = ImCategory::findOne($this->rootId);
        if ($root) {
            return $root->children()->all();
        }
        return [];
    }

    /** Формируем дерево списков в html
     *
     * @param $items
     *
     * @return string
     */
    private function renderTree($items)
    {
        $html = Html::beginTag('div', ['id' => 'tree-' . $this->id, 'class' => 'category-sidebar-menu']);

        $html .= $this->renderItemsRecursive($items);
        $html .= Html::endTag('div');
        return $html;
    }

    /** Формируем списки рекурсивно
     *
     * @param $items
     *
     * @return string
     */
    private function renderItemsRecursive($items)
    {
        $html = Html::beginTag('ul');
        foreach ($items as $item) {
            $itemModel = $item['item'];

            $active = ($this->curentCategory && $this->curentCategory->id === $itemModel->id) ? 'active' : '';

            $html .= Html::beginTag('li', ['id' => $itemModel->id, 'class' => $active]);
            $countHtml = '';
            if ($count = $this->getCount($itemModel->id)) {
                $countHtml = Html::tag('small', ' (' . $count . ')');
            }

            if (count($item['children']) || $count) {
                $html .= Html::a(
                    $itemModel->name . $countHtml,
                    $itemModel->getUrl()
                );
            }


            if (isset($item['children'])) {
                $html .= $this->renderItemsRecursive($item['children']);
            }
            $html .= Html::endTag('li');
        }
        $html .= Html::endTag('ul');
        return $html;
    }

    private function getCount($id)
    {
        if (isset($this->facet[$id])) {
            return $this->facet[$id]['count'];
        }
        return 0;
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        ImSidebarAsset::register($view);
    }
}
