<?php
namespace emilasp\im\frontend\widgets\ImSidebar;

use emilasp\core\components\base\Widget;
use emilasp\goal\common\models\Goal;
use yii;

/**
 * Class ImSidebar
 * @package emilasp\im\frontend\widgets\ImSidebar
 */
class ImSidebar extends Widget
{
    public $category;
    public $filters = '';

    public function init()
    {
        $this->registerAssets();
    }

    public function run()
    {
        echo $this->render('menu', [
            'category' => $this->category,
            'filters' => $this->filters,
        ]);
    }


    /**
     * Register client assets
     */
    private function registerAssets()
    {
        $view = $this->getView();
        ImSidebarAsset::register($view);
    }
}
