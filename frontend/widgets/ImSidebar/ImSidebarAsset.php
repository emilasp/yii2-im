<?php

namespace emilasp\im\frontend\widgets\ImSidebar;

use emilasp\core\components\base\AssetBundle;

/**
 * Class ImSidebar
 * @package emilasp\im\frontend\widgets\ImSidebarAsset
 */
class ImSidebarAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'sidebar'
    ];
    public $js = [
        //'sidebar'
    ];
}
