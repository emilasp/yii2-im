$(document).ready(function () {
    $('body').on('click', '.add-to-cart', function (event) {
        var button = $(this);
        var mainBlock = button.closest('.im-product');
        var variateBlock = mainBlock.find('.product-variation-block');
        var variateItems = variateBlock.find('.product-variation');

        var productId = button.data('id');

        var errorVariation = false;
        var res = {};
        res[productId] = {};
        var variations = [];
        
        variateItems.each(function(index, element) {
            var variation = [];

            $(this).find('select, input[type="checkbox"]').each(function (i, el) {
                var field = $(el);

                if (field.is('select') || field.is(':checked')) {
                    var val = field.val();
                    if (val) {
                        variation.push(val);
                    }
                }
            });

            if (!variation.length) {
                variateItems.addClass('cart-variate-error');
                errorVariation = true;
            } else {
                variations.push(variation)
                
            }
        });

        res[productId]['count'] = 1;
        res[productId]['variations'] = variations;
        
        if (!errorVariation) {
            toCart(res, mainBlock);
        }
    });

    $('body').on('click', '.cart-variate-error input, .cart-variate-error select', function (event) {
        $(this).closest('.cart-variate-error').removeClass('cart-variate-error');
    });

    $('body').on('click', '.btn-cart-remove', function (event) {
        var row = $(this).closest('.cart-item');
        var spinner = row.find('.cart-item-spinner');
        row.data('remove', 1);
        spinner.val(0);
        spinner.change();
    });

    $('body').on('change', 'input.cart-item-spinner', function (event) {
        var input = $(this);
        var row = input.closest('.cart-item');
        var id = row.data('id');
        var hash = row.data('hash');
        var url = row.data('url');
        var value = input.val();

        var res = {};
        res[id] = {};
        res[id]['count'] = value;
        res[id]['hash'] = hash;
        
        $.ajax({
            url: url,
            type: 'post',
            dataType: "json",
            data: {'object':'cart', 'action':'add', 'data':res},
            success: function(data) {
                if(data[0] == 1) {
                    //notice('Корзина успешно обновлена', 'green');

                    $('.cart-count-title, .cart-count').html(data['count']);
                    $('.cart-total-summ').html(data['summ']);
                    if (value == 0 && row.data('remove')) {
                        row.fadeTo("slow", 0.01, function(){
                            $(this).slideUp("slow", function() {
                                $(this).remove();
                            });
                        });
                    }
                } else {
                    notice('Не удалось обновить корзину', 'red');
                }
            }
        });
    });
});

function toCart(res, mainBlock)
{
    $.pjax({
        container: '#pjax-cart',
        "url": mainBlock.data('url'),
        "timeout": 0,
        "method": "POST",
        "scrollTo": false,
        "data": {'object':'cart', 'action':'add', 'data':res},
        push: false
    });

    $('.jBox-closeButton').click();

    var
        cart = $('.cart-widget'),
        img = mainBlock.find('.product-view-image .active img'),
        cartTopOffset = cart.offset().top,
        cartLeftOffset = cart.offset().left,
        imgTopOffset = img.offset().top,
        imgLeftOffset = img.offset().left;

    var flyingImg = img.clone();
    flyingImg.attr('src', img.attr('src')).addClass('flying-cart-img');
    flyingImg.css({position:"absolute", top:imgTopOffset+'px', left:imgLeftOffset+'px'}).css('z-index', 100000)
        .css('width', img.css('width')).css('height', img.css('height'));

    flyingImg.appendTo('body');

    flyingImg.animate({
        top: cartTopOffset,
        left: cartLeftOffset,
        width: 50,
        height: 50,
        opacity: 0.1
    }, 800, function () {
        flyingImg.remove();
    });
}

$('body').on("pjax:end", "#pjax-cart", function() {
    console.log($("#pjax-cart"));
});