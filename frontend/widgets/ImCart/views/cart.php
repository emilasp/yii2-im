<?php
use emilasp\files\models\File;
use kartik\touchspin\TouchSpinAsset;
use kartik\widgets\TouchSpin;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

TouchSpinAsset::register($this);
?>

<?php Pjax::begin(['id' => 'pjax-cart']); ?>

    <div class="cart-widget">
        <strong class="cart"><i class="fa fa-shopping-cart  fa-2x"></i> <?= Yii::t('im', 'Cart') ?>
            (<span class="cart-count"><?= $count ?></span>)
        </strong>

        <div class="cart-modal text-left">
            <h3>
                <?= Yii::t('im', 'Count products in you cart') ?>: <span class="cart-count-title"><?= $count ?></span>
            </h3>
            <hr/>
            <div class="cart-items cart-items-scroll">
                <?php foreach ($items as $id => $hashs) : ?>
                    <?php foreach ($hashs as $hash => $item) : ?>
                        <?php $product = $products[$id] ?>
                        <?= Html::beginTag('div', [
                            'class' => 'cart-item row',
                            'data'  => [
                                'id'     => $id,
                                'hash'   => $hash,
                                'count'  => $item['count'],
                                'url'    => Url::toRoute('/im/cart/change-count'),
                                'remove' => 1,
                            ],
                        ]) ?>
                        <div class="col-md-2">
                            <img src="<?= $product->getImage()->getUrl(File::SIZE_MIN) ?>" width="50px"/>
                        </div>
                        <div class="col-md-6">
                            <a href="<?= $product->getUrl() ?>"><?= $product->name ?></a>
                        </div>
                        <div class="col-md-2">
                            <?= TouchSpin::widget([
                                'name'          => 'volume',
                                'value'         => $item['count'],
                                'options'       => ['class' => 'cart-item-spinner'],
                                'pluginOptions' => [
                                    'step'              => 1,
                                    'verticalbuttons'   => true,
                                    'verticalupclass'   => 'glyphicon glyphicon-plus',
                                    'verticaldownclass' => 'glyphicon glyphicon-minus',
                                ],


                            ]) ?>
                        </div>
                        <div class="col-md-2"><?= $product->getPrice() ?> <?= Yii::t('im', 'Rub') ?></div>
                        <?= Html::endTag('div') ?>
                    <?php endforeach; ?>
                <?php endforeach; ?>
            </div>
            <hr/>
            <div class="text-right">
                <h3>
                    <?= Yii::t('im', 'Total summ') ?>:
                    <span class="cart-total-summ"><?= number_format($summ) ?></span>
                    <?= Yii::t('im', 'Rub') ?>
                </h3>
            </div>

            <div class="row">
                <div class="col-md-12 text-right">
                    <a href="<?= Url::toRoute('/im/checkout') ?>" class="btn btn-success" data-pjax="0">
                        <i class="fa fa-money"></i> <?= Yii::t('im', 'Checkout order') ?>
                    </a>&nbsp;
                    <a href="<?= Url::toRoute('/im/cart') ?>" class="btn btn-primary" data-pjax="0">
                        <i class="fa fa-cart-arrow-down"></i> <?= Yii::t('im', 'In cart') ?>
                    </a>
                </div>
            </div>
        </div>

    </div>

<?php Pjax::end(); ?>