<?php

namespace emilasp\im\frontend\widgets\ImCart;

use emilasp\core\components\base\AssetBundle;

/**
 * Class ImCart
 * @package emilasp\im\frontend\widgets\ImCartAsset
 */
class ImCartAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'cart'
    ];
    public $js = [
        'cart'
    ];
}
