<?php
namespace emilasp\im\frontend\widgets\ImCart;

use emilasp\core\components\base\Widget;
use emilasp\im\frontend\components\CartComponent;
use yii;
use yii\helpers\Json;
use yii\widgets\Pjax;

/**
 * Class ImCart
 * @package emilasp\im\frontend\widgets\ImCart
 */
class ImCart extends Widget
{
    public $componentCart;


    public function init()
    {
        $this->registerAssets();

        $this->componentCart = new CartComponent();
    }

    public function run()
    {
        $html = $this->render('cart', [
            'count'    => $this->componentCart->data['count'],
            'summ'     => $this->componentCart->data['summ'],
            'items'    => $this->componentCart->data['items'],
            'products' => $this->componentCart->items,
        ]);

        return $html;
    }


    /**
     * Register client assets
     */
    private function registerAssets()
    {
        ImCartAsset::register($this->view);
    }
}
