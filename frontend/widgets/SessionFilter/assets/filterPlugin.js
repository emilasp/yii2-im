(function ($) {

    var methods = {
        init: function (settings, $this) {

            var body = $('body');
            $.each(settings.events, function( eventName, event ) {

                $(document).off(eventName, event.selector);
                $(document).on(eventName, event.selector, function () {
                    methods.send(settings, methods._buildData(settings), event.reload, $this);
                });
            });

            body.off("imCatalog:reload");
            body.on("imCatalog:reload", function( event, param1, param2 ) {
                $('.owl-carousel').each(function () {
                    var $this = $(this);
                    $this.owlCarousel({
                        margin: 0,
                        lazyLoad: true,
                        loop: false,
                        items: 1
                    });
                    $this.closest('.product-view-image').find(".image-nav-prev").click(function () {
                        console.log('prev');
                        $this.trigger('prev.owl.carousel');
                    });

                    $this.closest('.product-view-image').find(".image-nav-next").click(function () {
                        $this.trigger('next.owl.carousel');
                    });
                });
            });

            methods._scrollFilters(settings);
        },
        send: function (settings, data, reload) {
            $.each(reload, function( index, value ) {
                if (value === '#im-grid') {
                    $(value).loading(true);
                }
            });

            $.ajax({
                type: "POST",
                url: document.location,
                data: $.param(data),
                success: function(msg){
                    var document = $(msg);
                    $.each(reload, function( index, value ) {
                        var newEl = document.find(value);

                        $(value).replaceWith(newEl);
                        if (value === '#im-grid') {
                            $('body').trigger("imCatalog:reload", [ "Custom", "Event" ] );
                        }
                    });
                    methods._scrollFilters(settings);
                }
            });
        },

        _scrollFilters: function (settings) {
            if (settings.scrollToChecked) {
                var container = $('#filter-' + settings.field).parent();
                var element;

                element = container.find('input[type="checkbox"]:checked').first();

                if (element.length) {
                    container.scrollTop(element.closest('.checkbox').offset().top + container.scrollTop() - container.offset().top);
                }
            }
        },

        _buildData: function (settings) {
            var allFilterFields = $(settings.main);
            var selectorExclude = settings.exclude;

            var data = {filters:{}};

            allFilterFields.each(function (index) {

                if (!$(this).is(selectorExclude)) {
                    var filterInputs = $(this).find('input.input-val, select');

                    filterInputs.each(function (indexFields) {
                        var inputObj = $(this);
                        var field = inputObj.data('field');
                        var value = inputObj.val();


                        if (typeof data.filters[field] === 'undefined') {
                            data.filters[field] = [];
                        }

                        if (inputObj.is('input[type="checkbox"], input[type="radio"]')) {
                            if (inputObj.is(':checked')) {
                                data.filters[field][data.filters[field].length] = value;
                            }
                        }
                        if (inputObj.is('select')) {
                            console.log(inputObj);
                            console.log(field);

                            if (value) {
                                data.filters[field][data.filters[field].length] = value;
                            }
                        }
                        if (inputObj.is('input[type="text"], input[type="hidden"]')) {
                            if (value) {
                                data.filters[field][data.filters[field].length] = value;
                            }
                        }
                    });
                }
            });

            return data;
        }
    };


    /**
     * Формируем и работаем с динамической моделью
     * @param options
     */
    $.fn.filters = function (options) {
        var $this = this;

        var settings = $.extend({
            'main': '.filter-field',
            'reload': '',
            'exclude': '',
            'eventType': 'click',
            'eventSelector': '.btn-filter',
            'scrollToChecked': false
        }, options);

        methods.init(settings, $this);
    };


})(window.jQuery);