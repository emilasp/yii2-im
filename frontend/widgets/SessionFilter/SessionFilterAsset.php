<?php

namespace emilasp\im\frontend\widgets\SessionFilter;

use emilasp\core\components\base\AssetBundle;

/**
 * Class SessionFilterAsset
 * @package emilasp\im\frontend\widgets\SessionFilter
 */
class SessionFilterAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'filter'
    ];
    public $js = [
        //'filter',
        'filterPlugin',
    ];
}
