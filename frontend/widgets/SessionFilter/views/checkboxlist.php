<?php
use yii\helpers\Html;

if (!isset($options['item'])) {
    $options['item'] = function ($index, $label, $name, $checked, $value) use ($id) {

        $html = Html::beginTag('div', ['class' => 'checkbox']);

        $html .= Html::checkbox($name, $checked,
            [
                'id' => 'checkbox' . $id . $value,
                'class' => 'input-val',
                'value' => $value,
                'data-field' => $id
            ]);
        $html .= Html::tag('label', Html::tag('span', '') . $label, ['for' => 'checkbox' . $id . $value]);
        $html .= Html::endTag('div');

        return $html;
    };
}


?>
<?= Html::checkboxList($name, $values, $items, $options) ?>