<?php
use kartik\slider\Slider;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

$values = $values ?? $items;
?>
<?= Html::beginTag('div', $options) ?>

<?= Html::tag('b', number_format($items[0]), ['class' => 'badge btn-info filter-price-badge-start']) ?>

<?= Slider::widget([
    'name'          => $name . '-slider',
    'value'         => implode(',', $values),
    'options'       => ['id' => $id . 'slider'],
    'sliderColor'   => Slider::TYPE_GREY,
    'handleColor'   => Slider::TYPE_SUCCESS,
    'pluginOptions' => [
        'min'       => $items[0],
        'max'       => $items[1],
        'step'      => 100,
        'range'     => true,
        'selection' => 'after',
    ],
    'pluginEvents'  => [
        'slideStop' => 'function() { $("[data-field=\'' . $id . '\']").val($("#' . $id . 'slider").val()); $("[data-field=\'' . $id . '\']").change(); }',
    ],
]) ?>

<?= Html::tag('b', number_format($items[1]), ['class' => 'badge btn-success filter-price-badge-end']) ?>

<?= Html::hiddenInput($name . 'min', $items[0], ['class' => 'input-min']) ?>
<?= Html::hiddenInput($name . 'max', $items[1], ['class' => 'input-max']) ?>
<?= Html::hiddenInput($name, implode(',', $values), ['class' => 'input-val', 'data-field' => $id]) ?>

<?= Html::endTag('div') ?>
