<?php
use yii\helpers\Html;

if (!isset($options['item'])) {
    $options['item'] = function ($index, $label, $name, $checked, $value) use ($id) {
        return '<div class="checkbox">' . Html::radio($name, $checked,
            ['class' => 'input-val', 'label' => $label, 'value' => $value, 'data-field' => $id]) . '</div>';
    };
}


?>
<?= Html::radioList($name, $values, $items, $options) ?>