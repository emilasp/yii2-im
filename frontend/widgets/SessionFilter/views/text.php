<?php
use yii\helpers\Html;

$values = !empty($values[0]) ? $values[0] : '';
?>
<?= Html::beginTag('div', $options) ?>

<?= Html::textInput($name, $values, ['class' => 'input-val', 'data-field' => $id]) ?>

<?= Html::endTag('div') ?>
