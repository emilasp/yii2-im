<?php
namespace emilasp\im\frontend\widgets\SessionFilter;

use emilasp\core\components\base\Widget;
use emilasp\goal\common\models\Goal;
use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * Class SessionFilter
 * @package emilasp\im\frontend\widgets\SessionFilter
 */
class SessionFilter extends Widget
{
    const TYPE_CHECKBOX_LIST = '1';
    const TYPE_RADIO_LIST    = '2';
    const TYPE_SELECT        = '3';
    const TYPE_SLIDER        = '4';
    const TYPE_COLORPICKER   = '5';
    const TYPE_TEXT          = '6';

    public $groupId = 'filters';

    public $id;
    public $type;
    public $scrollToChecked = false;

    public $items   = [];
    public $values  = [];
    public $options = [];

    public $defaultValue;

    public $facet    = [];
    public $facetAll = [];

    public $minmax;

    public $onClick = 'input';

    public $containers;
    public $events = [
        'change' => [
            'selector' => '',
            'reload'   => [],
        ],
        'click'  => [
            'selector' => '',
            'reload'   => [],
        ],
    ];

    /** @var array выполяем js после применения фильтра */
    public $js = ['after' => ''];

    private $defaultOptions = [
        'class' => 'filter-field',
    ];

    public function init()
    {
        $this->registerAssets();

        $this->setDefaultOptions();
        $this->setItemsForFacet();
    }

    public function run()
    {
        Pjax::begin(['id' => 'pjax-filter-' . $this->id]);
        echo $this->render($this->getViewByType($this->type), [
            'id'      => $this->id,
            'name'    => $this->groupId . '[' . $this->id . ']',
            'items'   => $this->items,
            'values'  => $this->values,
            'options' => $this->options,
        ]);
        Pjax::end();
    }

    /**
     * Фильтруем по фасету и добавляем количество
     */
    private function setItemsForFacet()
    {
        if ($this->facet) {
            foreach ($this->items as $valueId => $value) {
                if (isset($this->facet[$valueId])) {
                    $this->items[$valueId] = $value . ' <small>(' . $this->facet[$valueId]['count'] . ')</small>';
                } else {
                    if (isset($this->facetAll[$valueId])) {
                        $this->items[$valueId] = Html::tag('i', $value . ' <small>(0)</small>',
                            ['class' => 'disabled']);
                    } else {
                        if ($valueId) {
                            unset($this->items[$valueId]);
                        }
                    }
                }
            }
        }
    }

    /** Получаем вью для фильтра
     *
     * @param $type
     *
     * @return string
     */
    private function getViewByType($type)
    {
        $view = 'select';
        switch ($type) {
            case self::TYPE_CHECKBOX_LIST:
                $view = 'checkboxlist';
                break;
            case self::TYPE_RADIO_LIST:
                $view = 'radiolist';
                break;
            case self::TYPE_SELECT:
                $view = 'select';
                break;
            case self::TYPE_SLIDER:
                $view = 'slider';
                break;
            case self::TYPE_COLORPICKER:
                $view = 'color';
                break;
            case self::TYPE_TEXT:
                $view = 'text';
                break;

        }
        return $view;
    }


    /** Устанавливаем настройки по умолчанию
     */
    private function setDefaultOptions()
    {
        if ($this->minmax) {
            $this->items = $this->minmax;
        }

        $this->values               = Yii::$app->getModule('im')->filters->getValue($this->id);
        $this->defaultOptions['id'] = 'filter-' . $this->id;
        $this->options              = ArrayHelper::merge($this->defaultOptions, $this->options);
    }

    private function getUrl()
    {
        $parameters = array_merge(
            ['/' . Yii::$app->controller->module->id . '/' . Yii::$app->controller->id . '/' . Yii::$app->controller->action->id],
            Yii::$app->request->queryParams,
            ['filter' => $this->id]
        );
        return Url::to($parameters);
    }

    /**
     * Register client assets
     */
    private function registerAssets()
    {
        SessionFilterAsset::register($this->view);

        $options = Json::encode([
            'main'            => implode(', ', $this->containers),
            'events'          => $this->events,
            'url'             => $this->getUrl(),
            'field'           => $this->id,
            'scrollToChecked' => $this->scrollToChecked,
        ]);

        $js = <<<JS
        $('#{$this->id}').filters({$options});
JS;
        $this->view->registerJs($js);

    }
}
