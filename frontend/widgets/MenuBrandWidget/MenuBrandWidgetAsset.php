<?php

namespace emilasp\im\frontend\widgets\MenuBrandWidget;

use emilasp\core\components\base\AssetBundle;

/**
 * Class MenuBrandWidget
 * @package emilasp\im\frontend\widgets\MenuBrandWidget
 */
class MenuBrandWidgetAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'menu'
    ];
    /*public $js = [
        'menu'
    ];*/
}
