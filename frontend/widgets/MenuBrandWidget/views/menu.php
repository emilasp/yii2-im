<div class="header-brands">
    <div class="container effect6">

        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-6">
                        <ul class="nav navbar-nav">

                            <?php foreach ($brands as $brand) : ?>
                                <li role="presentation" class="dropdown">
                                    <a class="dropdown-toggle menu-brand-a disabled" data-toggle="dropdown"
                                       href="<?= $brand->getUrl() ?>"
                                       role="button" aria-haspopup="true" aria-expanded="false">
                                        <?= $brand->name ?> <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <?php foreach ($brand->groups as $group) : ?>
                                            <li><a class="menu-brand-sub-a" href="<?= $group->getUrl() ?>"><?= $group->name ?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            <?php endforeach; ?>

                        </ul>
                    </div>
                </div>
        </nav>
    </div>
</div>