<?php
namespace emilasp\im\frontend\widgets\MenuBrandWidget;

use emilasp\core\components\base\Widget;
use emilasp\im\common\models\Brand;
use emilasp\im\frontend\components\CartComponent;
use yii;
use yii\helpers\Json;
use yii\widgets\Pjax;

/**
 * Class MenuBrandWidget
 * @package emilasp\im\frontend\widgets\MenuBrandWidget
 */
class MenuBrandWidget extends Widget
{
    public $componentCart;


    public $brandIds = [12, 4, 5, 3, 8, 11, 7];
    
    public function init()
    {
        $this->registerAssets();
    }

    public function run()
    {
        return $this->render('menu', [
            'brands'    => $this->getBrands(),
        ]);
    }


    /** Get brands
     * @return static[]
     */
    private function getBrands()
    {
        return Brand::find()->with('groups')->where(['id' => $this->brandIds])->orderBy('name')->all();
    }
    
    /**
     * Register client assets
     */
    private function registerAssets()
    {
        MenuBrandWidgetAsset::register($this->view);
    }
}
