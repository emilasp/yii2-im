<?php
use emilasp\core\helpers\StringHelper;
use emilasp\files\models\File;

?>
<div class="product-assoc">

    <h3><?= $label ?>:</h3>

        <div class="image-nav-prev"><i class="fa fa-chevron-circle-left fa-3x"></i></div>
        <div class="image-nav-next"><i class="fa fa-chevron-circle-right fa-3x"></i></div>

    <div class="owl-carousel product-assoc-items clearfix">
        <?php foreach ($models as $product) : ?>
            <div class="product-assoc-item">
                <a href="<?= $product->getUrl() ?>">
                    <?php $image = $product->getImage(); ?>
                    <img src="<?= $image->getUrl(File::SIZE_MID) ?>" />
                    <div class="text-center"><?= StringHelper::truncateString($product->name, 20, ' ', '..') ?></div>
                </a>
                <div class="lineProduct"></div>
                <div class="row">
                    <div class="col-xs-6 ceil-category text-center">
                        <?php if ($product->categories) : ?>
                            <?= $product->categories[0]->name ?>
                        <?php endif ?>
                    </div>
                    <div class="col-xs-6">
                        <?php if ($product->getPrice() !== $product->getPrice(true)) : ?>
                            <div class="ceil-price price-sale text-center">
                                <?= number_format($product->getPrice(), 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                            </div>
                        <?php else : ?>
                            <div class="ceil-price text-center">
                                <?= number_format($product->getPrice(), 0, '', ' ') ?> <?= Yii::t('im', 'Rub') ?>
                            </div>
                        <?php endif ?>
                    </div>
                </div>


            </div>
        <?php endforeach; ?>
    </div>
</div>
