<?php

namespace emilasp\im\frontend\widgets\ImAssocProducts;

use emilasp\core\components\base\AssetBundle;

/**
 * Class ImAssocProducts
 * @package emilasp\im\frontend\widgets\ImAssocProducts
 */
class ImAssocProductsAsset extends AssetBundle
{
    public $sourcePath = __DIR__ . '/assets';

    public $jsOptions = ['position' => 1];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\web\JqueryAsset'
    ];

    public $css = [
        'items'
    ];
    public $js = [
        //'sidebar'
    ];
}
