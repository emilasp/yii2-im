<?php
namespace emilasp\im\frontend\widgets\ImAssocProducts;

use emilasp\core\components\base\Widget;
use yii;

/**
 * Class ImSidebar
 * @package emilasp\im\frontend\widgets\ImAssocProducts
 */
class ImAssocProducts extends Widget
{
    public $label;
    public $model;
    public $models;

    public function init()
    {
        $this->registerAssets();
        $this->getModels();
    }

    public function run()
    {
        if ($this->models) {
            echo $this->render('items', [
                'label'  => $this->label,
                'models' => $this->models,
            ]);
        }
    }

    private function getModels()
    {
        $models = $this->models;
        if (is_callable($models)) {
            $this->models = $models($this->model);
        }
    }


    /**
     * Register client assets
     */
    private function registerAssets()
    {
        ImAssocProductsAsset::register($this->getView());

        $js = <<<JS
        
        $('.product-assoc-items.owl-carousel').each(function () {
        var _this = $(this);
        _this.owlCarousel({
            margin: 0,
            loop: false,
            responsiveClass:true,
            responsive:{
                600:{items:3, nav:false},
                800:{items:4, nav:false},
                1000:{items:6, nav:false, loop:false}
            }
        });
        _this.closest('.product-assoc').find(".image-nav-prev").click(function () {
            _this.trigger('prev.owl.carousel');
        });

        _this.closest('.product-assoc').find(".image-nav-next").click(function () {
            _this.trigger('next.owl.carousel');
        });
    });
JS;
        $this->getView()->registerJs($js);

    }
}
