<?php
namespace emilasp\im\frontend\events;

use emilasp\im\backend\widgets\ImOrderProducts\ImOrderProducts;
use Yii;
use yii\base\Event;

/**
 * Class ImEvent
 * @package emilasp\im\frontend\events
 */
class ImEvent extends Event
{
    const EVENT_NEW_ORDER = 'onNewOrder';

    /** События
     * @var array
     */
    public static $events = [
        self::EVENT_NEW_ORDER
    ];

    /** @var  array */
    public $params = [];



    /** Событие создания нового пользователя
     * @param ImEvent $event
     */
    public function onNewOrder(ImEvent $event)
    {
        $orderTable =  ImOrderProducts::widget(['order' => $event->params['order'], 'typeView' => 'mail']);
        /** Send user email */
        $user = $event->params['user'];
        if ($user && $user->email) {
            Yii::$app->sender->sendMail(
                [$user->email],
                Yii::t('im', 'Im Email: new order'),
                'im/order-user',
                ['orderTable' => $orderTable, 'order' => $event->params['order']]
            );
        }

        /** Send admin email */
        Yii::$app->sender->sendMail(
            [Yii::$app->sender->email['admin'], Yii::$app->sender->email['support']],
            Yii::t('im', 'Im Email: new order'),
            'im/order-admin',
            ['orderTable' => $orderTable, 'order' => $event->params['order']]
        );
    }
}